# libmaum

MAUM.ai common c++, python, node libraries for product and service.

## It includes
- protobuf and grpc library.
- supervisor to manage all maum.ai service
- setup tool to install and configure maum.ai products.
- Cmake scripts to build maum.ai products and modules.
- C++ library
  - spdlog
  - config
  - jsoncpp
  - utilities
  - encoding
  - file or directory utilities library
- Python library
  - config
- Node library
  - config

## It supports
- m2u: [mindslab-ai/m2u](https://github.com/mindslab-ai/m2u)
- biz: [mindslab-ai/voc](https://github.com/mindslab-ai/biz)
- brain stt: [mindslab-ai/brain-stt](https://github.com/mindslab-ai/brain-stt)
- brain ta: [mindslab-ai/brain-ta](https://github.com/mindslab-ai/brain-ta)
- brain sds: [mindslab-ai/brain-sds](https://github.com/mindslab-ai/brain-sds)
- brain mrc: [mindslab-ai/brain-mrc](https://github.com/mindslab-ai/brain-mrc)
- brain qa: [mindslab-ai/brain-qa](https://github.com/mindslab-ai/brain-qa)
- and so on...

## Dependencies
- GCC 4.8.x
- G++ 4.8.x
- protobuf 3.5.x or later
- grpc 1.9.0 or later

### Gcc 5 mode
If you want to use gcc 5.x, set CC, CXX environment variable at ubuntu.
```bash
export CC=/usr/bin/cc
export CXX=/usr/bin/g++
```

## Directories
- cmake -> cmake common build utilities
- src ->  c++ libraries
- pysrc -> output config, supervisor libraries, setup tool.
- nodesrc -> output node libraries
- config -> common configuration for all maum.ai products.

## Build System
- CMAKE

## If you have previous grpc & protobuf & boost in `/usr/local`
```bash
cd /usr/local
sudo rm -rf libgrpc* libgpr.* libprotobuf* libprotoc.* libmaum-*
cd /usr/local/pkgconfig/
sudo rm -rf grpc*.pc protobuf*.pc
cd /usr/local/include/
sudo rm -rf grpc grpc++ google json/ libmaum/ maum/ spdlog/ boost/
sudo rm -rf share/cmake/modules/
sudo rm -rf /usr/local/bin/protoc /usr/local/bin/grpc_*plugin
```

## How to build

```
mkdir ~/git
cd ~/git
git checkout git@github.com:mindslab-ai/libmaum
cd libmaum
export MAUM_ROOT=~/maum
./build.sh
```
NOTE:
Before doing build.sh, **MAUM_ROOT must be defined**, typically as ~/maum.

### cmake options for manual build configuration
```bash
export MAUM_ROOT=~/maum
cmake \
 -DCMAKE_BUILD_TYPE=Debug \
 -DCMAKE_C_COMPILER=/usr/bin/gcc-4.8 \
 -DCMAKE_CXX_COMPILER=/usr/bin/g++-4.8 \
 -DCMAKE_INSTALL_PREFIX=${MAUM_ROOT} ~/git/libmaum/
```

## Installation Directory
### cmake modules
- ${MAUM_ROOT}/share/cmake/modules

### c++ libraries
- ${MAUM_ROOT}/lib/libmaum-common.so

### c++ header files
- ${MAUM_ROOT}/include/libmaum/*.h

### python
- ${MAUM_ROOT}/lib/python

### node
- ${MAUM_ROOT}/lib/node_modules

### log-plugins
- ${MAUM_ROOT}/libexec/log-plugins
- Please put `call log plugin`, `monitor log plugin` in this directory.
- Must be `shared object`.

## How to use cmake modules from this libraries

```
SET(CMAKE_MODULE_PATH ~/maum/share/cmake/modules)

## protobuf
include(grpc)
find_package(Protobuf REQUIRED)
file(GLOB PROTO_SRCS "*.proto")

protobuf_generate_grpc_cpp(PB_SRCS PB_HDRS ${PROTO_SRCS})


## import binaries
include(import)
msl_import_binary(STT_API_DIR STT_API_MADE "STT_API"
     binary_file_name.tar.gz)


add_executable(out ${SOURCE_FILES} $P{STT_API_MADE})

## gitversion
include(gitversion/cmake)
set(BINOUT sttd)
target_git_version_init(${BINOUT})

```

## Common setup tools
- Configuration specification is defined at proto/maum/common/setup/recipe.proto
- SEE, wiki of libmaum.

