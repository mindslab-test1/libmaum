let fs = require('fs');
let path = require('path');
let crypto = require('crypto');

function parseConfString(data) {
  let regex = {
    param: /^\s*([^=]+?)\s*=\s*(.*?)\s*$/,
    comment: /^\s*#.*$/,
  };

  let value = {};
  let lines = data.split(/[\r\n]+/);
  let section = null;
  lines.forEach(function(line) {
    if (regex.comment.test(line)) {
      return;
    } else if (regex.param.test(line)) {
      let match = line.match(regex.param);
      match[2] = match[2].replace(/\$\{MAUM_ROOT\}/g, this.maumRoot);
      value[match[1]] = match[2];
    } else if (line.length === 0) {
      return;
    }
  });
  return value;
}

/**
 * maum.ai common Configfile Loader
 */
class MaumConfig {
  constructor() {
    let maumRoot = process.env.MAUM_ROOT;
    if (maumRoot === undefined) {
      console.error(
          `\x1b[41m[ENV] MAUM_ROOT environment variable is not defined. \x1b[0m`);
      throw new Error('MAUM_ROOT is not specified.');
    }
    this.maumRoot = maumRoot;
    let confDir = path.join(maumRoot, 'etc');
    if (fs.existsSync(confDir) &&
        fs.lstatSync(confDir).isDirectory()) {
      this.confDir = confDir;
    } else {
      console.error(
          `\x1b[41m[CONF] Configuration directory not exist ${confDir}. \x1b[0m`);
      throw new Error('Configuration directory not exist.');
    }

    this.nv = {};
  }

  /**
   * 설정 정보를 로딩한다.
   *
   * @param key 요청하는 설정 변수 이름
   * @returns {String} 값
   */
  get(key) {
    return this.nv[key];
  }

  _decrypt(cipher) {
    let buf = Buffer.from(cipher, 'base64');
    if (buf.length < (16 * 3))
      return '';
    // console.log(buf);
    let ivBin = [];
    let keyBin = [];
    let encBin = [];

    for (let x = 0; x < (16 * 3); x++) {
      let mod = x % 3;
      switch (mod) {
        case 0:
          encBin.push(buf[x]);
          break;
        case 1:
          ivBin.push(buf[x]);
          break;
        case 2:
          keyBin.push(buf[x]);
          break;
      }
    }
    for (let x = (16 * 3); x < buf.length; x++) {
      encBin.push(buf[x]);
    }
    // console.log('IV', Buffer.from(ivBin));
    // console.log('KEY', Buffer.from(keyBin));
    // console.log('ENC', Buffer.from(encBin));
    let decipher = crypto.createDecipheriv('aes-128-cbc',
        Buffer.from(keyBin), Buffer.from(ivBin));
    decipher.setAutoPadding(true);
    // let ret = decipher.final('utf-8');
    let decBuf = Buffer.concat(
        [decipher.update(Buffer.from(encBin)), decipher.final()]);
    // console.log('dec', ret);
    // console.log('dec buf', decBuf);
    return decBuf.toString('utf-8');
  }

  /**
   * 설정 정보를 복호화하여 로딩한다.
   *
   * @param key 요청하는 설정 변수 이름
   * @returns {String} 복호화된 값
   */
  getDecrypt(key) {
    let v = this.nv[key];
    if (v !== undefined)
      return this._decrypt(v);
    return v;
  }

  load(filename) {
    let confFile = path.join(this.confDir, filename);
    try {
      let content = fs.readFileSync(confFile);
      this.nv = parseConfString(content.toString());
    } catch (e) {
      console.error(e);
      throw e;
    }
  }
}

module.exports = new MaumConfig();
