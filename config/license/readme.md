## License File

maum.ai license file includes several license of maum.ai products
and modules.

One license has multiple unit licences. A unit license defines a product,
expiration date, type (commercial, academy and so on), meta.

All maum.ai applications and servers check license on initialization.
If it failed in loading license or you have an expired license,
the application is not working.


## Requesting license

You should email to `license@mindslab.ai` or contact point of sales of MindsLab.

When requesting a license, you should envelop the following informations.

- Your company
- Your email
- Tour license description
- Products
- IP of MAC of running hosts

The following is an example.

```
{

 "licenseTo": "Good Company, Inc.",

 "description": "good company AI Service.",

 "registrant": "caller@good-company.com",

 "licenses": [

  {

   "product": "m2u",

   "type": "COMMERCIAL",

   "hosts": [

    "10.122.64.11",

    "aa:bb:cc:11:22:33"

   ],

   "allowedAccess": {

    "max-chatbots": 100,

    "concurrent-sessions": 5000

   }

  },

  {

   "product": "brain-mrc",

   "type": "ACADEMY",

   "hosts": [

    "10.122.64.12",

    "aa:bb:cc:22:22:55"

   ]

  },

  {

   "product": "brain-stt",

   "type": "DEMO",

   "hosts": [

    "10.122.64.12",

    "aa:bb:cc:22:22:55"

   ]

  }
 ]
}
```

## License File Location
License file must be $MAUM_ROOT/license/maumai-license.pem.
