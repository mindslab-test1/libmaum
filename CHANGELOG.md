<!---
 MAJOR.MINOR 패치는 # 즉, H1으로 표시
 MAJOR>MINOR.PATCH는 ## 즉, H2로 표시한다.
--->

<a name="1.1.2"></a>
## [1.1.2](https://github.com/mindslab-ai/libmaum/compare/v1.1.1...v1.1.2) (2017-12-22)

### Bug Fixes
* **c++-lib**: CallLog에 원하는 메타데이터를 넘기고 받는 구조 추가([#72](https://github.com/mindslab-ai/libmaum/issues/72))

<a name="1.1.1"></a>
## [1.1.1](https://github.com/mindslab-ai/libmaum/compare/v1.1.0...v1.1.1) (2017-12-12)

### Bug Fixes
* **cmake**: mysql.cmake 파일이름 오타 수정
* **c++-lib**: CallLog 실시간 적용 및 마지막 시간값 적용되지 않는 문제([#70](https://github.com/mindslab-ai/libmaum/issues/70))

<a name="1.1"></a>
# [1.1.0](https://github.com/mindslab-ai/libmaum/compare/v1.0.0...v1.1.0) (2017-12-08)

### Bug Fixes
* **config**: 폐쇄망에서 설치할 때 로컬 Primary IP를 구할 때,  google.com 을 참조하여 동작하지 않는 문제. IP를 참조하여 해결

### Features
* **third-party**: grpc 1.7.2로 업그레이드
* **third-party**: protobuf 3.4.0으로 업그레이드
* **jenkins**: CI 빌드가 되도록 구성
* **c++-lib**: spdlog 0.14.0 으로 업그레이드
* **c++-lib**: interval log 추가, 분 단위로 로그를 남기고 이에 기반하여 로그 생성 처리
* **c++-lib**: CallLog 라이브러리 추가, C++에서 grpc 호출 시점(caller, callee)에 로그를 남기는 표준 API 체계 정의
* **proto**: 네임스페이스 변경과 더불어서 m2u map을 위한 추가 공통 타입 정의, audio encoding 타입
* **cmake**: grpc c++ cmake에서 다중 경로에서 입력된 파일에 대한 처리를 할 수 있도록 개선
* **cmake**: mysql이 설치되어 있는지를 점검하여 관련 라이브러리를 가져올 수 있도록 구성하는 모듈 추가

### Enhancements
* **config**: python config 라이브러리가 python2.7 과 python3를 동시에 지원하도록 개선

### Breaking Changes
* **cmake**: grpc cmake에서 c++ 코드를 생성할 때, `PROTOBUF_IMPORT_DIRS`에 현재의 디렉토리도 추가되어야 합니다. 이 과정이 수행되어야만 정상적으로 빌드됩니다.
* **namespace**: 모든 namespace가 minds에서 maum으로 변경되었습니다. 이 변경으로 인해서 하위 호환성이 근본적으로 사라지게 됩니다. 기존 minds에 기반한 코드들은 모두 다시 작성되어야 합니다. 특히 python 코드가 영향을 많이 받을 것입니다.
* **namespace**: 기존의 MINDS_ROOT 라는 환경변수가 `MAUM_ROOT`로 변경되었습니다. 모든 스크립트는 이에 기반하여 재작성되어야 합니다.


<a name="1.0"></a>
# [1.0.0](https://github.com/mindslab-ai/libmaum/compare/48e973d...v1.0.0) (2017-10-27)

### Bug Fixes

* **config:** HOME 환경변수를 사용하지 않도록 변경 ([#15](https://github.com/mindslab-ai/libmaum/issues/15)) ([4125a93](https://github.com/mindslab-ai/libmaum/commit/4125a93))

### Features

* **setup:** 공통 설정 도구  [#39](https://github.com/mindslab-ai/libmaum/issues/39) [#38](https://github.com/mindslab-ai/libmaum/issues/38) [4eb4d0b](https://github.com/mindslab-ai/libmaum/commit/4eb4d0b)
* **setup:** 공통 설정 도구를 위한 recipe 정의
* **third-party:** grpc, protobuf 내장
* **third-party:** gprc 1.4.2로 업그레이드
* **third-party:** grpc cli를 grpc 빌드에 포함
* **third-party:** supervisor 3.3.0 내장
* **c++-lib:** spdlog 라이브러리 정의
* **c++-lib:** json 라이브러리 정의
* **c++-lib:** spdlog 라이브러리
* **c++-lib:** c++ 공통 config 라이브러리
* **c++-lib:** c++ 공통 config 라이브러리를 위한 테스트 작성
* **python-lib:** python 용 config 라이브러리
* **node-lib:** nodejs 용 config 라이브러리 [#46](https://github.com/mindslab-ai/libmaum/issues/46) [d87dba7](https://github.com/mindslab-ai/libmaum/commit/d87dba7)
* **cmake:** gitversion cmake에 추가 [#50](https://github.com/mindslab-ai/libmaum/issues/50) [aa9df85](https://github.com/mindslab-ai/libmaum/commit/aa9df85)
* **cmake:** 외부 binary를 로딩할 수 있는 import.cmake 정의
* **cmake:** proto를 c++ 또는 python으로 정의할 수 있는 grpc.cmake 정의
* **supervisor:** 기본 supervisor 설정 파일 정의
* **build:** gcc 4.8 과 gcc 5.x에 동시에 빌드될 수 있는 구조
* **build:** third-party 도구는 재빌드가 하지 않도록 최소화 처리

### Enhancements

None - Initial release


### Breaking Changes

None
