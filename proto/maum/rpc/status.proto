/*
 * maum.ai 표준 응답코드 체계
 *
 *
 */
syntax = "proto3";

package maum.rpc;

import "google/protobuf/any.proto";

/**
 * 마인즈랩 제품
 * {module}{proc}{hyphen}{code}{error_index}
 * 2 + 2 + 1 + 3 + 5 : 13자리
 */
enum Module {
  // 00 ~ 20 : production
  LIBMAUM = 0;
  M2U = 1;
  BIZ = 2;
  MLT = 3;
  API = 4;

  // 20 ~ 99 : brain engine
  BRAIN_STT = 20;
  BRAIN_STT_TRAIN = 21;
  BRAIN_TA = 22;
  BRAIN_TA_TRAIN = 23;
  BRAIN_SDS = 24;
  BRAIN_SDS_TRAIN = 25;
  BRAIN_MRC = 26;
  BRAIN_MRC_TRAIN = 27;
  BRAIN_BQA = 28;
  BRAIN_EEV = 30;

  // Undefined : default value
  UNDEFINED = 9999;

  // out of maum.ai
  EXTERNAL = 10000;
}

/**
 * M2u 프로세스 목록
 */
enum ProcessOfM2u {
  M2U_HZC = 0;
  M2U_POOL = 1;
  M2U_MAP = 2;
  M2U_ROUTER = 3;
  M2U_FRONT = 4;
  M2U_ITF = 5;
  M2U_DA_V3 = 6;
  M2U_DA_V4 = 7;
  M2U_SVCADM = 21;
  M2U_LOGGER = 22;
  M2U_ITFM = 23;
  M2U_DAM = 24;
  M2U_REST = 25;
  M2U_MAP_EXT_AUTH = 51;
  M2U_MAP_EXT_STT = 52;
  M2U_MAP_EXT_TTS = 53;
}

/**
 * Biz 프로세스 목록
 */
enum ProcessOfBiz {
  BIZ_CAPD = 0;
  BIZ_MRCP = 1;
  BIZ_PIPE = 2;
  BIZ_RTTD = 3;
}

/**
 * Mlt 프로세스 목록
 */
enum ProcessOfMlt {
  MLT_UNKNOWN = 0;
}

/**
 * Api 프로세스 목록
 */
enum ProcessOfApi {
  API_UNKNOWN = 0;
}

/**
 * Stt 프로세스 목록
 */
enum ProcessOfBrainStt {
  BRAIN_STT_MAIN = 0;
  BRAIN_STT_SUB = 1;
  BRAIN_STT_EEV = 2;
}

/**
 * stt train 프로세스 목록
 */
enum ProcessOfBrainSttTrain {
  BRAIN_STT_TRAIN_MAIN = 0;
  BRAIN_STT_TRAIN_CHILD = 1;
}

/**
 * ta 프로세스 목록
 */
enum ProcessOfBrainTa {
  BRAIN_NLP_KOR1 = 0;
  BRAIN_NLP_KOR2 = 1;
  BRAIN_NLP_ENG2 = 3;
  BRAIN_NLP_KOR3 = 4;
  BRAIN_NLP_CL = 5;
  BRAIN_HMD = 11; // C++ server
  BRAIN_WE = 21;
}

/**
 * ta train 프로세스 목록
 */
enum ProcessOfBrainTaTrain {
  BRAIN_TA_CLTRAIN = 0;
  BRAIN_TA_CLTRAIN_CHILD = 1;
}

/**
 * sds 프로세스 목록
 */
enum ProcessOfBrainSds {
  BRAIN_SDS_MAIN = 0;
  BRAIN_SDS_EXT_KOR = 1;
  BRAIN_SDS_INT_KOR = 2;
  BRAIN_SDS_EXT_ENG = 3;
  BRAIN_SDS_INT_ENG = 4;
  BRAIN_SDS_EDU_ENG = 5;
}

/**
 * sds train 프로세스 목록
 */
enum ProcessOfBrainSdsTrain {
  BRAIN_SDS_TRAIN_MAIN = 0;
  BRAIN_SDS_TRAIN_CHILD = 1;
}

/**
 * mrc 프로세스 목록
 */
enum ProcessOfBrainMrc {
  BRAIN_MRC_MAIN = 0;
  BRAIN_MRC_CHILD = 1;
}

/**
 * mrc train 프로세스 목록
 */
enum ProcessOfBrainMrcTrain {
  BRAIN_MRC_TRAIN_MAIN = 0;
  BRAIN_MRC_TRAIN_CHILD = 1;
}

/**
 * bqa 프로세스 목록
 */
enum ProcessOfBrainBqa {
  BRAIN_BQA_MAIN = 0;
}

/**
 * eev 프로세스 목록
 */
enum ProcessOfBrainEev {
  BRAIN_EEV_MAIN = 0;
}

/**
 *  ExCode 종류
 */
enum ExCode {
  SUCCESS = 0;
  /*
   * 300, CLIENT ORIGIN ERROR
   */

  // 인증실패 및 권한 획득 실패
  AUTHORIZATION_FAILED = 301;
  // 사용자 입력 데이터 오류
  INVALID_ARGUMENT = 302;

  /*
   * SERVER PROCESSING ERROR
   */
  // 리소스 없음
  NOT_FOUND = 400;
  FILE_NOT_FOUND = 401;
  DB_KEY_NOT_FOUND = 402;
  MEM_KEY_NOT_FOUND = 403;
  MQ_NOT_FOUND = 404;

  ALREADY_EXISTS = 410;
  FILE_ALREADY_EXISTS = 411;
  DB_KEY_ALREADY_EXISTS = 412;
  MEM_KEY_ALREADY_EXISTS = 413;
  MQ_ALREADY_EXISTS = 414;

  FAILED_PRECONDITION = 420;
  LOGICAL_ERROR = 421;
  CONDITION_ERROR = 422;
  ASSERTION_ERROR = 423;
  RUNTIME_ERROR = 424;

  RESOURCE_EXHAUSTED = 430;
  DISK_RESOURCE_EXHAUSTED = 431;
  OUT_OF_MEMORY = 432;
  TOO_MANY_REQUEST = 433;
  QUOTA_EXCEEDED = 434;

  OUT_OF_RANGE = 440;
  INDEX_OUT_OF_RANGE = 441;

  DATA_LOSS = 450;
  SYSTEM_CRASHED = 451;
  OS_ERROR = 452;
  PERMISSION_DENIED = 453;
  NETWORK_SYSTEM_ERROR = 454;
  DB_CRASHED = 455;
  MQ_CRASHED = 456;

  ABORTED = 500;
  REMOTE_CALL_GRPC_ERROR = 501;
  REMOTE_CALL_REST_ERROR = 502;
  REMOTE_CALL_HTTP_ERROR = 503;
  REMOTE_CALL_MQ_ERROR = 504;
  REMOTE_CALL_TCP_ERROR = 505;
  REMOTE_CALL_UDP_ERROR = 506;
  REMOTE_CALL_ETC_ERROR = 507;
}

/**
 * ResultStatus 결과
 */
message ResultStatus {
  // 2자리
  Module module = 1;
  // 2자리
  int32 process = 2;
  // 3자리
  ExCode code = 3;
  // 5자리
  // 1 ~ 99999 까지 정의가능
  int32 error_index = 4;
  // 상세메시지
  string message = 11;

  // A list of messages that carry the error details.  There is a common set of
  // message types for APIs to use.
  repeated google.protobuf.Any details = 100;
}

/**
 * grpc Status의 `error_details` 변수에 바이너리 형태로 마샬링되어서 전송되는
 * 객체. 실지로는 `grpc-status-details-bin` trailing meta 값으로 전달된다.
 *
 * 네트워크 호출 스택 구조를 반영하여 하위 호출의 에러를 그대로 담을 수 있다.
 */
message ResultStatusList {
  repeated ResultStatus list = 1;
}
