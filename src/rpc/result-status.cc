//#include <stdio.h>
#include <cstdarg>
#include "libmaum/rpc/result-status.h"

using maum::rpc::Module;
using maum::rpc::DebugInfo;
using maum::rpc::LocalizedMessage;
using maum::rpc::ResultStatus;
using maum::rpc::ResultStatusList;

namespace libmaum {
namespace rpc {

// 내부에서만 사용하는 함수
/**
 * ExCode에 해당하는 Grpc의 StatusCode를 가져온다.
 *
 * @param code ExCode
 * @return Grpc StatusCode
 */
StatusCode GetStatusCode(const ExCode code) {

  switch ((int) code) {
    case ExCode::SUCCESS: return StatusCode::OK;
    case ExCode::AUTHORIZATION_FAILED: return StatusCode::UNAUTHENTICATED;
    case ExCode::INVALID_ARGUMENT: return StatusCode::INVALID_ARGUMENT;
    case ExCode::PERMISSION_DENIED: return StatusCode::PERMISSION_DENIED;
  }

  switch ((int) code / 10) {
    case (ExCode::NOT_FOUND / 10): return StatusCode::NOT_FOUND;
    case (ExCode::ALREADY_EXISTS / 10): return StatusCode::ALREADY_EXISTS;
    case (ExCode::FAILED_PRECONDITION / 10): {
      return StatusCode::FAILED_PRECONDITION;
    }
    case (ExCode::RESOURCE_EXHAUSTED / 10): {
      return StatusCode::RESOURCE_EXHAUSTED;
    }
    case (ExCode::OUT_OF_RANGE / 10): return StatusCode::OUT_OF_RANGE;
    case (ExCode::DATA_LOSS / 10): return StatusCode::DATA_LOSS;
    case (ExCode::ABORTED / 10): return StatusCode::ABORTED;
  }

  return StatusCode::UNKNOWN;
}

LocalizedMessage BuildLocalizedMessage(const string &message,
                                       const string &locale = "en-US") {
  LocalizedMessage localized_message;

  localized_message.set_locale(locale);
  localized_message.set_message(message);

  return localized_message;
}

/**
 * status detail을 ResultStatusListf로 parsing한다.
*/
inline bool ParseResultStatusList(const Status &status,
                                  ResultStatusList &status_list) {
  if (status.error_details().empty()) {
    return false;
  }

  status_list.ParseFromString(status.error_details());

  return status_list.list_size() > 0;
}

inline const int GetResultCodeFromResultStatus(const ResultStatus &rs) {
  return rs.code() * 100000 + rs.error_index();
}

ostringstream &operator<<(ostringstream &oss,
                          const ResultStatus &status) {
  oss << std::setw(2) << std::setfill('0') << status.module()
      << std::setw(2) << std::setfill('0') << status.process() << "-"
      << std::setw(3) << std::setfill('0') << status.code()
      << std::setw(5) << std::setfill('0') << status.error_index()
      << ' ' << ExCode_Name(status.code());
  if (!status.message().empty()) {
    oss << ", " << status.message();
  }

  if (status.details().size() > 0) {
    LocalizedMessage localizedMessage;
    int i = 0;

    oss << ", [";
    for (auto detail : status.details()) {
      if (detail.Is<LocalizedMessage>()) {
        detail.UnpackTo(&localizedMessage);
        oss << localizedMessage.message();
      } else {
        oss << detail.Utf8DebugString();
      }
      i++;
      if (i < status.details().size()) {
        oss << ", ";
      }
    }
    oss << "]";
  }

  return oss;
}

// CommonResultStatus static 변수 및 함수
Module CommonResultStatus::module_ = Module::UNDEFINED;
int CommonResultStatus::process_ = -1;

/**
 * 프로세스 전역으로 사용할 모듈과 프로세스 코드를 지정한다.
 *
 * @param module 모듈 코드
 * @param process 프로세스 코드
 *
 */
void CommonResultStatus::SetModuleAndProcess(const Module module,
                                             const int process) {
  module_ = module;
  process_ = process;
}

/**
 * Grpc Status로 부터 error 문자열을 만든다.
 *
 * @param error 문자열로 변환할 Grpc Status
 */
const string CommonResultStatus::GetErrorString(const Status &status) {
  ResultStatusList status_list;
  ostringstream ss;

  // Parsing 성공하면 debug string return
  if (ParseResultStatusList(status, status_list)) {
    int size = status_list.list_size();
    int i = 0;
    for (auto &result : status_list.list()) {
      ss << "[" << i++ << "] ";
      ss << result;
      if (i < size) {
        ss << "\n";
      }
    }
  } else {
    // 실패하면 grpc error로 return
    ss << "GRPC(" << status.error_code() << ")";
    if (!status.error_message().empty()) {
      ss << " message: " << status.error_message();
      if (!status.error_details().empty()) {
        ss << " detail: " << status.error_details();
      }
    }
  }

  return ss.str();
}

/**
 * Grpc Status로 부터 result code를 가져 온다.
 *
 * @param result code를 가져 올 Grpc Status
 * @return 8자리의 result code integer
 */
const int CommonResultStatus::GetResultCode(const Status &status) {
  ResultStatusList status_list;

  // status detail을 ResultStatusListf로 parsing한다.
  // 성공하면
  if (ParseResultStatusList(status, status_list)) {
    return GetResultCodeFromResultStatus(status_list.list(0));
  } else {
    // 실패하면 grpc error로 처리
    return status.ok() ?
           0 :
           ExCode::REMOTE_CALL_GRPC_ERROR * 100000 + status.error_code();
  }
}

/**
 * Grpc Status로 부터 프로세스 내에서 기인한 result code 인지 검사한다.
 *
 * @param result code를 가져 올 Grpc Status
 * @return 프로세스 내부에서 기인한 경우 true를 리턴한다.
 */
const bool CommonResultStatus::IsInProcessResult(const Status &status) {
  ResultStatusList status_list;

  // status detail을 ResultStatusListf로 parsing한다.
  // 성공하면, 최상위 result status의 module, process 비교하여 결과 리턴
  if (ParseResultStatusList(status, status_list)) {
    auto &rs = status_list.list(0);
    return rs.module() == module_ && rs.process() == process_;
  } else {
    // 실패하면 grpc status 이므로 false 리턴
    return false;
  }
}

/**
 * Grpc Status로 부터 프로세스 내의 원인 result code를 가져 온다.
 *
 * @param result code를 가져 올 Grpc Status
 * @return 8자리의 result code integer
 */
const int CommonResultStatus::GetProcessOriginResultCode(const Status &status) {
  ResultStatusList status_list;

  // status detail을 ResultStatusListf로 parsing한다.
  // 성공하면
  if (ParseResultStatusList(status, status_list)) {
    int index = 0;
    int count = status_list.list_size() - 1;
    for (; index < count; ++index) {
      auto &rs = status_list.list(index + 1);
      if (rs.module() != module_ && rs.process() != process_) {
        break;
      }
    }

    return GetResultCodeFromResultStatus(status_list.list(index));
  } else {
    // 실패하면 grpc error로 처리
    return status.ok() ?
           0 :
           ExCode::REMOTE_CALL_GRPC_ERROR * 100000 + status.error_code();
  }
}


// CommonResultStatus 멤버 함수
/**
 * CommonResultStatus 생성자
 *
 * @param ex_code 에러코드
 * @param error_index 프로세스 내 상세코드
 * @param format 에러 메시지 포맷 지정
 * @param ... 에러 메시지 Arguments
 *
 */
CommonResultStatus::CommonResultStatus(const ExCode ex_code,
                                       int error_index,
                                       const char *format, ...) {
  auto status = status_list_.mutable_list()->Add();
  status->set_module(module_);
  status->set_process(process_);
  status->set_code(ex_code);
  status->set_error_index(error_index);
  {
    char buffer[1024];

    va_list args;
    va_start(args, format);
    vsnprintf(buffer, 1024, format, args);
    va_end(args);
    status->set_message(string(buffer));
  }
}

/**
 * ResultStatus의 Detail 문자열 추가
 */
CommonResultStatus &CommonResultStatus::AddDetail(const string &message,
                                                  const string &locale) {
  return AddDetail(BuildLocalizedMessage(message, locale));
}

/**
 * ResultStatus의 Detail message 추가
 */
CommonResultStatus &CommonResultStatus::AddDetail(const Message &message) {
  status_list_.mutable_list(0)->add_details()->PackFrom(message);

  return *this;
}

/**
 * ResultStatus에 debug info를 추가한다.
 */
CommonResultStatus &CommonResultStatus::AddDebugInfo(const vector<string> &entries,
                                                     const string &detail) {
  DebugInfo debug_info;

  for (auto &entry : entries) {
    debug_info.mutable_stack_entries()->AddAllocated(new string(entry));
  }

  if(!detail.empty()) {
    debug_info.set_detail(detail);
  }

  status_list_.mutable_list(0)->add_details()->PackFrom(debug_info);

  return *this;
}

/**
 * 이전 호출의 Grpc Status를 추가한다.
 *
 * @param status 이전 call에서 반환한 Ggrp Status
 *
 */
CommonResultStatus &CommonResultStatus::StackStatus(const Status &status) {
  ResultStatusList status_list;

  // status detail을 ResultStatusListf로 parsing한다.
  // 성공하면 merge
  if (ParseResultStatusList(status, status_list)) {
    status_list_.MergeFrom(status_list);
  } else {
    // 실패하면 grpc error로 저장
    auto grpc_status = status_list_.mutable_list()->Add();
    grpc_status->set_module(module_);
    grpc_status->set_process(process_);
    grpc_status->set_code(ExCode::REMOTE_CALL_GRPC_ERROR);
    grpc_status->set_error_index(status.error_code());
    grpc_status->set_message(status.error_message());
    if (!status.error_details().empty()) {
      grpc_status->mutable_details()->Add()
          ->PackFrom(BuildLocalizedMessage(status.error_details()));
    }
  }

  return *this;
}

/**
 * ResultStatus를 통해 error code generation
 */
const string CommonResultStatus::GetErrorString() {
  ostringstream ss;

  ss << status_list_.list(0);

  return ss.str();
}

/**
 * Grpc Status로 변환하여 생성한다.
 */
Status CommonResultStatus::GetStatus() {
  auto status = status_list_.list(0);
  StatusCode code = GetStatusCode(status.code());

  return Status(code, status.message(), status_list_.SerializeAsString());
}

}
}
