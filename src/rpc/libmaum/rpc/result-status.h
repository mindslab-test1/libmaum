#ifndef LIB_RESULT_STATUS_H
#define LIB_RESULT_STATUS_H
#include <string>
#include <sstream>
#include <cstdlib>
#include <iomanip>
#include <grpc++/impl/codegen/status_code_enum.h>
#include <grpc++/support/config.h>
#include <grpc++/grpc++.h>
#include <maum/rpc/status.pb.h>
#include <maum/rpc/error_details.pb.h>

using std::vector;
using std::string;
using std::ostringstream;
using std::unique_ptr;
using grpc::StatusCode;
using grpc::Status;
using google::protobuf::Message;
using maum::rpc::ExCode;

namespace libmaum {
namespace rpc {

/**
 * MAUM.ai 시스템 전체에 사용되는 설정 파일 관리 클래스
 *
 * 자바 프로퍼티 스타일의 파일이나 JSON 스타일의 설정 파일을 로딩할 수 있고
 * 프로퍼티 이름으로 정보를 가져올 수 있습니다.
 */
class CommonResultStatus {
 public:
  static void SetModuleAndProcess(const maum::rpc::Module module,
                                  const int process);
  static const string GetErrorString(const Status &status);
  static const int GetResultCode(const Status &status);
  static const bool IsInProcessResult(const Status &status);
  static const int GetProcessOriginResultCode(const Status &status);

  CommonResultStatus() = delete;
  CommonResultStatus &operator=(const CommonResultStatus &) = delete;
  CommonResultStatus(const maum::rpc::ExCode ex_code,
                     int error_index,
                     const char *format, ...);
  virtual ~CommonResultStatus() = default;

  /**
 * ResultStatus에 Detail 문자열 추가
 */
  CommonResultStatus &AddDetail(const string &message,
                                const string &locale = "en-US");

  /**
   * ResultStatus에 Detail message 추가
   */
  CommonResultStatus &AddDetail(const Message &message);

  /**
   * ResultStatus에 Detail 여러 문자열 추가
   */
  inline CommonResultStatus &AddDetails(const vector<string> &details) {
    for (auto &detail : details) {
      AddDetail(detail);
    }
    return *this;
  }

  /**
   * ResultStatus에 debug info를 추가한다.
   */
  CommonResultStatus &AddDebugInfo(const vector<string> &entries,
                                   const string &detail = string());

  /**
   * 이전 호출의 Grpc Status를 추가한다.
   */
  CommonResultStatus &StackStatus(const Status &status);

  /**
   * ResultStatus를 통해 error code 문자열을 만든다.
   */
  const string GetErrorString();

  /**
   * Grpc Status로 변환하여 생성한다.
   */
  Status GetStatus();

 private:
  static maum::rpc::Module module_;
  static int process_;

  maum::rpc::ResultStatusList status_list_;
};
}
}

#define ERROR_INDEX(minor) (ERROR_INDEX_MAJOR + minor)

#define GetResultStatus(code, error_index, format, ...) \
  libmaum::rpc::CommonResultStatus(code, error_index, format, ##__VA_ARGS__)

#define ResultGrpcStatus(code, error_index, format, ...) \
  libmaum::rpc::CommonResultStatus(code, error_index, format, ##__VA_ARGS__) \
    .GetStatus();

#define GetStackStatus(status, code, error_index, format, ...) \
  libmaum::rpc::CommonResultStatus(code, error_index, format, ##__VA_ARGS__) \
    .StackStatus(status)

#define StackGrpcStatus(status, code, error_index, format, ...) \
  libmaum::rpc::CommonResultStatus(code, error_index, format, ##__VA_ARGS__) \
    .StackStatus(status) \
    .GetStatus();

#endif
