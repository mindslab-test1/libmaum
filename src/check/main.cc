#include <iostream>
#include <string.h>
#include <gitversion/version.h>
#include <libmaum/common/util.h>

using namespace std;

int main(int argc, char **argv) {
  cout << " " << basename(argv[0]) << " " << version::VERSION_STRING << endl
       << "  └ " << GetGitVersionString() << endl;

  return 0;
}
