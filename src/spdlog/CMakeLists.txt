cmake_minimum_required(VERSION 2.8.12)

project(maum-spdlog CXX)

include_directories(.)

## 이전 버전이 설치되어 있을 수 있으므로 먼저 지운다.
file(REMOVE_RECURSE ${CMAKE_INSTALL_PREFIX}/include/spdlog)

install(DIRECTORY spdlog/ DESTINATION include/spdlog
    FILES_MATCHING PATTERN "*")
