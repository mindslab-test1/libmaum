#include <jni.h>
#include <stdio.h>
#include <stdbool.h>

extern bool CheckConfig(const char *prod_name);

JNIEXPORT void JNICALL
Java_ai_maum_libmaum_CheckConfig_check(JNIEnv *env,
                                       jobject obj,
                                       jstring productName) {
  const char *prod_name = (*env)->GetStringUTFChars(env, productName, 0);
  CheckConfig(prod_name);
  (*env)->ReleaseStringUTFChars(env, productName, prod_name);
}

