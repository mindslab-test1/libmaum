#include <unistd.h>
#include <fstream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <assert.h>
#include <fstream>
#include <regex>
#include <gitversion/version.h>

#include "libmaum/common/config.h"
#include "libmaum/common/util.h"

using namespace std;

#define MAX_PHYSICAL_ID  31
#define MAX_CPU_NUMBER  (MAX_PHYSICAL_ID + 1)

string GetGitVersionString() {
  return string("maum-common ") + string(version::VERSION_STRING);
}

std::vector<std::string> split(const std::string &str,
                               int delimiter(int)) {
  std::vector<std::string> result;
  auto e = str.end();
  auto i = str.begin();
  while (i != e) {
    i = std::find_if_not(i, e, delimiter);
    if (i == e) break;
    auto j = std::find_if(i, e, delimiter);
    result.push_back(std::string(i, j));
    i = j;
  }
  return result;
}

bool mkpath(const std::string &path) {
  bool succ = false;
  int rc = ::mkdir(path.c_str(), 0775);
  if (rc == -1) {
    switch (errno) {
      case ENOENT:
        //parent didn't exist, try to create it
        if (mkpath(path.substr(0, path.find_last_of('/'))))
          //Now, try to create again.
          succ = 0 == ::mkdir(path.c_str(), 0775);
        else
          succ = false;
        break;
      case EEXIST:
        //Done!
        succ = true;
        break;
      default:
        succ = false;
        break;
    }
  } else
    succ = true;
  return succ;
}

void DumpPid(const char *dir, const char *program) {
  pid_t pid = getpid();

  std::string filename(dir);
  filename += '/';
  filename += program;
  filename += ".pid";

  mkpath(dir);
  std::ofstream o(filename);

  o << int(pid) << std::endl;
}

// random port generation (range : 10000~65535)
int GenerateRandomPort() {
  srand((unsigned int) time(NULL));
  int port = rand() % 55536 + 10000;
  return port;
}

#include <stdio.h>
#include <ifaddrs.h>
#include <netpacket/packet.h>
#include <iostream>
#include <netdb.h>

string GetMacAddress(const char *ipaddr) {
  struct ifaddrs *ifaddr = nullptr;

  if (getifaddrs(&ifaddr) < 0) {
    std::cerr << "cannot get if addrs" << std::endl;
    return "";
  }
  char host[NI_MAXHOST];
  string if_name;
  for (struct ifaddrs *ifa = ifaddr; ifa != nullptr; ifa = ifa->ifa_next) {
    auto family = ifa->ifa_addr->sa_family;
    if (family == AF_INET || family == AF_INET6) {
      int s = getnameinfo(ifa->ifa_addr,
                          (family == AF_INET) ?
                          sizeof(struct sockaddr_in) :
                          sizeof(struct sockaddr_in6),
                          host, NI_MAXHOST,
                          nullptr, 0, NI_NUMERICHOST);
      if (s != 0) {
        std::cerr << "getnameinfo() failed: " << gai_strerror(s) << std::endl;
        continue;
      }
      if (strcmp(ipaddr, host) == 0) {
        if_name = ifa->ifa_name;
        break;
      }
    }
  }

  string out;
  for (struct ifaddrs *ifa = ifaddr; ifa != nullptr; ifa = ifa->ifa_next) {
    if (ifa->ifa_addr->sa_family == AF_PACKET &&
        if_name == ifa->ifa_name) {
      struct sockaddr_ll *s = (struct sockaddr_ll *) ifa->ifa_addr;
      for (auto i = 0; i < s->sll_halen; i++) {
        out += fmt::format("{0:02X}", (s->sll_addr[i]));
        if ((i + 1) != s->sll_halen)
          out += ':';
      }
      break;
    }
  }
  freeifaddrs(ifaddr);
  return out;
}

void GetPrimaryIp(char *buffer, size_t buflen) {
  assert(buflen >= 16);

  int sock = socket(AF_INET, SOCK_DGRAM, 0);
  assert(sock != -1);

  const char *kGoogleDnsIp = "8.8.8.8";
  uint16_t kDnsPort = 53;
  struct sockaddr_in serv;
  memset(&serv, 0, sizeof(serv));
  serv.sin_family = AF_INET;
  serv.sin_addr.s_addr = inet_addr(kGoogleDnsIp);
  serv.sin_port = htons(kDnsPort);

  int err = connect(sock, (const sockaddr *) &serv, sizeof(serv));
  assert(err != -1);

  sockaddr_in name;
  socklen_t name_len = sizeof(name);
  err = getsockname(sock, (sockaddr *) &name, &name_len);
  assert(err != -1);

  const char *p = inet_ntop(AF_INET, &name.sin_addr, buffer, buflen);
  assert(p);

  close(sock);
}

bool CheckPort(const char *ip, int port) {
  if (port < 0) {
    LOGGER()->debug("binding port failed {}", port);
    return true;
  }
  struct sockaddr_in serv;
  int sock = socket(AF_INET, SOCK_STREAM, 0);
  if (sock < 0) {
    LOGGER()->debug("Error opening socket {}", errno);
  }

  bzero((char *) &serv, sizeof(serv));
  serv.sin_family = AF_INET;
  serv.sin_addr.s_addr = inet_addr(ip);
  serv.sin_port = htons(port);

  if (bind(sock, (struct sockaddr *) &serv, sizeof(serv)) != -1) {
    LOGGER()->debug("cannot bind {}", errno);
    close(sock);
    return false;
  } else {
    close(sock);
    if (errno == EADDRINUSE) {
      LOGGER()->debug("port is used {}", port);
    } else {
      LOGGER()->debug("get another errno, check another status {}", errno);
    }
    return true;
  }
}

int32_t GetRandomTcpPort() {
  int sock = socket(AF_INET, SOCK_STREAM, 0);
  assert(sock != -1);

  struct sockaddr_in serv;
  memset(&serv, 0, sizeof(serv));
  serv.sin_family = AF_INET;
  serv.sin_addr.s_addr = inet_addr("0.0.0.0");
  serv.sin_port = htons(0);

  int err = bind(sock, (const sockaddr *) &serv, sizeof(serv));
  if (err != 0) {
    if (errno == EADDRINUSE) {
      LOGGER()->debug("bind error: Address {} is used, {}",
                      serv.sin_port,
                      errno);
    } else {
      LOGGER()->debug("bind error: {}, {}", err, errno);
    }
    return -1;
  }

  sockaddr_in port;
  socklen_t port_len = sizeof(port);
  err = getsockname(sock, (sockaddr *) &port, &port_len);

  if (err == -1) {
    LOGGER()->debug("getsockname error: {}, {}", err, errno);
    close(sock);
    return -1;
  }

  close(sock);
  return ntohs(port.sin_port);
}

int32_t GetRandomTcpPort(int min_port, int max_port) {
  static bool do_srand = true;
  int sock = socket(AF_INET, SOCK_STREAM, 0);
  assert(sock != -1);

  struct sockaddr_in serv;
  memset(&serv, 0, sizeof(serv));
  serv.sin_family = AF_INET;
  serv.sin_addr.s_addr = inet_addr("0.0.0.0");
  if (do_srand) {
    srand((unsigned int) time(NULL));
    do_srand = false;
  }
  unsigned int new_port = rand() % (max_port - min_port) + min_port;
  serv.sin_port = htons(new_port);

  int err = bind(sock, (const sockaddr *) &serv, sizeof(serv));
  if (err != 0) {
    if (errno == EADDRINUSE) {
      LOGGER()->debug("bind error: Address {} is used, {}", new_port, errno);
    } else {
      LOGGER()->debug("bind error: {}, {}", err, errno);
    }
    return -1;
  }

  sockaddr_in port;
  socklen_t port_len = sizeof(port);
  err = getsockname(sock, (sockaddr *) &port, &port_len);

  if (err == -1) {
    LOGGER()->debug("getsockname error: {}, {}", err, errno);
    close(sock);
    return -1;
  }

  close(sock);
  return ntohs(port.sin_port);
}

bool GetCpuCores(int &cpus, int &total_cores) {
  cpus = total_cores = 0;
  stringstream cpuinfo_stream;

  {
    ifstream ifs;
    ifs.open("/proc/cpuinfo", fstream::in);
    cpuinfo_stream << ifs.rdbuf();
    ifs.close();
  }

  int cpu_matrix[MAX_CPU_NUMBER] = {0,};
  try {
    regex pid_regex("physical id(\t| )+:.*", regex_constants::awk);
    regex cores_regex("cpu cores(\t| )+:.*", regex_constants::awk);
    int physical_id = -1;
    int cpu_cores = 0;
    string line;

    while (getline(cpuinfo_stream, line)) {
      trim(line);
      std::transform(line.begin(), line.end(), line.begin(), ::tolower);
      if (regex_match(line, pid_regex)) {
        physical_id = atoi(line.substr(line.rfind(':') + 1).c_str());
        if (physical_id > MAX_PHYSICAL_ID) {
          cerr << fmt::format("Physical ID({}) is too big.", physical_id)
               << endl;
          return false;
        }
        if (cpu_cores > 0) {
          if (cpu_matrix[physical_id] == 0) {
            cpu_matrix[physical_id] = cpu_cores;
          } else if (cpu_matrix[physical_id] != cpu_cores) {
            cerr << fmt::format("CPU Cores are mismatch of Physical ID({}).",
                                physical_id) << endl;
            return false;
          }
        }
      } else if (regex_match(line, cores_regex)) {
        int cores = atoi(line.substr(line.rfind(':') + 1).c_str());
        if (cores > 0) {
          if (physical_id >= 0) {
            if (cpu_matrix[physical_id] == 0) {
              cpu_matrix[physical_id] = cores;
            } else if (cpu_matrix[physical_id] != cores) {
              cerr << fmt::format("CPU Cores are mismatch of Physical ID({}).",
                                  physical_id) << endl;
              return false;
            }
          }
          cpu_cores = cores;
        } else {
          cerr << fmt::format("CPU Cores({}) is invalid value.", cores) << endl;
          return false;
        }
      } else if (line.empty()) {
        physical_id = -1;
        cpu_cores = 0;
      }
    }
  } catch (const regex_error &e) {
    cerr << "regex_error caught: " << e.what() << ", code: " << e.code()
         << endl;
    return false;
  } catch (const out_of_range &e) {
    cerr << "Exception: " << e.what() << endl;
    return false;
  }

  for (int cores: cpu_matrix) {
    if (cores > 0) {
      ++cpus;
      total_cores += cores;
    }
  }

  return true;
}
