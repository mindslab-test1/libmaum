#ifndef LIBMAUM_COMMON_INTERVAL_LOGGER_H__
#define LIBMAUM_COMMON_INTERVAL_LOGGER_H__

#include <spdlog/spdlog.h>
#include <spdlog/sinks/base_sink.h>
#include <spdlog/common.h>

#include <libmaum/common/util.h>
#include <spdlog/details/file_helper.h>
#include <spdlog/async.h>

using spdlog::filename_t;

struct IntervalFileNameCalculator {
  // Create filename for the form basename.YYYY-MM-DD
  static filename_t calc_filename(const filename_t &basename,
                                  const filename_t &prefix,
                                  int interval) {
    auto now = std::chrono::system_clock::now();
    time_t tnow = std::chrono::system_clock::to_time_t(now);
    // if t_next has some remained modular value, trim it.
    tnow -= (tnow % (interval * 60));

    std::tm tm = spdlog::details::os::localtime(tnow);
    std::conditional<std::is_same<filename_t::value_type, char>::value,
                     fmt::memory_buffer, fmt::wmemory_buffer>::type d;
    fmt::format_to(d, SPDLOG_FILENAME_T("{}/{:04d}{:02d}{:02d}"),
                   basename,
                   tm.tm_year + 1900,
                   tm.tm_mon + 1,
                   tm.tm_mday,
                   prefix,
                   tm.tm_year + 1900,
                   tm.tm_mon + 1,
                   tm.tm_mday,
                   tm.tm_hour,
                   tm.tm_min);
    mkpath(fmt::to_string(d));

    std::conditional<std::is_same<filename_t::value_type, char>::value,
                     fmt::memory_buffer, fmt::wmemory_buffer>::type w;
    fmt::format_to(w,
                   SPDLOG_FILENAME_T(
                       "{}/{:04d}{:02d}{:02d}/{}.{:04d}{:02d}{:02d}{:02d}{:02d}.log"),
                   basename,
                   tm.tm_year + 1900,
                   tm.tm_mon + 1,
                   tm.tm_mday,
                   prefix,
                   tm.tm_year + 1900,
                   tm.tm_mon + 1,
                   tm.tm_mday,
                   tm.tm_hour,
                   tm.tm_min);
    return fmt::to_string(w);
  }
};

/*
 * Rotating file sink based on interval, minitutes = 1, 5, 10
 */
template<class Mutex, class FileNameCalc = IntervalFileNameCalculator>
class IntervalSink : public spdlog::sinks::base_sink<Mutex> {
 public:
  //create daily file sink which rotates on given time
  IntervalSink(const filename_t &base_filename,
               const std::string &prefix,
               int interval)
      : base_filename_(base_filename),
        prefix_(prefix),
        rotation_interval_(interval) {
    if (interval < 0 || interval > 24 * 60)
      throw spdlog::spdlog_ex("interval_sink: Invalid rotation");
    rotation_tp_ = _next_rotation_tp();
    file_helper_.open(
        FileNameCalc::calc_filename(base_filename_,
                                    prefix_, rotation_interval_));
  }

 protected:
  void sink_it_(const spdlog::details::log_msg &msg) override {
    if (std::chrono::system_clock::now() >= rotation_tp_) {
      file_helper_.open(
          FileNameCalc::calc_filename(base_filename_,
                                      prefix_, rotation_interval_));
      rotation_tp_ = _next_rotation_tp();
    }
    fmt::memory_buffer formatted;
    spdlog::sinks::sink::formatter_->format(msg, formatted);
    file_helper_.write(formatted);
  }

  void flush_() override {
    if (std::chrono::system_clock::now() >= rotation_tp_) {
      file_helper_.open(
          FileNameCalc::calc_filename(base_filename_,
                                      prefix_, rotation_interval_));
      rotation_tp_ = _next_rotation_tp();
    }
    file_helper_.flush();
  }

 private:
  std::chrono::system_clock::time_point _next_rotation_tp() {
    auto now = std::chrono::system_clock::now();
    time_t tnow = std::chrono::system_clock::to_time_t(now);
    // if t_next has some remained modular value, trim it.
    tnow -= (tnow % (rotation_interval_ * 60));

    // if t_next has some remained modular value, trim it.
    time_t t_next = tnow + rotation_interval_ * 60;
#if DEBUG
    printf("now %ld next %ld\n", tnow, t_next);
#endif

    auto rotation_time = std::chrono::system_clock::from_time_t(t_next);
    if (rotation_time > now)
      return rotation_time;
    else
      return std::chrono::system_clock::time_point(
          rotation_time + std::chrono::minutes(rotation_interval_));
  }

  filename_t base_filename_;
  std::string prefix_;
  int rotation_interval_;
  std::chrono::system_clock::time_point rotation_tp_;
  spdlog::details::file_helper file_helper_;
};

typedef IntervalSink<std::mutex> IntervalSinkMt;

using namespace spdlog;
inline std::shared_ptr<spdlog::logger> CreateIntervalLogger(
    const std::string &logger_name,
    const filename_t &base,
    const std::string &prefix,
    int interval) {
  spdlog::sink_ptr sink = std::make_shared<IntervalSinkMt>(base,
                                                           prefix, interval);
  // ^12 = 4096 개만 유지한다.
  return std::make_shared<async_logger>(logger_name,
                                        sink,
                                        spdlog::thread_pool(),
                                        async_overflow_policy::overrun_oldest);
}

#endif //LIBMAUM_COMMON_INTERVAL_LOGGER_H_
