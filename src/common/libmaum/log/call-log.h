#ifndef LIBMAUM_LOG_CALL_LOG_H_
#define LIBMAUM_LOG_CALL_LOG_H_

#include <grpc++/grpc++.h>
#include <google/protobuf/message.h>
#include <spdlog/spdlog.h>

using grpc::ServerContext;
using grpc::Status;
using google::protobuf::Message;
using std::string;
using std::unique_ptr;

namespace libmaum {
namespace log {

struct CallLogRecord {
  CallLogRecord(const char *func,
                const ServerContext *ctx,
                const Message *request,
                const Message *response,
                const Status *status)
      : func_(func),
        ctx_(ctx),
        request_(request),
        response_(response),
        status_(status) {
    clock_gettime(CLOCK_REALTIME, &begin_);
  }

  ~CallLogRecord() {}
  const string &func() {
    return func_;
  }
  const timespec *begin_at() {
    return &begin_;
  }
  const timespec *end_at() {
    return &end_;
  }
  const ServerContext *context() {
    return ctx_;
  }
  void GetCallLogMeta(Message &msg);
  const Message *request() {
    return request_;
  }
  const Message *response() {
    return response_;
  }
  const Status *status() {
    return status_;
  }
  void SetEndAt() {
    clock_gettime(CLOCK_REALTIME, &end_);
  }
 private:
  string func_;
  struct timespec begin_;
  struct timespec end_;
  const ServerContext *ctx_;
  const Message *request_;
  const Message *response_;
  const Status *status_;
};

typedef string (*CallLogHandler)(void *);

class CallLogger {
  // prevent call
  CallLogger();
 public:
  ~CallLogger() {
    stop_ = true;
  }
  static CallLogger *GetInstance();
  void InitLogger(const string &name,
                  const char *logs_dir,
                  const string &process_name,
                  int interval_minute = 5);
  std::shared_ptr<spdlog::logger> GetLogger() {
    return logger_;
  }
  void Log(const string &str) {
    if (logger_) {
      logger_->info(str);
    }
  }
 private:
  void Monitor();
  std::string logs_dir_;
  std::shared_ptr<spdlog::logger> logger_;
  int timer_fd_;
  std::thread monitor_;
  bool stop_ = false;
  // instance
  static std::atomic<CallLogger *> instance_;
  static std::mutex instance_mutex_;
};

inline CallLogger *CALL_LOGGER() {
  return CallLogger::GetInstance();
};

/**
 * CallLogInvoker
 *
 * CallLog를 호출해주는 클래스.
 */
class CallLogInvoker {
 public:
  static CallLogHandler handler_;
 public:
  CallLogInvoker(const char *func,
                 const ServerContext *ctx,
                 const Message *request,
                 const Message *response,
                 const Status *status = &Status::OK) {
    if (handler_) {
      record_ = unique_ptr<CallLogRecord> (new CallLogRecord(func, ctx, request, response, status));
    }
  }

  ~CallLogInvoker() {
    if (record_) {
      record_->SetEndAt();
      string logmsg = handler_(record_.get());
      if (!logmsg.empty()) {
        CALL_LOGGER()->Log(logmsg);
      }
    }
  }

 private:
  unique_ptr<CallLogRecord> record_;
};

void SetCallLogHandler(CallLogHandler handler);

}
}

#endif //LIBMAUM_LOG_CALL_LOG_H_
