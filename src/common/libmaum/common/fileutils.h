#if !defined(_LIBMAUM_FILEUTILS_H)
#define _LIBMAUM_FILEUTILS_H 1

#ifdef  __cplusplus
extern "C" {
#endif

enum {
  FILEUTILS_PRESERVE_STATUS = 1,  // -p
  FILEUTILS_DEREFERENCE = 2,      // -d
  FILEUTILS_RECUR = 4,            // -r
  FILEUTILS_FORCE = 8,            // -f
};

int CopyFile(const char *src, const char *dst, int flags);

int RemoveFile(const char *path, int flags);

int IsDir(const char *path, int followLink);

int IsFile(const char *path, int followLink);

#if defined(__cplusplus)
}
#endif

#if defined(__cplusplus)

#include <sys/types.h>
#include <dirent.h>
#include <string>
#include <vector>

using std::string;
using std::vector;
using StringArr = vector<string>;

class Directory {
 public:
  enum DirFileType {
    DT_UNKNOWN = 0,
    DT_FIFO = 1,
    DT_CHR = 2,
    DT_DIR = 4,
    DT_BLK = 6,
    DT_REG = 8,
    DT_LNK = 10,
    DT_SOCK = 12,
    DT_WHT = 14,
    DT_SELF = 30,
  };
  static unsigned long filemask(DirFileType dt) { return (1 << dt); }
  static unsigned long alltype() { return ~0; }
  static unsigned long regfileonly() { return filemask(DT_REG); }
  static unsigned long files() { return filemask(DT_REG) | filemask(DT_LNK); }
  static unsigned long alldir() { return filemask(DT_DIR) | filemask(DT_SELF); }
  static unsigned long dironly() { return filemask(DT_DIR); }
  static unsigned long excludeself() {
    return filemask(DT_FIFO) | filemask(DT_CHR) | filemask(DT_DIR) |
        filemask(DT_BLK) | filemask(DT_REG) | filemask(DT_LNK) |
        filemask(DT_SOCK) | filemask(DT_WHT);
  }

  Directory()
      : dirname_(),
        dp_(0) {
  }
  Directory(const string& dirname)
      : dirname_(dirname),
        dp_(0) {
  }
  ~Directory() {
    doClose();
  }
  void Open(const char *dirname) {
    dirname_ = dirname;
    doOpen();
  }
  void Close() {
    doClose();
  }
  int List(StringArr &files, unsigned long = (1 << DT_REG), bool recursive = false);
  const char *name() const {
    return dirname_.c_str();
  }

 private:
  static bool ismasked(unsigned long mask, DirFileType dt) {
    return (mask & (1 << dt)) > 0;
  }
  void doOpen();
  void doClose();
  string dirname_;
  DIR *dp_;
};

#endif

#endif /* _LIBMAUM_FILEUTILS_H */
