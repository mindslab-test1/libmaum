#ifndef LIBMAUM_LICENSE_H
#define LIBMAUM_LICENSE_H

#include <openssl/evp.h>
#include <string>

#include <maum/license/license.pb.h>

using std::string;
using maum::license::License;

namespace libmaum {

void LicenseUnseal(License &license, const string &path);

}

#endif // LIBMAUM_LICENSE_H
