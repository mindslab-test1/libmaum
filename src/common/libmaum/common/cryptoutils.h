#ifndef LIBMAUM_CRYPTOUTILS_H
#define LIBMAUM_CRYPTOUTILS_H

#include <openssl/evp.h>

#include <string>
#include <stdexcept>
#include <vector>

using std::string;
using std::vector;

namespace libmaum {

const int kKeyLength = 16;

void Sha256(const void *data, size_t len, vector<uint8_t> &out);

void Aes128CbcDecrypt(const uint8_t key[kKeyLength],
                      const uint8_t iv[kKeyLength],
                      const vector<uint8_t> &cipher_text,
                      string &clear_text);

void Aes128CbcEncrypt(const uint8_t *key,
                      const uint8_t *iv,
                      const string &clear_text,
                      vector<uint8_t> &cipher_text);

void Sign(EVP_PKEY *pkey, const string &msg, vector<uint8_t> &sign);

void Verify(EVP_PKEY *pkey, const string &msg, const vector<uint8_t> &sign);

void Verify(EVP_PKEY *pkey, const string &msg, const vector<uint8_t> &sign);

void Recover(EVP_PKEY *pkey,
             const vector<uint8_t> &pki_sign,
             vector<uint8_t> &sign);

}

#endif //LIBMAUM_CRYPTOUTILS_H
