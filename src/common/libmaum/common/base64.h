#ifndef LIB_COMMON_BASE64_H
#define LIB_COMMON_BASE64_H
#include <string>

std::string Base64Encode(unsigned const char *, unsigned int len);
std::string Base64Decode(const std::string &s);

inline std::string Base64Encode(const std::string &str) {
  return Base64Encode((unsigned const char *) str.c_str(), str.size());
}

#endif