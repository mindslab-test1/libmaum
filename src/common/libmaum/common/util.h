#ifndef COMMON_UTIL_H
#define COMMON_UTIL_H

#include <vector>
#include <string>
#include <algorithm>
#include <cctype>
#include <functional>
#include <locale>

std::string GetGitVersionString();

bool mkpath(const std::string &path);

int GenerateRandomPort();

void GetPrimaryIp(char *buffer, size_t buflen);
std::string GetMacAddress(const char *ipaddr);

bool CheckPort(const char *ip, int port);

int32_t GetRandomTcpPort();
int32_t GetRandomTcpPort(int min_port, int max_port);
bool GetCpuCores(int &cpus, int &total_cores);

std::vector<std::string> split(const std::string &str,
                               int delimiter(int) = ::isspace);

inline std::string &ltrim(std::string &s) {
  s.erase(s.begin(),
          std::find_if(s.begin(), s.end(),
                       std::not1(std::ptr_fun<int, int>(std::isspace))));
  return s;
}

// trim from end
inline std::string &rtrim(std::string &s) {
  s.erase(std::find_if(s.rbegin(),
                       s.rend(),
                       std::not1(std::ptr_fun<int, int>(std::isspace))).base(),
          s.end());
  return s;
}

// trim from both ends
inline std::string &trim(std::string &s) {
  return ltrim(rtrim(s));
}

inline std::string &EraseSpaces(std::string &str) {
  str.erase(std::remove_if(str.begin(), str.end(), ::isspace), str.end());
  return str;
}

void DumpPid(const char *dir, const char *program);

extern "C" {
void EnableCoreDump();
void SetNoFile(unsigned no_file);
}
#endif
