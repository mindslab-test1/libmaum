#ifndef LIB_COMMON_ENCODING_H
#define LIB_COMMON_ENCODING_H

#include <string>

int ChangeEncoding(const char *from, const char *to,
                   const char *src, char **dst);

int ChangeEncoding(const char *from, const char *to,
                   const char *src, std::string &out);

int ChangeEncodingUtfToEuc(const char *to, const char *src, char **dst);

int ChangeEncodingUtfToEuc(const char *to, const char *src, std::string &out);

inline int EuckrToUtf8(const char *src, std::string &out) {
  return ChangeEncoding("EUC-KR", "UTF-8", src, out);
}

inline int EuckrToUtf8(const std::string &src, std::string &out) {
  return ChangeEncoding("EUC-KR", "UTF-8", src.c_str(), out);
}

inline std::string EuckrToUtf8(const std::string &src) {
  std::string out;
  ChangeEncoding("EUC-KR", "UTF-8", src.c_str(), out);
  return out;
}

inline int Cp949ToUtf8(const char *src, std::string &out) {
  return ChangeEncoding("CP949", "UTF-8", src, out);
}

inline int Cp949ToUtf8(const std::string &src, std::string &out) {
  return ChangeEncoding("CP949", "UTF-8", src.c_str(), out);
}

inline std::string Cp949ToUtf8(const std::string &src) {
  std::string out;
  ChangeEncoding("CP949", "UTF-8", src.c_str(), out);
  return out;
}

inline int Utf8ToEuckr(const char *src, std::string &out) {
  return ChangeEncodingUtfToEuc("EUC-KR", src, out);
}

inline int Utf8ToEuckr(const std::string &src, std::string &out) {
  return ChangeEncodingUtfToEuc("EUC-KR", src.c_str(), out);
}

inline std::string Utf8ToEuckr(const std::string &src) {
  std::string out;
  ChangeEncodingUtfToEuc("EUC-KR", src.c_str(), out);
  return out;
}

inline int Utf8ToCp949(const char *src, std::string &out) {
  return ChangeEncodingUtfToEuc("CP949", src, out);
}

inline int Utf8ToCp949(const std::string &src, std::string &out) {
  return ChangeEncodingUtfToEuc("CP949", src.c_str(), out);
}

inline std::string Utf8ToCp949(const std::string &src) {
  std::string out;
  ChangeEncodingUtfToEuc("CP949", src.c_str(), out);
  return out;
}

#endif
