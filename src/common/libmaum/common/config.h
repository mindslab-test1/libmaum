#ifndef LIB_CONFIG_H
#define LIB_CONFIG_H

#include <string>
#include <unordered_map>
#include <vector>
#include <cstdlib>

#include <spdlog/spdlog.h>
#include <json/json.h>

using std::string;
using std::unordered_map;
using std::vector;

namespace libmaum {

/**
 * MAUM.ai 시스템 전체에 사용되는 설정 파일 관리 클래스
 *
 * 자바 프로퍼티 스타일의 파일이나 JSON 스타일의 설정 파일을 로딩할 수 있고
 * 프로퍼티 이름으로 정보를 가져올 수 있습니다.
 */
class Config {
 private:
  Config() = default;
  void Load(const string &filename = "", bool secondary = false);

 public:
  static Config &Instance();
  static bool CheckConfig(const string &prod_name);
  static Config &Init(int argc, char **argv, const char *config_name = nullptr);
  static string GetMaumRoot();

  virtual ~Config() = default;

  /**
   * 설정파일의 이름을 가져옵니다.
   * @return 설정파일의 FULL PATH
   */
  const string &filename() {
    return filename_;
  }

  /**
   * 추가적으로 정의한 설정 파일의 전체 경로들을 반환합니다.
   * @return 추가적인 설정 파일들의 전체 경로들
   */
  const vector<string> additional_filenames() {
    return additional_filenames_;
  }

  /**
   * 새로운 config 파일을 추가한다. 하나 이상의 추가적인 설정을 읽어들일 수 있습니다.
   *
   * @param config_file 추가할 설정 파일의 이름, 설정 파일은 MAUM_ROOT/etc 디렉토리에
   * 존재하는 파일이어야 한다.
   */
  void AddConfigFile(const char *config_file);

  /**
   * 로딩된 설정을 명시적으로 변경한다. 기존의 설정값이 없으면 자동으로 추가한다.
   *
   *
   * @param name 새로운 키 값
   * @param value 설정할 값
   */
  void Set(const string &name, const string &value);

  /**
   * 설정 값을 가져옵니다.
   *
   * 기존 동작은 설정값을 찾을 수 없을 때, 프로그램을 종료합니다.
   * 이유는 설정값이 없는 조건에 동작하는 것 보다는
   * 프로그램이 동작하도록 정상적인 설정을 넣는 것이 더 좋기 때문입니다.
   *
   * @param name  설정 값의 키값
   * @param error 값이 없을 경우에 에러를 내보내도록 한다. 실지로 프로그램이 죽습니다.
   *
   * @return 구해진 갮
   */
  string Get(const string &name, bool error = true);
  string GetDefault(const string &name, const char *def) {
    string ret = Get(name, false);
    if (ret.empty())
      return def;
    return ret;
  }

  /**
   * 암호화된 설정 데이터를 복호화해서 가져온다.
   * @param name 설정값의 이름
   * @param error 값이 없을 경우에 에러를 내보내도록 한다. 실지로 프로그램이 죽습니다.
   * @return 복호화한 결과물
   */
  string GetDecrypt(const string &name, bool error = true);

  /**
   * 설정 값을 정수로 반환합니다.
   *
   * @param name  설정 값의 키값
   * @param error 값이 없을 경우에 에러를 내보내도록 한다. 실지로 프로그램이 죽습니다.
   *
   * @return 구해진 갮
   */
  long GetAsInt(const string &name, bool error = true) {
    string val = Get(name, error);
    return std::stol(val);
  }

  /**
   * 설장값을 찾고 만일 없으면 기본 값을 이용하여 처리합니다.
   *
   * @param name  설정 값의 키값
   * @param def 없는 경우에 사용할 기본값
   * @return 구해진 값
   */
  long GetDefaultAsInt(const string &name, const char *def) {
    string val = GetDefault(name, def);
    return std::stol(val);
  }

  /**
   * JSON 설정을 로딩한 경우에 JSON의 값
   *
   * JSON 은 별도로 로딩되는 데, 프로그램의 시작할 때,
   * "-j json-file" 형식으로 파라미터를 주면 동작합니다.
   *
   * @return 내부에 있는 JSON 값 전체
   */
  Json::Value &GetJson() {
    return json_root_;
  }

  /**
   * 설정 파일 전체를 다시 로딩합니다.
   *
   * 이 동작을 할때 기본의 데이터는 모두 삭제하고 새로이 로딩합니다.
   */
  void Reload();

  // string GetRuntimePath();

  /**
   * 프로그램의 PID를 자동으로 특정 위치에 기록합니다.
   *
   * 위치는 MAUM_ROOT/run/program.pid 입니다.
   */
  void DumpPid();

  // USE AFTER SET PROGRAM
  /**
   * 프로그램 전체에 동작하고 있는 기본 로거를 반환합니다.
   *
   * @return 기본 로거 정보
   */
  std::shared_ptr<spdlog::logger> GetLogger() {
    if (!logger_) {
      logger_ = MakeDefaultLogger();
    }
    return logger_;
  };

  /**
   * 프로그램의 기본 동작 경로를 구해옵니다.
   *
   * "maum.runtime.home" 값을 기본으로 사용합니다.
   *
   * @return
   */
  string GetRuntimeHome() {
    return Get("maum.runtime.home");
  }

  /**
   * 프로그램의 이름을 구합니다.
   *
   * 이 프로그램 이름은 로거를 생성할 때 사용합니다.
   *
   * 기본적으로
   * 자동으로 계산된 프로그램의 이름은 전체 경로에서 마지막 위체 있는
   * 파일 이름과 같습니다.
   * @return 프로그램의 이름
   */
  const char *GetProgramName() {
    return exe_name_.c_str();
  }

  /**
   * 새로운 프로그램의 이름을 설정합니다.
   *
   * @param name 새로운 프로그램의 이름
   */
  void SetProgramName(const string &name) {
    exe_name_ = name;
  }

  /**
   * 로거의 이름을 지정합니다.
   *
   * @param logger 로거 이름
   */
  void SetLoggerName(const string &logger) {
    logger_name_ = logger;
  }

  /**
   * 전체 경로의 프로그램 경로를 구해옵니다.
   * @return 프로그램의 전체 경로
   */
  const string &GetProgramPath();

 private:
  string exe_path_;
  string exe_name_;
  string filename_;
  vector<string> additional_filenames_;
  string logger_name_;
  typedef unordered_map<string, string> NamedValue;
  NamedValue named_values_;
  Json::Value json_root_;
  std::shared_ptr<spdlog::logger> logger_;
  std::shared_ptr<spdlog::logger> MakeDefaultLogger();
};

}

inline std::shared_ptr<spdlog::logger> LOGGER() {
  return libmaum::Config::Instance().GetLogger();
}

#endif // LIB_CONFIG_H
