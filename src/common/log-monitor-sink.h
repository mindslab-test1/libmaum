#ifndef LIBMAUM_LOG_MONITOR_SINK_H
#define LIBMAUM_LOG_MONITOR_SINK_H

#include <spdlog/sinks/base_sink.h>
using func_log_write = void (*)(const void *);
using spdlog::level::level_enum;

/**
 * 로그 출력 함수와 함께, 로그레벨을 지정하여 전달한다.
 *
 * 이 sink는 외부에서 지정한 write 함수를 호출한다.
 *
 * `write` 함수는 구현내부에서 초기화 처리를 해야 한다.
 *
 * @tparam Mutex spdlog에서 사용하는 mutex 정보
 */
template<class Mutex>
class LogMonitorSink : public spdlog::sinks::base_sink<Mutex> {
 public:
  LogMonitorSink(func_log_write write_func,
                 spdlog::level::level_enum level)
      :write_(write_func),
       level_(level) {
  }
  ~LogMonitorSink() = default;

 protected:
  /**
   * 실지로 로그를 내보낸다.
   * @param msg
   */
  void sink_it_(const spdlog::details::log_msg &msg) override {
    // 메시지가 지정된 레벨 이상일 경우에만 처리한다.
    if (msg.level >= level_) {
      write_(&msg);
    }
  }

  // 따로 할 일이 없다.
  virtual void flush_() override {
  }


 private:
  func_log_write write_;
  level_enum level_;
};

spdlog::sink_ptr LoadLogMonitorSink(libmaum::Config& config);

#endif // LIBMAUM_LOG_MONITOR_SINK_H
