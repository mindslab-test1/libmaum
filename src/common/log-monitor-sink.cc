#include <dlfcn.h>
#include <iostream>
#include "libmaum/common/config.h"

#include "log-monitor-sink.h"

const char *kNilPlugin = "none";

/**
 * 샘플로 플러그인 log가 잘 동작하는지 검증하는 루틴
 *
 * @param logmsg
 */
void sample_write(const spdlog::details::log_msg *logmsg) {
  fprintf(stderr, "log monitor: %s\n", fmt::to_string(logmsg->raw).c_str());
}

/**
 * 로그 writer 함수를 로딩한다.
 *
 * @param so 공유 라이브러리 이름
 * @param func 함수 이름
 * @return 함수 로딩에 성공하면, 함수 포인터를 반환한다.
 */
static func_log_write LoadPlugin(const string &so, const string &func) {
  if (so == kNilPlugin || func == kNilPlugin)
    return nullptr;

  if (so.empty()) {
    // std::cerr << "plugin name is empty " << std::endl;
    return nullptr;
  }

  if (func.empty()) {
    std::cerr << "plugin: " << so << "defined, but write func name is empty "
              << std::endl;
    return nullptr;
  }

  void *ptr = dlopen(so.c_str(), RTLD_NOW);
  if (ptr == nullptr) {
    std::cerr << "cannot load so/" << so << ":" << errno << std::endl;
    return nullptr;
  }
  func_log_write func_ptr = nullptr;

  func_ptr = reinterpret_cast<func_log_write>(dlsym(ptr, func.c_str()));
  if (func_ptr == nullptr) {
    std::cerr << "cannot load func/" << func << ":" << errno << std::endl;
    dlclose(ptr);
    return nullptr;
  }
  return func_ptr;
}

/**
 * LogMonitorSink Sink 객체를 생성하여 반환한다.
 *
 * 플러그인 공유 라이브러리와 함수 이름을 로딩해서 이를 반환한다.
 *
 * @param config 설정값을 읽어들일 config
 * @return sink_ptr 즉, sink의 shared_ptr 을 반환한다.
 */
spdlog::sink_ptr LoadLogMonitorSink(libmaum::Config &config) {

  string plugin = config.Get("logs.monitor.plugin", false);
  string write_func = config.Get("logs.monitor.write-function", false);
  string level = config.GetDefault("logs.monitor.level", "warning");

  func_log_write func_ptr = nullptr;

  if (plugin == "sample" && write_func == "sample_write") {
    func_ptr = reinterpret_cast<func_log_write >(&sample_write);
    goto load_sink;
  }

  func_ptr = LoadPlugin(plugin, write_func);
  // 함수를 로딩할 수 없을 경우에는 빈 `shared_object`을 반환합니다.
  if (func_ptr == nullptr) {
    return spdlog::sink_ptr();
  }

  load_sink:
  auto lvl = spdlog::level::from_str(level);
  if (lvl == spdlog::level::off)
    lvl = spdlog::level::warn;
  using LogMonitorSinkMt = LogMonitorSink<std::mutex>;

  spdlog::sink_ptr sink = std::make_shared<LogMonitorSinkMt>(func_ptr, lvl);

  return sink;
}