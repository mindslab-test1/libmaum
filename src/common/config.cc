#include "libmaum/common/config.h"
#include <iostream>
#include <fstream>

#include "log-daily-rotating-sink.h"
#include "log-monitor-sink.h"
#include "log-mask-formatter.h"
#include <spdlog/async.h>
#include <spdlog/sinks/rotating_file_sink.h>

#include <libgen.h> // basename
#include <maum/license/license.pb.h>
#include <libmaum/common/license.h>
#include "libmaum/common/base64.h"
#include "libmaum/common/cryptoutils.h"

#define __LOG 0

using std::vector;
using std::cout;
using std::cerr;
using std::endl;
using std::ifstream;
using std::ios_base;

const int kMaxFileSize = 500 * 1024 * 1024;
const int kMaxRotateCount = 20;
const int kDefaultMaxFileSize = 5 * 1024 * 1024;
const int kDefaultMaxRotateCount = 10;
const char *kLogPattern = "[%m-%d %H:%M:%S.%e] %n[%P %t] %L: %v";

extern "C" bool CheckConfig(const char *prod_name) {
  const string temp_name(prod_name);
  return libmaum::Config::CheckConfig(temp_name);
}

void ReplaceStringInPlace(std::string &subject, const std::string &search,
                          const std::string &replace) {
  size_t pos = 0;
  while ((pos = subject.find(search, pos)) != std::string::npos) {
    subject.replace(pos, search.length(), replace);
    pos += replace.length();
  }
}

namespace libmaum {

string Config::Get(const string &name, bool error) {
  auto got = named_values_.find(name);
  if (got != named_values_.end()) {
    return got->second;
  } else {
    // TODO
    // 좀 더 복잡한 설정을 위해서
    // a/b/c/d 와 같은 형식으로 계층적인 데이터를 가져올 수 있도록 한다.
    Json::Value &config_object = json_root_["config"];
    Json::Value &val = config_object[name];
    if (!val.empty()) {
      return val.asString();
    }

    cerr << "cannot config value for " << name
         << " at " << filename_ << endl;
    if (error) {
      exit(100);
    }
    return "";
  }
}

static string DecryptText(const string &b64) {
  string b_enc_temp = Base64Decode(b64);

  if (b_enc_temp.size() < (kKeyLength * 3)) {
    throw std::runtime_error("invalid encrypted data");
  }
  vector<uint8_t> iv_bin;
  iv_bin.reserve(kKeyLength);
  vector<uint8_t> key_bin;
  key_bin.reserve(kKeyLength);
  vector<uint8_t> enc_bin;
  enc_bin.reserve(b_enc_temp.size() - kKeyLength * 2);

  for (int x = 0; x < (kKeyLength * 3); x++) {
    int mod = x % 3;
    switch (mod) {
      case 0: {
        enc_bin.emplace_back(b_enc_temp[x]);
        break;
      }
      case 1: {
        iv_bin.emplace_back(b_enc_temp[x]);
        break;
      }
      case 2: {
        key_bin.emplace_back(b_enc_temp[x]);
        break;
      }
    }
  }

  std::copy(b_enc_temp.begin() + (kKeyLength * 3), b_enc_temp.end(),
            std::back_inserter(enc_bin));

  string ret;
  Aes128CbcDecrypt(key_bin.data(), iv_bin.data(), enc_bin, ret);
  return ret;
}

string Config::GetDecrypt(const string &name, bool error) {
  string enc_str = Get(name, error);
  try {
    return DecryptText(enc_str);
  } catch (std::runtime_error &e) {
    cerr << "cannot decrypt for " << name << ", ["
         << e.what() << "] at " << filename_ << endl;
    if (error)
      exit(100);
    return "";
  }
}

/**
 * MAUM_ROOT 환경변수를 이용해서 MAUM_ROOT를 계산해 낸다.
 * @return MAUM_ROOT 문자열.
 */
string Config::GetMaumRoot() {
  const char *p = ::getenv("MAUM_ROOT");
  string maum_root;
  if (p == nullptr) {
    const char *home = ::getenv("HOME");
    if (home == nullptr)
      home = "/srv/maum";
    maum_root = home;
    maum_root += "/maum";
  } else
    maum_root = p;
  return maum_root;
}

/**
 * 지정된 파일에서 config 설정을 읽어 들입니다.
 *
 * 이 함수는 반드시 FindConfigFile 을 호출한 다음에 호출되므로
 * 함수 내부에서는 파일의 이름을 점검하지는 않는다.
 *
 * 재로딩할 때에도 이 함수를 호출할 수 있는데,
 * 만일 원래 파일이 삭제된 경우라면 원하는 동작을 할 수 없다.
 * 사용자의 책임이다.
 *
 * @param filename 로딩해야할 설정 파일의 전체 경로를 준다.
 * @param secondary 기본 로딩이 아닌 경우, 추가적으로 로딩해야 하는 파일들
 */
void Config::Load(const string &filename, bool secondary) {
  string cfg_file;
  if (filename.empty()) {
    // secondary는 무시한다.
    // secondary = false;
    cfg_file = filename_;
  } else {
    cfg_file = filename;
    if (secondary) {
      additional_filenames_.push_back(filename);
    } else {
      filename_ = filename;
    }
  }

  std::ifstream is_file(cfg_file);
  string maum_root = Config::GetMaumRoot();

  string line;
  while (std::getline(is_file, line)) {
    if (line[0] == '#')
      continue;
    std::istringstream is_line(line);
    std::string key;
    if (std::getline(is_line, key, '=')) {
      std::string value;
      if (std::getline(is_line, value)) {

        trim(key);
        trim(value);
        ReplaceStringInPlace(value, "${MAUM_ROOT}", maum_root);

        // '[]'로 끝나는 변수는 배열로 처리하고, ' ' 또는 ','로 구분한다.
        if (key.find_last_of("[]") != string::npos) {
          auto got = named_values_.find(key);
          if (got != named_values_.end()) {
            auto temp = got->second;
            temp += ' ';
            temp += ',';
            temp += value;
            value = temp;
          }
        }

        // TODO
        // value 에 #을 넣어서 설명을 붙이는 경우는 현재 허용하지 않는다.
        named_values_.insert(NamedValue::value_type(key, value));
#if 0
        cout << key <<  "=" << value << endl;
#endif
      }
    }
  }
}

void Config::Reload() {
  named_values_.clear();
  Load(filename_, false);

  for (const auto &f : additional_filenames_) {
    Load(f, true);
  }
}

string GetExePath() {
  char result[PATH_MAX];
  ssize_t count = readlink("/proc/self/exe", result, PATH_MAX);
  return string(result, count > 0 ? (size_t) count : 0);
}

void Config::DumpPid() {
  string exe_path = GetExePath();
  string exe_dir = exe_path.substr(0, exe_path.find_last_of('/'));

  string pid_dir = Get("pid.dir", false);
  string path = pid_dir;
  if (path.empty()) {
    // TODO
    // 환경변수를 통해서도 처리할 수 있다.
    path = exe_dir + "/run";
  }
  string exe_name = exe_name_;

  if (json_root_.isObject()) {
    Json::Value index = json_root_["index"];
    if (index.isInt()) {
      exe_name += '-';
      exe_name += index.asString();
    }
  }

  ::DumpPid(path.c_str(), exe_name.c_str());
}

static inline char easytolower(char in) {
  if (in <= 'Z' && in >= 'A')
    return in - ('Z' - 'z');
  return in;
}

std::shared_ptr<spdlog::logger> Config::MakeDefaultLogger() {
  string exe_path = GetExePath();
  string exe_dir = exe_path.substr(0, exe_path.find_last_of('/'));

  bool daily_rotate = GetDefault("logs.daily.rotate", "false") == "true";
  bool masking_enable = GetDefault("logs.masking.enable", "false") == "true";
  string logs_dir = Get("logs.dir", false);
  string path = logs_dir;
  if (path.empty()) {
    path = exe_dir + "/../logs";
    char buf[BUFSIZ];
    path = realpath(path.c_str(), buf);
  }

  long max_file_size = kDefaultMaxFileSize;
  string temp = Get("logs.max-file-size", false);

  if (!temp.empty()) {
    try {
      if (temp.back() != 'M') {
        max_file_size = std::stol(temp);
      } else {
        max_file_size = std::stol(temp.substr(0, temp.length() - 1));
        max_file_size = max_file_size * 1024 * 1024;
      }
    } catch (...) {
      max_file_size = kDefaultMaxFileSize;
    }
  }

  if (max_file_size > kMaxFileSize) {
    max_file_size = kMaxFileSize;
  }

  int rotate_count = kDefaultMaxRotateCount;
  try {
    rotate_count = (int) GetAsInt("logs.rotate.count", false);
  } catch (...) {
    rotate_count = kDefaultMaxRotateCount; // default value
  }

  if (rotate_count >= kMaxRotateCount)
    rotate_count = kMaxRotateCount;
  else if (rotate_count < 0)
    rotate_count = 0;

  mkpath(path);

  path += '/';
  path += exe_name_;
  path += ".log";

  string logger = logger_name_.empty() ? exe_name_ : logger_name_;

  string lvl;
  auto idx = logger.find('<');
  string
      level_name = (idx != std::string::npos) ? logger.substr(0, idx) : logger;
  string level_for_logger = "logs.level." + level_name;
  // 이 값을 가져오려고 하지만, 없으면 에러가 나지 않는다.
  lvl = Get(level_for_logger, false);
  // logger 이름으로 level 정보를 가져오지 못하면 default 로그 레벨을 가져온다.
  if (lvl.empty()) {
    lvl = GetDefault("logs.level.default", "debug");
  }

  std::transform(lvl.begin(), lvl.end(), lvl.begin(), easytolower);

  auto user_level = spdlog::level::from_str(lvl);

  // 로거를 생성한다.
  // 로거는 `sink`라는 출력을 담당하는 출력 객체를 이용해서 처리한다.
  using spdlog::sinks::rotating_file_sink_mt;

  std::shared_ptr<spdlog::logger> ret;

#if 0
  using my_daily_file_sink_mt =
  spdlog::sinks::daily_file_sink<
      std::mutex,
      spdlog::sinks::dateonly_daily_file_name_calculator>;
#endif

  spdlog::sink_ptr sink;
  if (daily_rotate) {
#if 0
    sink = std::make_shared<my_daily_file_sink_mt>(path, 0, 0);
#else
    sink = std::make_shared<DailyRotatingSink<std::mutex>>(path,
                                                           size_t(max_file_size),
                                                           0, 0);
#endif
  } else {
    sink = std::make_shared<rotating_file_sink_mt>(path,
                                                   size_t(max_file_size),
                                                   size_t(rotate_count));
  }

  //
  // 외부 로그 플러그인을 추가로 로드한다.
  //
  spdlog::sink_ptr sink2 = LoadLogMonitorSink(*this);

  spdlog::init_thread_pool((1 << 14), 1);
  if (sink2 != nullptr) {
    spdlog::sinks_init_list sink_list = {sink, sink2};
    ret = std::make_shared<async_logger>(logger,
                                         sink_list,
                                         spdlog::thread_pool(),
                                         async_overflow_policy::overrun_oldest);
  } else {
    // 단일 SINK로 생성하도록 한다.
    ret = std::make_shared<async_logger>(logger, sink,
                                         spdlog::thread_pool(),
                                         async_overflow_policy::overrun_oldest);
  }

  // masking enable 인 경우 LogMaskPatternFormatter 를 사용하도록 한다.
  if (masking_enable) {
    ret->set_formatter(std::unique_ptr<spdlog::formatter>(
        new MaskPatternFormatter(kLogPattern)));
  } else {
    ret->set_pattern(kLogPattern);
  }
  // 사용자가 지정한 레벨로 설정한다.
  ret->set_level(user_level);
  // INFO 레벨은 무조건 내보내도록 한다.
  ret->should_log(spdlog::level::info);
  // `critical`이 들어오면 모두 정리한다.
  ret->flush_on(spdlog::level::info);
  spdlog::flush_every(std::chrono::seconds(1));

  return ret;
}

/**
 * 여러가지 경로 후보에서 설정 파일을 구해온다.
 *
 * - 실행파일이 있는 현재의 디렉토리
 * - 실행파일이 있는 현재의 디렉토리 ../etc/
 * - $HOME/.maum
 * - /etc/maum
 * - /etc
 *
 * 의 순서로 구해온다.
 */
string FindConfigFile(const string &name) {
  string exe_path = GetExePath();
  string exe_dir = exe_path.substr(0, exe_path.find_last_of('/'));
  string near_etc_dir = exe_dir + "/../etc";

  vector<string> dirs;
  const char *maum_root = getenv("MAUM_ROOT");
  if (maum_root) {
    dirs.push_back(string(maum_root) + "/etc");
  } else {
    cerr << "MAUM_ROOT environment variable is not defined!" << endl;
  }
  const char *home_dir = getenv("HOME");
  if (home_dir) {
    dirs.push_back(string(home_dir) + "/.maum");
  }

  dirs.push_back(near_etc_dir);
  dirs.push_back(exe_dir);
  dirs.emplace_back("/etc/maum");
  dirs.emplace_back("/etc");

  char cwd_buf[PATH_MAX];
  getcwd(cwd_buf, sizeof cwd_buf);
  dirs.emplace_back(cwd_buf);

  for (const auto &dir : dirs) {
    string full = dir;
    full += '/';
    full += name;
    std::ifstream in_file(full);
    if (in_file.good()) {
#if __LOG
      cout << "found config file at " << full << endl;
#endif
      return full;
    } else {
#if __LOG
      cout << "cannot find config file at " << full << endl;
#endif
    }
  }

  string paths;
  paths = "[";
  for (const auto &dir : dirs) {
    paths += dir;
    paths += ',';
  }
  paths[paths.length() - 1] = ']';
  cerr << "Cannot find " << name << " in " << paths << endl;
  return "";
}

Config &Config::Instance() {
  static Config singleton_;
  return singleton_;
}

void ChdirToAppHome() {
  auto exe_path = GetExePath();
  string home = exe_path.substr(0, exe_path.find_last_of("/bin"));
  chdir(home.c_str());
}

const string &Config::GetProgramPath() {
  if (exe_path_.empty()) {
    exe_path_ = GetExePath();
  }
  return exe_path_;
}

bool Config::CheckConfig(const string &prod_name) {
  // 무조건 true를 리턴하여 License 제약을 하지 않도록 함.
  return true;
  try {
    maum::license::License lic;
    string license_file;

    string maum_root = Config::GetMaumRoot();
    license_file = maum_root;
    license_file += "/license/maumai-license.pem";
    libmaum::LicenseUnseal(lic, license_file);

    char ipaddr[30];
    int cpus, total_cores;
    time_t now = time(nullptr);
    GetPrimaryIp(ipaddr, sizeof ipaddr);
    string mac = GetMacAddress(ipaddr);
    GetCpuCores(cpus, total_cores);

    for (auto ul: lic.licenses()) {
      if (prod_name == ul.product()) {
        if (ul.expire_on().seconds() < now) {
          cerr << "License failure: "
               << "The license has expired for " << prod_name << '.' << endl;
          exit(EXIT_FAILURE);
        }
        if (ul.cpu_cores() < total_cores) {
          cerr << "License  failure: "
               << "Max. " << ul.cpu_cores() << " CPU Core(s) permitted for "
               << prod_name << '.' << endl;
          exit(EXIT_FAILURE);
        }
        for (auto h : ul.hosts()) {
          std::transform(h.begin(), h.end(), h.begin(), ::toupper);
          if (h == ipaddr || h == mac) {
            string coupon_file = string(::getenv("HOME")) + "/.maumai-coupon";
            struct stat buffer;
            if (stat(coupon_file.c_str(), &buffer) == 0) {
              remove(coupon_file.c_str());
            }
            cout << "The License has confirmed for "
                 << prod_name << '.' << endl;
            return true;
          }
        }
        cerr << "License  failure: "
             << "Permitted host is not found for " << prod_name << '.' << endl;
        exit(EXIT_FAILURE);
      }
    }
    cerr << "License not found for " << prod_name << '.' << endl;
    exit(EXIT_FAILURE);
  } catch (const std::exception &e) {
    string coupon_file = string(::getenv("HOME")) + "/.maumai-coupon";
    ifstream ifs(coupon_file, ios_base::in | ios_base::binary);
    if (ifs.good()) {
      vector<uint8_t> coupon;
      ifs.unsetf(std::ios::skipws);
      coupon.insert(coupon.begin(),
                    std::istream_iterator<uint8_t>(ifs),
                    std::istream_iterator<uint8_t>());
      ifs.close();

      vector<uint8_t> iv_key;
      uint8_t *value;
      string description;
      auto out_iter = std::back_inserter(iv_key);

      char ip_addr[30];
      GetPrimaryIp(ip_addr, sizeof ip_addr);
      string mac = GetMacAddress(ip_addr);

      description = License::Descriptions_Name(License::validTo);
      std::copy(description.begin() + 1, description.end() - 2, out_iter);
      value = (uint8_t *) &License::dedicated;
      std::copy(value + 2, value + 4, out_iter);
      std::copy(mac.begin(), mac.end(), out_iter);
      description = License::Descriptions_Name(License::allowedTo);
      std::copy(description.begin() + 2, description.end() - 2, out_iter);
      value = (uint8_t *) &License::expiration;
      std::copy(value, value + 4, out_iter);

      try {
        string issued_temp;
        Aes128CbcDecrypt(iv_key.data(),
                         iv_key.data() + kKeyLength,
                         coupon,
                         issued_temp);
        time_t issued = std::stoul(issued_temp.substr(1384, 8), 0, 16);
        time_t span = time(nullptr) - issued;
        if (span >= 0 && span <= 604800) {
          cout << "The License has temporarily confirmed for "
               << prod_name << '.' << endl;
          return true;
        }
      } catch (...) {
        remove(coupon_file.c_str());
      }
    }

    cerr << "License loading failed for " << prod_name << ". "
         << e.what() << endl;
    exit(EXIT_FAILURE);
  }
}

/**
 * 시스템 전체에서 사용되는 config 파일을 설정합니다.
 *
 * 기본 초기화에서 Init(argc, argv, "xxxx.conf") 형식으로 사용한다.
 * 명령 라인에서 -c yyyy.conf 라고 주면, xxxx.conf 는 무시한다.
 *
 * @param argc 파라미터 입력의 개수
 * @param argv  파리미터의 내용
 * @param config_name 기본 config 파일 이름
 * @return 샐운 설정을 반환한다.
 */
Config &Config::Init(int argc, char **argv, const char *config_name) {
  const char *prog_name = basename(argv[0]);
  const char *config_file = nullptr;

  string prog_conf_name(prog_name);
  prog_conf_name += ".conf";

  Json::Value root;

  for (int i = 1; i < argc; i++) {
    if (strcmp(argv[i], "-c") == 0) {
      if ((i + 1) < argc) {
        config_file = argv[i + 1];
        i++;
      } else {
        cerr
            << "config file is not provided!" << endl
            << prog_name << " -c [config file name or path]" << endl
            << "  : if uses name only it searches config file the following directories."
            << endl
            << "  [. ../etc /etc/maum /etc ~/.maum]" << endl;
        exit(1);
      }
    }

    if (strcmp(argv[i], "-j") == 0) {
      if ((i + i) < argc) {
        string json_file = argv[i + 1];
        i++;
        std::ifstream ifs(json_file);

        Json::CharReaderBuilder builder;
        builder["collectComments"] = false;
        std::string errs;
        bool ok = Json::parseFromStream(builder, ifs, &root, &errs);
        if (!ok) {
          cerr << "JSON config file has an error: " << errs << endl;
          exit(1);
        }
      } else {
        cerr
            << "JSON config file is not provided!" << endl
            << prog_name << " -j [JSON config file name or path]" << endl;
        exit(1);
      }
    }
  }

  if (!config_file) {
    if (config_name) {
      config_file = config_name;
    } else {
      config_file = prog_conf_name.c_str();
    }
  }

  auto dot = strchr(config_file, '.');
  string prod_name;
  if (dot != nullptr) {
    prod_name.assign(config_file, dot);
  } else {
    prod_name = config_file;
  }

  CheckConfig(prod_name);

  string load_conf_file;

  if (access(config_file, R_OK) < 0) {
    string conf_file = FindConfigFile(config_file);
    if (conf_file.empty()) {
      cerr << "Cannot find config file named " << config_file << endl;
      exit(1);
    }
    load_conf_file = conf_file;
  } else {
    load_conf_file = config_file;
  }

  libmaum::Config &c = Config::Instance();
  c.Load(load_conf_file);
  c.json_root_ = root;
  string exe_path = GetExePath();
#if __LOG
  cerr << "exe path is [" << exe_path << "]" << endl;
#endif

  c.exe_path_ = exe_path;
  c.exe_name_ = prog_name;

  ChdirToAppHome();

  return c;
}

void Config::AddConfigFile(const char *config_file) {
  string conf_file;
  conf_file = FindConfigFile(config_file);
  if (conf_file.empty()) {
    LOGGER()->error("Cannot find config file named {}", config_file);
    return;
  }
  this->Load(conf_file, true);
}

void Config::Set(const string &name, const string &value) {
  auto got = named_values_.find(name);
  if (got != named_values_.end()) {
    got->second = value;
  } else {
    named_values_.insert(NamedValue::value_type(name, value));
  }
}

}
