#ifndef LIBMAUM_LOG_ROTATING_SINK_H
#define LIBMAUM_LOG_ROTATING_SINK_H

#include <string>
#include <sstream>
#include <iomanip>
#include <libmaum/common/util.h>
#include "../spdlog/spdlog/sinks/base_sink.h"
#include "../spdlog/spdlog/details/file_helper.h"

using filename_t = std::string;
using std::string;
using std::size_t;

template<class Mutex>
class DailyRotatingSink : public spdlog::sinks::base_sink<Mutex> {
 public:
  DailyRotatingSink(filename_t base_filename,
                  size_t max_size,
                  int rotation_hour,
                  int rotation_minute)
      : base_filename_(std::move(base_filename)),
        max_size_(max_size),
        rotation_h_(rotation_hour),
        rotation_m_(rotation_minute) {

    if (rotation_hour < 0 || rotation_hour > 23 || rotation_minute < 0 ||
        rotation_minute > 59) {
      throw spdlog::spdlog_ex("daily_file_sink: Invalid rotation time in ctor");
    }

    over_tp_ = false;
    backup_dirname_ = "backup";

    // base_filename > logs_dirpath, logger_name 추출
    size_t pos = base_filename_.find_last_of('/');
    logs_dirpath_ = base_filename_.substr(0, pos);
    filename_t filename = base_filename_.substr(pos + 1);
    string model_name, ext;
    std::tie(model_name, ext) =
        spdlog::details::file_helper::split_by_extenstion(filename);
    logger_name_ = model_name;

    rotation_tp_ = NextRotationTimePoint();
    file_helper_.open(CalcFilename(over_tp_, base_filename_, 0));
    current_size_ = file_helper_.size(); // expensive. called only once
  }

  ~DailyRotatingSink() = default;

  // filename : ~/logs/module.log, ("logs/mylog.txt, 3) => "logs/mylog.3.txt".
  static filename_t CalcFilename(bool over_tp,
                                 const filename_t &filename,
                                 size_t nn_index) {

    typename std::conditional<std::is_same<string::value_type, char>::value,
                              fmt::memory_buffer,
                              fmt::wmemory_buffer>::type w;

    // logs/backup/yyyy-mm-dd/logger.yyyy-mm-dd.NN.log
    if (nn_index != 0u) {
      string basename, ext;
      std::tie(basename, ext) =
          spdlog::details::file_helper::split_by_extenstion(filename);
      fmt::format_to(
          w, SPDLOG_FILENAME_T("{}.{}.{}{}"), basename, GetDateName(over_tp),
          nn_index, ext);
    }
      // logs/logger.log
    else {
      fmt::format_to(w, SPDLOG_FILENAME_T("{}"), filename);
    }
    return fmt::to_string(w);
  }

  static filename_t GetTargetFile(bool over_tp, const filename_t &date_dir) {
    filename_t target;
    size_t nn_index = 1;
    while (true) {
      target = CalcFilename(over_tp, date_dir, nn_index);
      if (!spdlog::details::file_helper::file_exists(target)) {
        break;
      }
      ++nn_index;
    }
    return target;
  }

  // 현재 시간에 대한 yyyy-mm-dd 문자열을 리턴
  static string GetDateName(bool over_tp) {
    auto now = std::chrono::system_clock::now();
    // tp 를 over 한 경우 다음날이므로 rotating 시점의 로그는 이전날에 rotate
    if (over_tp) {
      now -= std::chrono::hours(24);
    }
    time_t tnow = std::chrono::system_clock::to_time_t(now);
    std::tm tm = spdlog::details::os::localtime(tnow);

    std::stringstream set_year, set_month, set_day;
    set_year << std::setw(4) << std::setfill('0') << tm.tm_year + 1900;
    set_month << std::setw(2) << std::setfill('0') << tm.tm_mon + 1;
    set_day << std::setw(2) << std::setfill('0') << tm.tm_mday;

    return set_year.str() + "-" + set_month.str() + "-" + set_day.str();
  }

  static string CheckFolderExist(bool over_tp, const filename_t &logs_dirpath,
                                 const filename_t &backup_dirname) {
    // logs 의 하위에 backup 폴더 없는 경우 생성
    filename_t backup_dirpath = logs_dirpath + '/' + backup_dirname;
    mkpath(backup_dirpath);
    // backup 폴더 밑에 rotate 시점의 yyyy-mm-dd 폴더 없는 경우 생성
    filename_t date_dirpath = backup_dirpath + '/' + GetDateName(over_tp);
    mkpath(date_dirpath);
    return date_dirpath;
  }

 protected:

  void sink_it_(const spdlog::details::log_msg &msg) override {
    fmt::memory_buffer formatted;
    spdlog::sinks::sink::formatter_->format(msg, formatted);
    current_size_ += formatted.size();

    over_tp_ = std::chrono::system_clock::now() >= rotation_tp_;
    /**
     * now > time_point(midnight) 또는 max_size 를 넘은 경우 backup 으로 이동
     * _over_tp = true : 다음날임
     */
    if (over_tp_ || current_size_ > max_size_) {
      Rotate();
      // tp 를 넘었으면 다음 tp 를 계산
      if (over_tp_) {
        rotation_tp_ = NextRotationTimePoint();
      }
      current_size_ = formatted.size();;
    }
    file_helper_.write(formatted);
  }

  void flush_() override {
    file_helper_.flush();
  }

 private:

  // Rotate files
  void Rotate() {
    using spdlog::details::os::filename_to_str;

    file_helper_.close();

    // backup 폴더, yyyy-mm-dd 폴더 체크해서 없으면 생성하고, yyyy-mm-dd 를 return
    filename_t date_dir = CheckFolderExist(over_tp_, logs_dirpath_,
                                           backup_dirname_);
    date_dir += ('/' + logger_name_ + ".log");

    filename_t src = CalcFilename(over_tp_, base_filename_, 0);
    filename_t target = GetTargetFile(over_tp_, date_dir);

    if (target.empty() || (spdlog::details::file_helper::file_exists(src) &&
        spdlog::details::os::rename(src, target) != 0)) {
      throw spdlog::spdlog_ex("rotating_file_sink: failed renaming " +
          filename_to_str(src) + " to " + filename_to_str(target), errno);
    }

    file_helper_.reopen(true);
  }

  std::chrono::system_clock::time_point NextRotationTimePoint() {
    auto now = std::chrono::system_clock::now();
    time_t tnow = std::chrono::system_clock::to_time_t(now);
    tm date = spdlog::details::os::localtime(tnow);
    date.tm_hour = rotation_h_;
    date.tm_min = rotation_m_;
    date.tm_sec = 0;
    auto rotation_time =
        std::chrono::system_clock::from_time_t(std::mktime(&date));
    if (rotation_time > now) {
      return rotation_time;
    }
    return {rotation_time + std::chrono::hours(24)};
  }

  filename_t base_filename_;
  filename_t backup_dirname_;
  filename_t logs_dirpath_;
  string logger_name_;
  size_t max_size_;
  size_t current_size_;
  int rotation_h_;
  int rotation_m_;
  bool over_tp_;
  std::chrono::system_clock::time_point rotation_tp_;
  spdlog::details::file_helper file_helper_;
};

#endif //LIBMAUM_LOG_ROTATING_SINK_H
