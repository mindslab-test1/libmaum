#include <cmath>
#include "log-mask.h"

using namespace std;

// 정규 표현식의 패턴 크기
#define REGEX_PATTERN_SIZE 4
// 사업자 등록 번호 유효성 체크시 사용되는 id 개수
#define BUSINESS_NUMBER_ID 10

// 각 패턴에 대한 masking 처리될 char 의 개수
static const std::array<int, REGEX_PATTERN_SIZE> kMaskCharLength = {7, 4, 7, 5};
// business number validate 체크 시 사용될 id
static const std::array<int, BUSINESS_NUMBER_ID> kBusinessNumberIds
    = {1, 3, 7, 1, 3, 7, 1, 3, 5, 1};

#define MAX_PCRE_SUB_STR_VEC 100
const int kBusinessPatternIndex = 3;

// "$1$2*******" : right(7)
const char *
    kPersonalNumber = "\\b[0-9]{2}(0[1-9]|1[012])(0[1-9]|1[0-9]|2[0-9]|3[01])-?"
                      "[012349][0-9]{6}\\b";
// "$1$2$3$4****" : right(4)
const char *kPhoneNumber = "\\b(01[0|1|7|8|9])([-|\\s])?(\\d{3,4})([-|\\s])?"
                           "\\d{4}\\b";
// "$1$2*******" : right(7)
const char *kPassportNumber = "\\b([a-zA-Z][a-zA-Z])([ -])?(\\d{7})\\b";
// "$1$2$3$4*****" : right(5)
const char *kBusinessNumber = "\\b(\\d{3})([-|\\s])?(\\d{2})([-|\\s])?\\d{5}\\b";

/**
 * 사업자 등록 번호에 대해서 유효성을 체크
 *
 * @param bno bizno 사업자 등록 번호
 * @return true : 유효, false : 무효
 */
static bool CheckValidateBusinessNumber(const string &bno_bizno) {

  string bizno(bno_bizno);
  // business_number 에서 " "제거, "-" 제거
  bizno.erase(std::remove(bizno.begin(), bizno.end(), ' '),
              bizno.end());
  bizno.erase(std::remove(bizno.begin(), bizno.end(), '-'),
              bizno.end());

  int summary = 0;
  for (int index = 0 ; index < BUSINESS_NUMBER_ID - 2 ; ++index) {
    summary += kBusinessNumberIds[index] * (bizno[index] - '0');
  }

  string check = "0" + std::to_string(kBusinessNumberIds[8] *
      (bizno[8] - '0'));

  auto clen = check.length();
  string sub_check = check.substr(clen - 2, clen);
  summary += floor(sub_check[0] - '0') + floor(sub_check[1] - '0');

  return floor(bizno[9] - '0') == ((10 - (summary % 10)) % 10);
}

LogMask::LogMask() {
  regex_res_.push_back(CompileRegex(kPersonalNumber));
  regex_res_.push_back(CompileRegex(kPhoneNumber));
  regex_res_.push_back(CompileRegex(kPassportNumber));
  regex_res_.push_back(CompileRegex(kBusinessNumber));
}

LogMask::~LogMask() {
  regex_res_.clear();
}

#if 0
// pcre 를 통한 정규 표현식 처리를 한다.
void LogMask::Mask(spdlog::details::log_msg &msg) {
  string raw_str = msg.raw.c_str();
  int mask_index = 0;
  for (const auto &re_compiled : regex_re_) {
    raw_str = PcreExec(re_compiled, raw_str.c_str(), mask_index++);
  }
  msg.raw.clear();
  msg.raw << raw_str;
}
#endif

// pcre 를 통한 정규 표현식 처리를 한다.
string LogMask::Mask(const string &msg) {
  string raw_str = msg;
  int mask_index = 0;
  for (const auto &re_compiled : regex_res_) {
    raw_str = PcreExec(re_compiled, raw_str.c_str(), mask_index++);
  }
  return raw_str;
}

string LogMask::PcreExec(pcre *re_compiled,
                         const char *input,
                         int mask_index) {
  const char *p_substr_matchstr;

  int sub_str_vec[MAX_PCRE_SUB_STR_VEC];
  int exec_ret;
  string ret_string(input);
  int mask_length = kMaskCharLength[mask_index];

  if (re_compiled == nullptr) {
    return ret_string;
  }

  int start_looking = 0;
  while (true) {
    exec_ret = pcre_exec(re_compiled,
                         nullptr/*pcre_extra*/,
                         ret_string.c_str(),
                         strlen(ret_string.c_str()),  // length of string
                         start_looking,               // Start looking at this point
                         0,                           // OPTIONS
                         sub_str_vec,
                         MAX_PCRE_SUB_STR_VEC);       // Length of sub_str_vec_
    // 정규표현식에 해당되는 문자열이 없으면 break
    if (exec_ret < 0) {
      break;
    }

    if (exec_ret == 0) {
      // logger->debug("But too many substrings were found to fit in sub_str_vec_!");
      // Set rc to the max number of substring matches possible.
      exec_ret = MAX_PCRE_SUB_STR_VEC / 3;
    }

    // check the first mached substring only
    pcre_get_substring(input,
                       sub_str_vec,
                       exec_ret,
                       0,
                       &p_substr_matchstr);

    // ret_string 의 (end - mask_length) ~ end 까지  "*"으로 replace
    int end_index = sub_str_vec[1];
    // 사업자 등록번호의 패턴체크인 경우 유효성 검사
    if (mask_index == kBusinessPatternIndex &&
        !CheckValidateBusinessNumber(p_substr_matchstr)) {
      pcre_free_substring(p_substr_matchstr);
      start_looking = end_index;
      continue;
    }

    auto start_index = end_index - mask_length;
    while (start_index < end_index) {
      ret_string.replace(start_index, 1, "*");
      start_index++;
    }
    pcre_free_substring(p_substr_matchstr);
    start_looking = end_index;
  }

  return ret_string;
}

pcre *LogMask::CompileRegex(const char *regex) {
  const char *pcre_error_str;
  int pcre_error_offset;
  return pcre_compile(regex, 0, &pcre_error_str, &pcre_error_offset, nullptr);
}

