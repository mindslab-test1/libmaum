#include "libmaum/log/call-log.h"

#include <iomanip>
#include <fstream>
#include <libmaum/common/config.h>
#include <libmaum/log/interval-logger.h>
#include <sys/timerfd.h>
#include <cinttypes>

using grpc::ClientContext;

namespace libmaum {
namespace log {

CallLogHandler CallLogInvoker::handler_ = nullptr;

void SetCallLogHandler(CallLogHandler handler) {
  CallLogInvoker::handler_ = handler;
}

void CallLogMetaSetter(ClientContext *ctx, Message *msg) {
  if (CallLogInvoker::handler_) {
    ctx->AddMetadata("log.meta-bin", msg->SerializeAsString());
  }
}

std::atomic<CallLogger *> CallLogger::instance_;
std::mutex CallLogger::instance_mutex_;

CallLogger *CallLogger::GetInstance() {
  CallLogger *tmp = instance_.load(std::memory_order_relaxed);
  std::atomic_thread_fence(std::memory_order_acquire);
  if (tmp == nullptr) {
    std::lock_guard<std::mutex> lock(instance_mutex_);
    tmp = instance_.load(std::memory_order_relaxed);
    if (tmp == nullptr) {
      tmp = new CallLogger;
      std::atomic_thread_fence(std::memory_order_release);
      instance_.store(tmp, std::memory_order_relaxed);
    }
  }
  return tmp;
}

void CallLogRecord::GetCallLogMeta(Message &msg) {
  if (ctx_) {
    auto got = ctx_->client_metadata().find("log.meta-bin");
    if (got != ctx_->client_metadata().end()) {
      string str(got->second.begin(),
                 got->second.end());
      msg.ParseFromString(str);
    }
  }
}

CallLogger::CallLogger() {
  auto &c = libmaum::Config::Instance();
  logs_dir_ = c.Get("logs.dir", false);
}

void CallLogger::InitLogger(const string &name,
                            const char * logs_dir,
                            const string &process_name,
                            int interval_minute) {
  if (logs_dir && strlen(logs_dir)) {
    logs_dir_ = logs_dir;
  }
  string n = name;
  if (n.length() == 0) {
    n = "maum.ai";
  }
  string proc_name = process_name;
  if (proc_name.length() == 0) {
    proc_name = "maum";
  }

  auto logger = CreateIntervalLogger(n, logs_dir_, proc_name,
                                     interval_minute);
  logger->set_pattern("%v");
  logger_ = logger;

  timespec now;
  clock_gettime(CLOCK_REALTIME, &now);
  itimerspec itimer;

  // FIXME: 매 20초마다 TIMER를 돌리도록 한다.
  long interval = 20;
  itimer.it_value.tv_sec = now.tv_sec + interval;
  itimer.it_value.tv_nsec = now.tv_nsec;
  itimer.it_interval.tv_sec = interval;
  itimer.it_interval.tv_nsec = 0;

  timer_fd_ = timerfd_create(CLOCK_REALTIME, 0);
  timerfd_settime(timer_fd_, TFD_TIMER_ABSTIME, &itimer, nullptr);
  monitor_ = std::thread(&CallLogger::Monitor, this);
}

#include <sys/epoll.h>

void CallLogger::Monitor() {

  struct epoll_event ev;
  int epfd = epoll_create(1);
  if (epfd < 0) {
    LOGGER()->warn("epoll_create() failed: errno={}", errno);
    close(timer_fd_);
    return;
  }

  ev.events = EPOLLIN;
  if (epoll_ctl(epfd, EPOLL_CTL_ADD, timer_fd_, &ev) == -1) {
    LOGGER()->warn("epoll_ctl(ADD) failed: errno={}", errno);
    close(epfd);
    close(timer_fd_);
    return;
  }

  int64_t exp;
  while (!stop_) {
    memset(&ev, 0, sizeof(ev));
    int ret = epoll_wait(epfd, &ev, 1, 0);
    if (ret < 0) {
      LOGGER()->warn("epoll_wait() failed: errno={}", errno);
      close(epfd);
      close(timer_fd_);
      return;
    }
    ssize_t s = read(timer_fd_, &exp, sizeof(uint64_t));
    if (logger_) {
      if (s > 0)
        logger_->flush();
    }
  }

  close(timer_fd_);
}

} // namespace
} // namespace
