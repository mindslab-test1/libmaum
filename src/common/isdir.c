/*
 * Utility routines.
 *
 * Based in part on code from sash, Copyright (c) 1999 by David I. Bell
 * Permission has been granted to redistribute this code under the GPL.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <sys/stat.h>
#include "libmaum/common/fileutils.h"

/*
 * Return TRUE if a fileName is a directory.
 * Nonexistent files return FALSE.
 */
int IsDir(const char *fileName, int followLinks) {
  int status;
  struct stat statBuf;

  if (followLinks)
    status = stat(fileName, &statBuf);
  else
    status = lstat(fileName, &statBuf);

  if (status < 0 || !(S_ISDIR(statBuf.st_mode))) {
    status = 0;
  } else
    status = 1;

  return status;
}

/*
 * Return TRUE if a fileName is a directory.
 * Nonexistent files return FALSE.
 */
int IsFile(const char *fileName, int followLinks) {
  int status;
  struct stat statBuf;

  if (followLinks)
    status = stat(fileName, &statBuf);
  else
    status = lstat(fileName, &statBuf);

  if (status < 0 || !(S_ISREG(statBuf.st_mode))) {
    status = 0;
  } else
    status = 1;

  return status;
}
