#include "libmaum/common/fileutils.h"
#include "libmaum/common/config.h"

void Directory::doOpen() {
  if (dirname_[dirname_.size() - 1] == '/')
    dirname_.erase(dirname_.size() - 1);
  DIR *dp = opendir(dirname_.c_str());
  if (dp == 0) {
    int eno = errno;
    LOGGER()->critical("errno {}", eno);
    throw errno;
  }
  dp_ = dp;
}

void Directory::doClose() {
  if (dp_)
    (void) closedir(dp_);

  dp_ = 0;
}

int Directory::List(StringArr &files, unsigned long mask, bool recursive) {
  if (dp_ == 0)
    return 0;

  struct dirent *ep = 0;
  while ((ep = readdir(dp_))) {
    if (strcmp(ep->d_name, ".") == 0 ||
        strcmp(ep->d_name, "..") == 0) {
      if (ismasked(mask, DT_SELF)) {
        std::string full(dirname_);
        full += '/';
        full += ep->d_name;
        files.push_back(full);
      }
    } else if (ismasked(mask, DirFileType(ep->d_type))) {
      std::string full(dirname_);
      full += '/';
      full += ep->d_name;
      files.push_back(full);
    } else if (recursive && ep->d_type == DT_DIR) {
      std::string dirname(dirname_);
      dirname += '/';
      dirname += ep->d_name;
      Directory tempd(dirname.c_str());
      tempd.List(files, mask, true);
    }
  }
  delete ep;

  return (int)files.size();
}

