#include <sys/time.h>
#include <sys/resource.h>
#include <errno.h>
#include <stdio.h>
#include <memory.h>
#include <stdint.h>

void EnableCoreDump() {
  struct rlimit core_limit;
  core_limit.rlim_cur = RLIM_INFINITY;
  core_limit.rlim_max = RLIM_INFINITY;
  if (setrlimit(RLIMIT_CORE, &core_limit) < 0) {
    int err = errno;
    fprintf(stderr, "cannot enable core limit, %d %s\n", err, strerror(err));
  }
}

void SetNoFile(unsigned fd_count) {
  struct rlimit no_file_limit;
  if (fd_count > UINT16_MAX) {
    fprintf(stderr, "SetNoFile, too big request %d\n", fd_count);
  }
  no_file_limit.rlim_cur = fd_count;
  no_file_limit.rlim_max = UINT16_MAX;
  if (setrlimit(RLIMIT_NOFILE, &no_file_limit) < 0) {
    int err = errno;
    fprintf(stdout, "cannot set no file %d %s\n", err, strerror(err));
  }
}
