#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/stat.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include "libmaum/common/fileutils.h"

char *last_char_is(const char *s, int c) {
  if (s && *s) {
    size_t sz = strlen(s) - 1;
    s += sz;
    if ((unsigned char) *s == c)
      return (char *) s;
  }
  return NULL;
}

char *concat_path_file(const char *path, const char *filename) {
  char *lc;
  char *result;

  result = (char *) malloc((strlen(path) + strlen(filename)) * sizeof(char *));

  if (!path) {
    path = "";
  }
  lc = last_char_is(path, '/');
  while (*filename == '/') {
    filename++;
  }

  sprintf(result, "%s%s%s", path, (lc == NULL ? "/" : ""), filename);
  return result;
}

char *concat_subpath_file(const char *path, const char *f) {
  if (f && *f == '.' && (!f[1] || (f[1] == '.' && !f[2])))
    return NULL;
  return concat_path_file(path, f);
}

int RemoveFile(const char *path, int flags) {
  struct stat path_stat;
  int status = 0;

  if (lstat(path, &path_stat) < 0) {
    status = errno;
    goto finish;
  }

  if (!(flags & FILEUTILS_FORCE)) {
    status = EINVAL;
    goto finish;
  }

  if (S_ISDIR(path_stat.st_mode)) {
    DIR *dp;
    struct dirent *d;

    if (!(flags & FILEUTILS_RECUR)) {
      status = EINVAL;
      goto finish;
    }

    dp = opendir(path);
    if (dp == NULL) {
      status = errno;
      goto finish;
    }

    while ((d = readdir(dp)) != NULL) {
      char *new_path;

      new_path = concat_subpath_file(path, d->d_name);
      if (new_path == NULL)
        continue;

      if (RemoveFile(new_path, flags) < 0) {
        status = errno;
        free(new_path);
        goto finish;
      }
    }

    if (closedir(dp) < 0) {
      status = errno;
      goto finish;
    }

    if (rmdir(path) < 0) {
      status = errno;
    }
    goto finish;
  }

  if (unlink(path) < 0) {
    status = errno;
  }

  finish:
  errno = status;
  return status * -1;
}
