#include <cstring>
#include <memory>
#include <openssl/err.h>
#include <openssl/rsa.h>
#include <spdlog/fmt/fmt.h>

#include "libmaum/common/cryptoutils.h"

using std::string;
using std::vector;
using std::unique_ptr;
using fmt::format;

using EVP_MD_CTX_free_ptr = unique_ptr<EVP_MD_CTX, void (*)(EVP_MD_CTX *)>;
using EVP_CIPHER_CTX_free_ptr = unique_ptr<EVP_CIPHER_CTX,
                                           void (*)(EVP_CIPHER_CTX *)>;
using EVP_PKEY_CTX_free_ptr = unique_ptr<EVP_PKEY_CTX,
                                         void (*)(EVP_PKEY_CTX *)>;

namespace libmaum {

/**
 * SHA256 해쉬 함수를 수행하고 그 결과를 반환한다.
 *
 * @param data 데이터
 * @param len 데이터 길이
 * @param out 해쉬된 원본 데이터
 */
void Sha256(const void *data, size_t len, vector<uint8_t> &out) {
#ifdef EVP_MD_CTX_destroy
  EVP_MD_CTX_free_ptr mdctx(EVP_MD_CTX_new(), EVP_MD_CTX_free);
#else
  EVP_MD_CTX_free_ptr mdctx(EVP_MD_CTX_create(), EVP_MD_CTX_destroy);
#endif

  if (!mdctx) {
    char buf[BUFSIZ];
    throw std::runtime_error(
        fmt::format("EVP_MD_CTX_create failed {}",
                    ERR_error_string(ERR_get_error(), buf)));
  }

  if (1 != EVP_DigestInit_ex(mdctx.get(), EVP_sha256(), nullptr)) {
    char buf[BUFSIZ];
    throw std::runtime_error(
        fmt::format("EVP_DigestInit_ex failed {}",
                    ERR_error_string(ERR_get_error(), buf)));
  }

  if (1 != EVP_DigestUpdate(mdctx.get(), data, len)) {
    char buf[BUFSIZ];
    throw std::runtime_error(
        fmt::format("EVP_DigestUpdate failed {}",
                    ERR_error_string(ERR_get_error(), buf)));
  }

  std::vector<uint8_t> digest(EVP_MD_size(EVP_sha256()));
  unsigned int d_len;

  if (1 != EVP_DigestFinal_ex(mdctx.get(), digest.data(), &d_len)) {
    char buf[BUFSIZ];
    throw std::runtime_error(
        fmt::format("EVP_DigestFinal_ex failed {}",
                    ERR_error_string(ERR_get_error(), buf)));
  }

  out.clear();
  std::copy(digest.begin(), digest.end(), back_inserter(out));
}

/**
 * AES-128 알고리즘을 이용해서 CBC 방식으로 복호화한다.
 *
 * @param key 키
 * @param iv IV
 * @param cipher_text 암호화 원문
 * @param clear_text 복호화된 복호문
 */
void Aes128CbcDecrypt(const uint8_t *key,
                      const uint8_t *iv,
                      const vector<uint8_t> &cipher_text,
                      string &clear_text) {
  EVP_CIPHER_CTX_free_ptr ctx(EVP_CIPHER_CTX_new(), ::EVP_CIPHER_CTX_free);
  int rc = EVP_DecryptInit_ex(ctx.get(), EVP_aes_128_cbc(), NULL, key, iv);
  if (rc != 1) {
    char buf[BUFSIZ];
    throw std::runtime_error(
        fmt::format("EVP_DecryptInit_ex failed {}",
                    ERR_error_string(ERR_get_error(), buf)));

  }

  // Recovered text contracts upto BLOCK_SIZE
  clear_text.resize(cipher_text.size());
  int out_len1 = (int) clear_text.size();

  rc = EVP_DecryptUpdate(ctx.get(), (uint8_t *) &clear_text[0], &out_len1,
                         &cipher_text[0], (int) cipher_text.size());
  if (rc != 1) {
    char buf[BUFSIZ];
    throw std::runtime_error(
        fmt::format("EVP_DecryptUpdate failed {}",
                    ERR_error_string(ERR_get_error(), buf)));
  }

  int out_len2 = (int) clear_text.size() - out_len1;
  rc = EVP_DecryptFinal_ex(ctx.get(),
                           (uint8_t *) &clear_text[0] + out_len1,
                           &out_len2);
  if (rc != 1) {
    char buf[BUFSIZ];
    throw std::runtime_error(
        fmt::format("EVP_DecryptFinal_ex failed {}",
                    ERR_error_string(ERR_get_error(), buf)));
  }
  // Set recovered text size now that we know it
  clear_text.resize(out_len1 + out_len2);
}

/**
 * AES-128 알고리즘을 이용해서 CBC 방식으로 복호화한다.
 *
 * @param key 암호화 키
 * @param iv 암호화 IV
 * @param clear_text 평문 원문
 * @param cipher_text 암호화된 암호문
 */
void Aes128CbcEncrypt(const uint8_t *key,
                      const uint8_t *iv,
                      const string &clear_text,
                      vector<uint8_t> &cipher_text) {
  EVP_CIPHER_CTX_free_ptr ctx(EVP_CIPHER_CTX_new(), ::EVP_CIPHER_CTX_free);
  int rc = EVP_EncryptInit_ex(ctx.get(), EVP_aes_128_cbc(), nullptr, key, iv);
  if (rc != 1) {
    char buf[BUFSIZ];
    throw std::runtime_error(
        fmt::format("EVP_EncryptInit_ex failed {}",
                    ERR_error_string(ERR_get_error(), buf)));
  }


  // Recovered text contracts upto BLOCK_SIZE
  int expected = (int) ((clear_text.size() / kKeyLength) + 1) * kKeyLength;
  cipher_text.resize((unsigned) expected);
  int out_len1 = 0;

  rc = EVP_EncryptUpdate(ctx.get(),
                         (uint8_t *) &cipher_text[0],
                         &out_len1,
                         (const uint8_t *) clear_text.c_str(),
                         (int) clear_text.size());
  if (rc != 1) {
    char buf[BUFSIZ];
    throw std::runtime_error(
        fmt::format("EVP_EncryptUpdate failed {}",
                    ERR_error_string(ERR_get_error(), buf)));
  }

  int out_len2 = (int) expected - out_len1;
  rc = EVP_EncryptFinal_ex(ctx.get(),
                           (uint8_t *) &cipher_text[0] + out_len1,
                           &out_len2);
  if (rc != 1) {
    char buf[BUFSIZ];
    throw std::runtime_error(
        fmt::format("EVP_EncryptFinal_ex failed {}",
                    ERR_error_string(ERR_get_error(), buf)));
  }
  // Set recovered text size now that we know it
  cipher_text.resize((unsigned) (out_len1 + out_len2));
}

/**
 * PKI를 이용해서 원문을 SHA256을 통해서 서명하고, 서명 데이터를 반환한다.
 *
 * @param pkey PKI 키
 * @param msg 원문 메시지
 * @param sign 서명된 메시지
 */
void Sign(EVP_PKEY *pkey, const string &msg, vector<uint8_t> &sign) {
  if (msg.empty() || !pkey) {
    throw std::invalid_argument("msg empty or private key is null!");
  }

#ifdef EVP_MD_CTX_destroy
  EVP_MD_CTX_free_ptr ctx(EVP_MD_CTX_new(), EVP_MD_CTX_free);
#else
  EVP_MD_CTX_free_ptr ctx(EVP_MD_CTX_create(), EVP_MD_CTX_destroy);
#endif

  if (ctx == nullptr) {
    char buf[BUFSIZ];
    throw std::runtime_error(
        fmt::format("EVP_MD_CTX_create failed {}",
                    ERR_error_string(ERR_get_error(), buf)));
  }

  int rc = EVP_DigestSignInit(ctx.get(), nullptr, EVP_sha256(), nullptr, pkey);
  if (rc != 1) {
    char buf[BUFSIZ];
    throw std::runtime_error(
        fmt::format("EVP_DigestSignInit failed {}",
                    ERR_error_string(ERR_get_error(), buf)));
  }

  rc = EVP_DigestSignUpdate(ctx.get(), msg.c_str(), msg.length());
  if (rc != 1) {
    char buf[BUFSIZ];
    throw std::runtime_error(
        fmt::format("EVP_DigestSignUpdate failed {}",
                    ERR_error_string(ERR_get_error(), buf)));
  }

  size_t req = 0;
  rc = EVP_DigestSignFinal(ctx.get(), nullptr, &req);
  if (rc != 1) {
    char buf[BUFSIZ];
    throw std::runtime_error(
        fmt::format("EVP_DigestSignFinal test failed {}",
                    ERR_error_string(ERR_get_error(), buf)));
  }

  if (req <= 0) {
    char buf[BUFSIZ];
    throw std::runtime_error(
        fmt::format("EVP_DigestSignFinal test failed {}",
                    ERR_error_string(ERR_get_error(), buf)));
  }

  sign.resize(req);

  size_t slen = req;
  rc = EVP_DigestSignFinal(ctx.get(), &sign[0], &slen);
  if (rc != 1) {
    char buf[BUFSIZ];
    throw std::runtime_error(
        fmt::format("EVP_DigestSignFinal test failed {}",
                    ERR_error_string(ERR_get_error(), buf)));
  }
}

/**
 * PKI를 이용해서 원문을 SHA256을 통해서 서명하고, 서명 데이터를 반환한다.
 *
 * @param pkey PKI 키
 * @param msg 원문 메시지
 * @param sign 검토에 사용할 서명
 */
void Verify(EVP_PKEY *pkey, const string &msg, const vector<uint8_t> &sign) {
  if (msg.empty() || !pkey || sign.empty()) {
    throw std::invalid_argument(
        "msg empty or private key is null or sign is empty!");
  }

#ifdef EVP_MD_CTX_destroy
  EVP_MD_CTX_free_ptr ctx(EVP_MD_CTX_new(), EVP_MD_CTX_free);
#else
  EVP_MD_CTX_free_ptr ctx(EVP_MD_CTX_create(), EVP_MD_CTX_destroy);
#endif

  if (ctx == nullptr) {
    char buf[BUFSIZ];
    throw std::runtime_error(
        fmt::format("EVP_MD_CTX_create failed {}",
                    ERR_error_string(ERR_get_error(), buf)));
  }

  int rc =
      EVP_DigestVerifyInit(ctx.get(), nullptr, EVP_sha256(), nullptr, pkey);
  if (rc != 1) {
    char buf[BUFSIZ];
    throw std::runtime_error(
        fmt::format("EVP_DigestVerifyInit failed {}",
                    ERR_error_string(ERR_get_error(), buf)));
  }

  rc = EVP_DigestVerifyUpdate(ctx.get(), msg.data(), msg.length());
  if (rc != 1) {
    char buf[BUFSIZ];
    throw std::runtime_error(
        fmt::format("EVP_DigestVerifyUpdate failed {}",
                    ERR_error_string(ERR_get_error(), buf)));
  }

  /* Clear any errors for the call below */
  ERR_clear_error();

  rc = EVP_DigestVerifyFinal(ctx.get(), sign.data(), sign.size());
  if (rc != 1) {
    char buf[BUFSIZ];
    throw std::runtime_error(
        fmt::format("EVP_DigestVerifyFinal failed {}",
                    ERR_error_string(ERR_get_error(), buf)));
  }
}

/**
 * PKI 키를 이용해서 서명으로부터 원문 서명을 꺼내온다.
 * @param pkey PKI 키
 * @param pki_sign PKI를 통해서 만들어진 서명
 * @param sign 원문 해쉬
 */
void Recover(EVP_PKEY *pkey,
             const vector<uint8_t> &pki_sign,
             vector<uint8_t> &sign) {
  EVP_PKEY_CTX_free_ptr ctx(EVP_PKEY_CTX_new(pkey, nullptr),
                            ::EVP_PKEY_CTX_free);

  int rc = EVP_PKEY_verify_recover_init(ctx.get());
  if (rc != 1) {
    char buf[BUFSIZ];
    throw std::runtime_error(
        fmt::format("EVP_PKEY_verify_recover_init failed {}",
                    ERR_error_string(ERR_get_error(), buf)));
  }

  /* Error */
  if (EVP_PKEY_CTX_set_rsa_padding(ctx.get(), RSA_PKCS1_PADDING) <= 0) {
    char buf[BUFSIZ];
    throw std::runtime_error(
        fmt::format("EVP_PKEY_CTX_set_rsa_padding failed {}",
                    ERR_error_string(ERR_get_error(), buf)));
  }
  /* Error */
  if (EVP_PKEY_CTX_set_signature_md(ctx.get(), EVP_sha256()) <= 0) {
    char buf[BUFSIZ];
    throw std::runtime_error(
        fmt::format("EVP_PKEY_CTX_set_signature_md failed {}",
                    ERR_error_string(ERR_get_error(), buf)));
  }

  size_t outlen = 0;
  rc = EVP_PKEY_verify_recover(ctx.get(), nullptr, &outlen,
                               pki_sign.data(), pki_sign.size());
  if (rc != 1) {
    char buf[BUFSIZ];
    throw std::runtime_error(
        fmt::format("EVP_PKEY_verify_recover test {}",
                    ERR_error_string(ERR_get_error(), buf)));
  }

  sign.resize(outlen);
  rc = EVP_PKEY_verify_recover(ctx.get(), sign.data(), &outlen,
                               pki_sign.data(), pki_sign.size());

  if (rc != 1) {
    char buf[BUFSIZ];
    throw std::runtime_error(
        fmt::format("EVP_PKEY_verify_recover {}",
                    ERR_error_string(ERR_get_error(), buf)));
  }
  sign.resize(outlen);
}

}
