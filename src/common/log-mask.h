#ifndef LIBMAUM_LOG_MASK_H
#define LIBMAUM_LOG_MASK_H

#include <string>
#include <vector>
#include <array>
#include <pcre.h>
#include <cstring>
#include <spdlog/details/log_msg.h>

using std::string;
using std::vector;

class LogMask {

 public:
  LogMask();
  ~LogMask();

  // pcre 를 통한 정규 표현식 처리를 한다.
  string Mask(const string &msg);

 private:
  pcre *CompileRegex(const char *regex);
  string PcreExec(pcre *re_compiled, const char *input, int mask_index);

 private:
  // 정규 표현식 관련 변수
  std::vector<pcre *> regex_res_;
};

#endif //LIBMAUM_LOG_MASK_H
