#include "libmaum/common/encoding.h"

#include <stdlib.h>
#include <errno.h>
#include <stdlib.h>
#include <iconv.h>

#include <string.h>

int ChangeEncoding(const char *from, const char *to,
                   const char *src, char **dst) {
    size_t srclen;
    size_t dstlen;
    size_t inleftsize, outleftsize;
    size_t res;                         /* report of iconv */
    iconv_t cd = (iconv_t) -1;

    char *inptr;
    char *outptr;

    /* open iconv */
    cd = iconv_open(to, from);
    if (cd == (iconv_t) (-1)) {
        return -1;
        // return *((int *) cd);
        // 64bit 시스템에서 오류이 위험성 있어서 원 개발의 의도에 맞춰서
        // -1을 반한한다.
    }
    if (!strcmp(from, "UCS-2"))
        inleftsize = 2;
    else {
        srclen = (size_t) strlen(src);
        inleftsize = srclen;
    }
    outleftsize = (size_t) (inleftsize * (size_t) 4);
    *dst = (char *) malloc(outleftsize);
    dstlen = outleftsize;
    inptr = (char *) src;
    outptr = *dst;

    while (true) {
        res = iconv(cd, (char **) &inptr,
                    &inleftsize, &outptr,
                    &outleftsize);
        if (res == (size_t) (-1)) {
            if (errno == EILSEQ) { /* not defined char in the table ? */
                inptr++;
                inleftsize--;
            } else if (errno == EINVAL) {
                /* incomplete char, need readin more codes */
                if (inleftsize <= 2) {
                    *outptr = '?';
                    outleftsize--;
                    break;
                }
            }
            *outptr = '?';
            outptr++;
            outleftsize--;
            inptr++;
            inleftsize--;
        } else break;
    }
    (*dst)[dstlen - outleftsize] = '\0';
    /* close iconv */
    iconv_close(cd);
    return (int) (dstlen - outleftsize);
}

int ChangeEncoding(const char *from, const char *to,
                   const char *src, std::string &out) {
    char *dest = nullptr;
    int rc = -1;

    // src가 empty가 아닌 경우 Encoding 수행
    if (src && *src) {
      rc = ChangeEncoding(from, to, src, &dest);
    }

    if (dest != nullptr) {
        if (rc >= 0)
            out = dest;
        free(dest);
    }

    return rc;
}

#define UTF8_3BYTE_CHAR 0xE0
#define UTF8_2BYTE_CHAR 0xC0
#define UTF8_MASK 0xF0

int ChangeEncodingUtfToEuc(const char *to, const char *src, char **dst) {
    size_t srclen;
    size_t dstlen;
    size_t inleftsize, outleftsize;
    iconv_t cd = (iconv_t) -1;

    char *inptr;
    char *outptr;

    /* open iconv */
    std::string to_trans(to);
    to_trans += "//translit//ignore";
    cd = iconv_open(to_trans.c_str(), "UTF-8");
    if (cd == (iconv_t) (-1)) {
        return -1;
        // return *((int *) cd);
        // 64bit 시스템에서 오류이 위험성 있어서 원 개발의 의도에 맞춰서
        // -1을 반한한다.
    }

    srclen = (size_t) strlen(src);
    inleftsize = srclen;
    outleftsize = (size_t) (inleftsize * (size_t) 4);
    *dst = (char *) malloc(outleftsize);
    dstlen = outleftsize;
    inptr = (char *) src;
    outptr = *dst;

    // convert

    while (inptr && *inptr) {

        // euc-kr로 변환
        if (iconv(cd, &inptr, &inleftsize, &outptr, &outleftsize) == (size_t) -1) {
            if (EILSEQ == errno) {
                // replace with '?'
                (*dst)[dstlen - outleftsize] = '?';

                // go to other character position of output
                ++outptr;
                --outleftsize;
                size_t readUnit = 0;    // check unit
                int maskVal = inptr[0] & UTF8_MASK;

                if (maskVal == UTF8_2BYTE_CHAR) {
                    // 2바이트이면
                    readUnit = 2;
                } else if (maskVal == UTF8_3BYTE_CHAR) {
                    // 3바이트이면(한글)
                    readUnit = 3;
                } else if (maskVal == UTF8_MASK) {
                    // UCS4영역
                    readUnit = 4;
                } else {
                    // 기본문자
                    readUnit = 1;
                }

                // go to other character
                inptr += readUnit;
                inleftsize -= readUnit;
            } else if (EINVAL == errno) {
                inptr++;
                inleftsize--;
            } else {
            }
        }

    }
    (*dst)[dstlen - outleftsize] = '\0';
    /* close iconv */
    iconv_close(cd);
    return (int) (dstlen - outleftsize);
}

int ChangeEncodingUtfToEuc(const char *to, const char *src, std::string &out) {
  char *dest = nullptr;
  int rc = -1;

  // src가 empty가 아닌 경우 Encoding 수행
  if (src && *src) {
    rc = ChangeEncodingUtfToEuc(to, src, &dest);
  }

  out = rc < 0 ? std::string("") : dest;

  if (dest != nullptr) {
    free(dest);
  }

  return rc;
}
