#include <fstream>
#include <iostream>
#include <maum/license/license.pb.h>
#include <libmaum/common/license.h>
#include <getopt.h>
#include <google/protobuf/util/json_util.h>

#include <gitversion/version.h>
#include <libmaum/common/util.h>
#include <libmaum/common/config.h>
#include "libmaum/common/cryptoutils.h"

using std::cout;
using std::cerr;
using std::endl;
using std::ofstream;
using std::ios_base;

using google::protobuf::util::JsonStringToMessage;
using maum::license::License;

using libmaum::kKeyLength;
using libmaum::Aes128CbcEncrypt;

void Query() {
  cout << endl;

  int cpus, total_cores;

  if (GetCpuCores(cpus, total_cores)) {
    cout << "CPU(s): " << cpus << endl
         << "CPU Core(s): " << total_cores << endl;
  } else {
    cout << "Failed to get CPU Core(s)" << endl;
  }

  char ip_addr[30];
  GetPrimaryIp(ip_addr, sizeof ip_addr);
  string mac = GetMacAddress(ip_addr);

  cout << "Primary IP: " << ip_addr << endl
       << "MAC Address: " << mac << endl
       << endl;

  exit(EXIT_SUCCESS);
}

void Coupon(string &mac) {
  string coupon_file = string(::getenv("HOME")) + "/.maumai-coupon";

  struct stat buffer;
  if (stat(coupon_file.c_str(), &buffer) == 0) {
    cerr << "A coupon is already issued." << endl;
    exit(EXIT_SUCCESS);
  }

  string now;
  srand((unsigned int) time(nullptr) ^ pthread_self());
  for (int i = 0; i < 173; i++) {
    now += fmt::format("{0:08x}", (int32_t) rand());
  }
  now += fmt::format("{0:08x}", time(nullptr));
  for (int i = 0; i < 149; i++) {
    now += fmt::format("{0:08x}", (int32_t) rand());
  }

  vector<uint8_t> iv_key;
  vector<uint8_t> coupon;
  uint8_t *value;
  string description;
  auto out_iter = std::back_inserter(iv_key);

  if (mac.empty()) {
    char ip_addr[30];
    GetPrimaryIp(ip_addr, sizeof ip_addr);
    mac = GetMacAddress(ip_addr);
  }
  std::transform(mac.begin(), mac.end(), mac.begin(), ::toupper);

  description = License::Descriptions_Name(License::validTo);
  std::copy(description.begin() + 1, description.end() - 2, out_iter);
  value = (uint8_t *) &License::dedicated;
  std::copy(value + 2, value + 4, out_iter);
  std::copy(mac.begin(), mac.end(), out_iter);
  description = License::Descriptions_Name(License::allowedTo);
  std::copy(description.begin() + 2, description.end() - 2, out_iter);
  value = (uint8_t *) &License::expiration;
  std::copy(value, value + 4, out_iter);

  Aes128CbcEncrypt(iv_key.data(), iv_key.data() + kKeyLength, now, coupon);

  ofstream ofs(coupon_file, ios_base::out | ios_base::binary);
  ofs.write((const char *) coupon.data(), coupon.size());
  ofs.close();

  cout << "A coupon has been issued." << endl;

  exit(EXIT_SUCCESS);
}

static const char *kOptionsString = R"(options:
  -h --help      : show this message.
  -l license.pem : specify license file. If not specified, use default.
  --license=license.pem
  -q, --query    : query specifications of system.
)";

void Help(const char *prname, const char *msg = nullptr) {
  if (msg)
    cout << msg << endl;

  cout << prname << " [-l license.pem]" << endl
       << endl
       << kOptionsString << endl
       << "[" << version::VERSION_STRING << "]" << endl;

  exit(EXIT_SUCCESS);
}

void ProcessOptions(int argc, char *argv[], string &license_file) {
  const char *prname = basename(argv[0]);
  int c;

  license_file.clear();

  while (true) {
    static const struct option long_options[] = {
        {"help", no_argument, nullptr, 'h'},
        {"license", required_argument, nullptr, 'l'},
        {"query", no_argument, nullptr, 'q'},
        {"coupon", optional_argument, nullptr, 'c'},
        {nullptr, no_argument, nullptr, 0}
    };

    /* getopt_long stores the option index here. */
    int option_index = 0;

    c = getopt_long(argc, argv, "hl:q", long_options, &option_index);

    /* Detect the end of the options. */
    if (c == -1) {
      break;
    }

    switch (c) {
      case 0: {
        break;
      }
      case 'h':
      case '?':
      default: {
        Help(prname);
        break;
      }
      case 'l': {
        license_file = optarg;
        break;
      }
      case 'q': {
        Query();
        break;
      }
      case 'c': {
        string mac = optarg == nullptr ? "" : optarg;
        Coupon(mac);
        break;
      }
    }
  }

  if (argc == 1 && license_file.empty()) {
    string maum_root = libmaum::Config::GetMaumRoot();
    string temp_file = maum_root;
    temp_file += "/license/maumai-license.pem";
    license_file = temp_file;
  }

  if (license_file.empty()) {
    Help(prname);
  }
}

void DumpLicense(const License &lic, const char *msg) {
  google::protobuf::util::JsonOptions options;
  options.add_whitespace = true;
  options.always_print_primitive_fields = true;
  string json;
  google::protobuf::util::MessageToJsonString(lic, &json, options);

  cout << "You have the following license." << endl << endl
       << "MAUM.AI License: " << msg << endl
       << "-----" << endl
       << json << endl
       << "-----" << endl;
}

int main(int argc, char **argv) {
  string license_file;
  ProcessOptions(argc, argv, license_file);
  try {
    License lic;
    libmaum::LicenseUnseal(lic, license_file);
    DumpLicense(lic, license_file.c_str());
  } catch (const std::exception &e) {
    cerr << "cannot unseal license : " << e.what() << endl;
    cerr << "License file is " << license_file << endl;
  } catch (...) {
    cerr << "License file is " << license_file << endl;
  }

  return 0;
}
