cmake_minimum_required(VERSION 2.8.12)

project(maum-json CXX)

include_directories(.)

set(OUTLIB maum-json)

set(CMAKE_CXX_STANDARD 11)

if (CMAKE_COMPILER_IS_GNUCXX)
  add_definitions(-std=c++11)
endif ()

set(SOURCE_FILES jsoncpp.cpp)

add_library(${OUTLIB} SHARED ${SOURCE_FILES})

if (APPLE)
  set_target_properties(${OUTLIB}
      PROPERTIES MACOSX_RPATH ${CMAKE_INSTALL_PREFIX}/lib)
  target_link_libraries(${OUTLIB} ${LIBEXT})
endif()

install(TARGETS ${OUTLIB}
    RUNTIME DESTINATION bin COMPONENT libraries
    LIBRARY DESTINATION lib COMPONENT libraries
    ARCHIVE DESTINATION lib COMPONENT libraries)

install(DIRECTORY json/ DESTINATION include/json
    FILES_MATCHING PATTERN "*.h")
