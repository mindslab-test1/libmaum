cmake_minimum_required(VERSION 2.8.12)

project(sample-log-monitor CXX)

include_directories(.)
include_directories(../../src/spdlog)

set(OUTLIB sample-log-monitor)

set(CMAKE_CXX_STANDARD 11)

if (CMAKE_COMPILER_IS_GNUCXX)
  set(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -std=c++11")
endif ()

set(SOURCE_FILES
    sample-log-monitor.cc
    )

add_library(${OUTLIB} SHARED ${SOURCE_FILES})

target_link_libraries(${OUTLIB}
    )

#install(TARGETS ${OUTLIB}
#    RUNTIME DESTINATION bin COMPONENT libraries
#    LIBRARY DESTINATION lib COMPONENT libraries
#    ARCHIVE DESTINATION lib COMPONENT libraries)
