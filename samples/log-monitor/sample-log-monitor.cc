#include <spdlog/spdlog.h>
#include <memory>

using namespace std;

using log_msg = spdlog::details::log_msg;

class SampleLogMonitor {
 public:
  SampleLogMonitor() {

  }

  ~SampleLogMonitor() {

  }

  void Write(const log_msg * msg) {
    count_++;
    fprintf(stderr, "%ld %s %s\n",
            count_,
            msg->logger_name->c_str(),
            fmt::to_string(msg->raw).c_str());
  }

 private:
  int64_t count_ = 0;
};


shared_ptr<SampleLogMonitor> g_monitor_sink;

extern "C" {
  void write_sample(const void *msg) {
    if (g_monitor_sink == nullptr) {
      g_monitor_sink = make_shared<SampleLogMonitor>();
    }
    const log_msg * m = (const log_msg*)msg;
    g_monitor_sink->Write(m);
  }
}