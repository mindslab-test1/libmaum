#include "libmaum/common/config.h"
#include <string>
#include <iostream>


//
// README
//
// 빌드된 test_config 파일을 현재의 디렉토리에 복사한 이후에 동작시킵니다.
// MAUM_ROOT=YOUR_PATH ./test_config 로 실행합니다.
//
// logs 디렉토리가 생깁니다.
// 여기에 실행 결과가 나옵니다.
//

void TestDecrypt() {
  LOGGER()->debug("Test Decrypt");
  auto & c = libmaum::Config::Instance();
  auto enc = c.Get("test.enc");
  auto val = c.GetDecrypt("test.enc");
  assert(val == "All examples are available on Github");
  LOGGER()->debug("GetDecrypt returns {}, enc {}", val, enc);

  auto enc2 = c.Get("test.enc2");
  auto val2 = c.GetDecrypt("test.enc2");
  assert(val2 == "MAUM.ai");
  LOGGER()->debug("GetDecrypt returns {}, enc {}", val2, enc2);

}

void TestAddConfigFile() {
  LOGGER()->debug("TestAddConfigFile");
  auto & c = libmaum::Config::Instance();
  c.AddConfigFile("second.conf");
  auto name = c.Get("second.name");
  auto id = c.Get("second.id");
  LOGGER()->debug("second.name is {}, second.id is {}", name, id);
}

void TestSetConfig() {
  LOGGER()->debug("TestSetConfig");
  auto & c = libmaum::Config::Instance();
  c.Set("second.id", "4321");
  c.Set("second.notused", "notused");

  auto name = c.Get("second.id");
  auto not_used= c.Get("second.notused");
  LOGGER()->debug("second.id is {}, second.notused is {}", name, not_used);
}

static char cwd_buf[PATH_MAX];

void TestReloadConfig() {
  LOGGER()->debug("TestReloadConfig");
  std::string name;
  std::string id;

  char temp_name[PATH_MAX];
  char temp_to_name[PATH_MAX];

  auto & c = libmaum::Config::Instance();

  // RENAME FILE
  snprintf(temp_name, sizeof temp_name, "%s/second.conf", cwd_buf);
  snprintf(temp_to_name, sizeof temp_to_name, "%s/second.conf.bak", cwd_buf);
  rename(temp_name, temp_to_name);

  // RENAME FILE
  snprintf(temp_name, sizeof temp_name, "%s/second.reload.conf", cwd_buf);
  snprintf(temp_to_name, sizeof temp_to_name, "%s/second.conf", cwd_buf);
  rename(temp_name, temp_to_name);

  // RELOAD
  c.Reload();

  name = c.Get("test.name");
  id = c.Get("test.id");
  LOGGER()->debug("test.name is {}, test.id is {}", name, id);

  name = c.Get("second.name");
  id = c.Get("second.id");
  LOGGER()->debug("second.name is {}, second.id is {}", name, id);

  // RENAME FILE
  // RENAME FILE
  snprintf(temp_name, sizeof temp_to_name, "%s/second.conf", cwd_buf);
  snprintf(temp_to_name, sizeof temp_name, "%s/second.reload.conf", cwd_buf);
  rename(temp_name, temp_to_name);

  snprintf(temp_name, sizeof temp_to_name, "%s/second.conf.bak", cwd_buf);
  snprintf(temp_to_name, sizeof temp_name, "%s/second.conf", cwd_buf);
  rename(temp_name, temp_to_name);
}

int main(int argc, char* argv[]) {
  getcwd(cwd_buf, sizeof cwd_buf);

  libmaum::Config::Init(argc, argv, "test.conf");

  chdir(cwd_buf);

  TestDecrypt();
  TestAddConfigFile();
  TestSetConfig();
  TestReloadConfig();

  return 0;
}
