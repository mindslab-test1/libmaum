#include "libmaum/common/config.h"
#include <string>
#include <iostream>

int main(int argc, char* argv[]) {
  libmaum::Config::Init(argc, argv, "test.conf");

  LOGGER()->flush_on(spdlog::level::debug);

  // 총 84MByte크기의 로그 파일이 만들어짐
  for (int i = 0; i < 10 * 1024 * 1024; i++) {
    LOGGER()->debug("this is test");
  }

  return 0;
}
