#include <json/json.h>

const char * kJsonSample = R"JSON(
{
  "root": {
    "mem-1": {
      "arr-1": [
        {
          "l": 0,
          "m": false,
          "n": "n string"
        },
        {
          "l": 2,
          "m": false,
          "n": "nnnnnnn"
        },
        {
          "l": 4,
          "m": false,
          "n": "n string"
        }
      ],
      "str-2": "value str - 2"
    },
    "mem-2": {
      "arr-1": [
        {
          "l": 0,
          "m": false,
          "n": "n string"
        },
        {
          "l": 2,
          "m": false,
          "n": "n string"
        },
        {
          "l": 4,
          "m": false,
          "n": "n string"
        }
      ],
      "str-2": "value str - 2"
    },
    "mem-3": {
      "arr-1": [
        {
          "l": 0,
          "m": false,
          "n": "n string"
        },
        {
          "l": 2,
          "m": false,
          "n": "n string"
        },
        {
          "l": 4,
          "m": false,
          "n": "n string"
        }
      ],
      "str-2": "mem-3-str-2"
    },
    "mem-4": {
      "arr-1": [
        {
          "l": 0,
          "m": false,
          "n": "n string"
        },
        {
          "l": 2,
          "m": false,
          "n": "n string"
        },
        {
          "l": 4,
          "m": true,
          "n": "n string"
        }
      ],
      "str-2": "value str - 2"
    },
    "direct": "direct defined"
  }
}
)JSON";

#include <iostream>
#include <cstring>

using namespace std;

int main() {
  Json::Value root;
  Json::CharReaderBuilder builder;
  Json::CharReader * reader = builder.newCharReader();
  std::string error;
  if (!reader->parse(kJsonSample, kJsonSample + strlen(kJsonSample), &root, &error)) {
    cout << "parse error: " << error << endl;
    return -1;
  }

  const char * keys[] = {
      "root.mem-2.arr-1[0].l",
      "root.mem-4.arr-1[2].m",
      "root.mem-1.arr-1[1].n",
      "root.mem-3.str-2",
      "root.direct"
  };

  const char * values[] = {
      "0",
      "true",
      "nnnnnnn",
      "mem-3-str-2",
      "direct defined"
  };

  for (int i = 0; i < 5; i++ ) {
    Json::Path path(keys[i]);
    auto &v = path.resolve(root);
    cout << "resolved: v = " << v.toStyledString() << endl;
    string v_str(v.asString());
    string cmp(values[i]);
    if (v_str != cmp) {
      cout << "invalid result: " << cmp << endl;
    }
  }
  return 0;
}
