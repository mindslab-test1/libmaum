# -*- coding:utf-8 -*-

"""
설정 파일의 내용을 읽어서 이를 반환한다.
싱글톤으로 동작한다.
"""
import base64
import binascii
import os
import subprocess
import sys
import tempfile


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(
                *args, **kwargs)
        return cls._instances[cls]


class Config(object):
    """
    설정파일을 로딩한다.
    설정 파일에 있는 값을 꺼낸다.
    암호화되어 있는 경우에는 decrypt()함수를 통해서 복호화 처리한다.
    """
    __metaclass__ = Singleton
    config_file = ''
    additional_filenames = []
    config_map = {}
    maum_root = ''
    conf_path = ''
    conf_path_alter = ''

    def __init__(self):
        self.maum_root = self.get_maum_root()
        if 'PYTHONPATH' in os.environ:
            # 이 루틴은 개발자 환경에서 테스트를 지원하기 위한 목적인데
            # 간헐적으로 오동작하는 경우가 많아서 `conf_path_alter`를 추가합니다.
            # for simple server connect test and unit test
            cur_path = os.environ.get('PYTHONPATH').split(":")[0]
            self.conf_path = os.path.join(cur_path.split('pysrc')[0],
                                          'config')
            # for installed environment
            self.conf_path_alter = os.path.realpath(
                os.path.join(self.maum_root, 'etc'))
        else:
            # for installed environment
            self.conf_path = os.path.realpath(
                os.path.join(self.maum_root, 'etc'))

    def init(self, filename):
        """
        기본 config 파일 경로
        """
        conf_file = self.find_config_file(filename, False)
        self.parse_config(conf_file)

    def add_config_file(self, filename):
        """
        두번째 config 파일을 추가합니다.
        :param filename: 경로가 없는 config 파일 이름
        """
        conf_file = self.find_config_file(filename, True)
        self.parse_config(conf_file)

    @staticmethod
    def _conf_file(path, filename):
        return os.path.realpath(os.path.join(path, filename))

    def find_config_file(self, filename, secondary):
        """
        경로를 찾아낸다.
        :param filename: config 파일 이름
        :param secondary: 추가적인 파일 인지 여부
        :return: 전체 경로
        """
        cf = self._conf_file(self.conf_path, filename)
        if not os.path.exists(cf):
            if len(self.conf_path_alter) > 0:
                cf = Config._conf_file(self.conf_path_alter, filename)
                if not os.path.exists(cf):
                    raise RuntimeError('config file not exist: {}'.format(cf))
            else:
                raise RuntimeError('config file not exist: {}'.format(cf))
        if secondary:
            self.additional_filenames.append(cf)
        else:
            self.config_file = cf
        return cf

    @staticmethod
    def get_maum_root():
        maum_root = os.environ.get('MAUM_ROOT')
        if maum_root is None:
            home = os.environ.get('HOME')
            if home is not None:
                maum_root = os.path.join(home, 'maum')
            else:
                exe_path = os.path.realpath(sys.argv[0])
                bin_path = os.path.dirname(exe_path)
                p_path = os.path.realpath(os.path.join(bin_path, '..'))
                print('set as maum_root = {}'.format(p_path))
                maum_root = p_path
        return maum_root

    @staticmethod
    def decrypt_text(cipher):
        """
        :param cipher: 암호화된 설정 문자열
        :return: 복호화된 문자열
        """
        b_enc_str = base64.decodestring(cipher)
        if b_enc_str < (16 * 3):
            return False
        iv_bin = ''
        key_bin = ''
        enc_bin = ''

        for i in range(0, (16 * 3)):
            mod = i % 3
            if mod == 0:
                enc_bin += b_enc_str[i]
            elif mod == 1:
                iv_bin += b_enc_str[i]
            else:
                key_bin += b_enc_str[i]
        for i in range((16 * 3), len(b_enc_str)):
            enc_bin += b_enc_str[i]

        iv = binascii.b2a_hex(iv_bin)
        key = binascii.b2a_hex(key_bin)

        tempdir = tempfile.mkdtemp(prefix='maum-py-config', suffix='.d',
                                   dir='/tmp')
        in_file = os.path.join(tempdir, 'in.txt')
        with open(in_file, "wb") as ii:
            ii.write(enc_bin)
            ii.close()
        out_file = os.path.join(tempdir, 'out.bin')

        # SOMEDAY, PIPE 기반 openssl 호출도 성공했으면 좋겠네요.
        cmd = ['openssl', 'enc', '-aes-128-cbc',
               '-d',
               '-K', key,
               '-iv', iv,
               '-in', in_file,
               '-out', out_file]
        pipe = subprocess.Popen(cmd, close_fds=True, stdin=None, stdout=None)
        pipe.wait()

        with open(out_file, "rb") as oo:
            dec_bin = oo.read()
            oo.close()

        os.remove(in_file)
        os.remove(out_file)
        os.rmdir(tempdir)

        return dec_bin

    # private
    def parse_config(self, conf_file):
        """
        config 파일에서 설정의 내용을 읽어들인다.
        :param conf_file: 읽어들일 config 파일
        :return:
        """
        with open(conf_file) as myfile:
            while True:
                line = myfile.readline()
                if not line:
                    break
                line = line.strip()
                if len(line) == 0 or line[0] == '#':
                    continue
                name, var = line.partition("=")[::2]
                var = var.strip().replace('${MAUM_ROOT}', self.maum_root)
                n = name.strip()
                # 변수명이 []로 끝나면 배열로 처리한다.
                # 각 프로그램에서 알아서 " ", "," 로 분리해서 처리해야 한다.
                if n.endswith('[]') and n in self.config_map:
                    self.config_map[n] = self.config_map[n] + ', ' + var
                else:
                    self.config_map[n] = var

    def get(self, field):
        if field in self.config_map:
            return self.config_map[field]
        else:
            return ''

    def decrypt(self, field):
        enc = self.get(field)
        if enc:
            return Config.decrypt_text(enc)
        else:
            return enc
