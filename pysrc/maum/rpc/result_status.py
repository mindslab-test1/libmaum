# -*- coding:utf-8 -*-
import grpc
from maum.rpc import status_pb2
from maum.rpc import error_details_pb2
from google.protobuf import any_pb2


class ResultStatus():
    module = None
    process = None

    @staticmethod
    def setModule(module):
        ResultStatus.module = module

    @staticmethod
    def setProcess(process):
        ResultStatus.process = process

    def __init__(self, *args):
        """
        ResultStatus 생성자
        parameter로 1개 받을 때에는 Client에서 호출
        parameter로 3개 받을 때에는 Server에서 에러 생성시 호출
         0 : code
         1 : error_index
         2 : message
        :param args: parameter
        """
        # client에선 response에 대한 데이터를 생성자에 넣는다
        if len(args) == 1:
            self.status_list = status_pb2.ResultStatusList()
            self.status_list.ParseFromString(args[0])
        # server
        elif len(args) == 3:
            self.init(args)
        else:
            print("Invalid argument")
        pass

    def init(self, args):
        """
        maum.rpc.ResultStatus 객체 초기화

        :param args : [0] : code, [1] : error_index, [2] : message
        :return:
        """
        self.status = status_pb2.ResultStatus()
        self.status.module = self.module
        self.status.process = self.process
        self.status.code = args[0]
        self.status.error_index = args[1]
        self.status.message = args[2]

    def addDetail(self, message):
        """
        maum.rpc.ResultStatus detail 추가, ResultStatusList에 ResultStatus 추가
        :param message: Error_Detail의 Message 객체
        """

        any = any_pb2.Any()
        any.Pack(message)
        self.status.details.extend([any])

        # ResultStatusList create
        self.status_list = status_pb2.ResultStatusList()
        self.status_list.list.extend([self.status])

    def addStackStatus(self, newStatus):
        """
        ResultStatus의 status_list 객체에 이전 Stack의 ResultStatusList를 추가
        :param newStatus: Class ResultStatus 객체
        """
        if self.status_list == None:
            self.status_list = status_pb2.ResultStatusList()

        # 이전 Stack Status를 ResultStatusList에 담음
        for status in newStatus.status_list.list:
            self.status_list.list.extend(status)

    def toMeta(self, context):
        """
        Meta에 ResultStatusList를 담는 함수
        :param context: grpc Context
        """
        result = self.status_list.SerializePartialToString()

        _SERVER_TRAILING_METADATA = (
            ('grpc-status-details-bin', result),
            ('grpc-status-details-key', 'maum.ai status result')
        )
        context.set_trailing_metadata(_SERVER_TRAILING_METADATA)

    def getStatusCode(self):
        """
        ResultStatus.ExCode를 grpc.Status Code로 변환하는 함수
        :return: error code
        """
        error_number = (self.status.code / 10) * 10
        return error_number

    def makeErrorCode(self):
        """
        ResultStatus를 받으면 13자리 error code를 리턴한다.
        {module}{proc}{hyphen}{code}{error_index}
        2 + 2 + 1 + 3 + 5 : 13자리
        :return: 13자리 code
        """
        if (len(self.status_list.list) == 0):
            print("Not existed ResultStats")
            return None
        else:
            result = self.status_list.list[0]
            code = str(result.module).zfill(2) + \
                   str(result.process).zfill(2) + "-" + \
                   str(result.code).zfill(3) + \
                   str(result.error_index).zfill(5)
            print('code : {}'.format(code))
            return code

    def printAllStack(self):
        """
        ResultStatus의 모든 Stack 정보 출력
        """
        for i in self.status_list.list:
            print('Module : {}'.format(i.module))
            print('Process: {}'.format(i.process))
            print('Error Code: {}'.format(i.code))
            print('Error Index: {}'.format(i.error_index))
            print('Message: {}'.format(i.message))
            print('Details')

            for j in i.details:
                if j.Is(error_details_pb2.DebugInfoFile.DESCRIPTOR):
                    debug = error_details_pb2.DebugInfoFile()
                j.Unpack(debug)

                if j.Is(error_details_pb2.QuotaFailure.DESCRIPTOR):
                    debug = error_details_pb2.QuotaFailure()
                j.Unpack(debug)

                if j.Is(error_details_pb2.PreconditionFailure.DESCRIPTOR):
                    debug = error_details_pb2.PreconditionFailure()
                j.Unpack(debug)

                if j.Is(error_details_pb2.BadRequest.DESCRIPTOR):
                    debug = error_details_pb2.BadRequest()
                j.Unpack(debug)

                if j.Is(error_details_pb2.RequestInfo.DESCRIPTOR):
                    debug = error_details_pb2.RequestInfo()
                j.Unpack(debug)

                if j.Is(error_details_pb2.ResourceInfo.DESCRIPTOR):
                    debug = error_details_pb2.ResourceInfo()
                j.Unpack(debug)

                if j.Is(error_details_pb2.Help.DESCRIPTOR):
                    debug = error_details_pb2.Help()
                j.Unpack(debug)

                if j.Is(error_details_pb2.LocalizedMessage.DESCRIPTOR):
                    debug = error_details_pb2.LocalizedMessage()
                j.Unpack(debug)

                print(debug)
