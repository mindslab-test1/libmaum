# -*- coding:utf-8 -*-

from __future__ import print_function

import argparse
import os
import sys

from .config_generator import ConfigGenerator
from .config_generator import export_sv_systemd
from .recipe import Recipe

class Command(object):
    """
    명령을 수행하는 단위
    """
    # states
    _BUILD_TARGET = ('dev', 'test', 'staging', 'prod')

    # constants
    _DEPENDENCY_RECIPE_EXT = '.dependency.recipe.json'
    _CONFIG_RECIPE_EXT = '.config.recipe.json'
    _RECIPE_EXT = '.recipe.json'
    _RECIPE = 'recipe'
    _RECIPE_D = 'recipe.d'
    _DEPENDENCY = 'dependency'
    _CONFIG = 'config'
    _MAUM_ROOT = 'MAUM_ROOT'

    # variables
    recipe_d = ''
    product = {}
    target = ''
    commands = None
    maum_root = ''
    verbose = False
    use_default = False


    def __init__(self, products, target, commands, use_default, verbose):
        """

        :param products: 제품 all
        :param target:
        :param commands:
        :return:
        """
        if self._MAUM_ROOT not in os.environ:
            raise self._MAUM_ROOT, 'is not defined.'
        self.maum_root = os.environ['MAUM_ROOT']
        self.recipe_d = os.path.join(self.maum_root, 'etc', self._RECIPE_D)
        self.verbose = verbose
        self.use_default = use_default
        # 현재는 하나의 명령어만 수행하기 때문에 `commands`가 배열이지 않지만
        #  나중에는 여러 개를 동시에 수행하게 되므로 순서도 중요한다.
        self.commands = commands

        if products == 'all':
            self.load_products()
        else:
            self.load_product(products)

        if target in self._BUILD_TARGET:
            self.target = target
        else:
            raise RuntimeError('Invalid environment name' + target)

    def load_products(self):
        """
        create product recipe
        :return: load recipes for dependencies and config
        """
        if os.path.exists(self.recipe_d) and os.path.isdir(self.recipe_d):
            r_files = [x for x in os.listdir(self.recipe_d)
                       if x.endswith(self._RECIPE_EXT)]
            for f in r_files:
                names = f.split('.')
                if len(names) != 4:
                    print('skip invalid file ', f)
                    continue

                assert names[2] == self._RECIPE
                if names[0] not in self.product:
                    p = Recipe(names[0])
                    self.product[names[0]] = p
                else:
                    p = self.product[names[0]]
                if names[1] == self._DEPENDENCY:
                    p.load_dependencies(os.path.join(self.recipe_d, f))
                    if p.dependencies is None:
                        raise RuntimeError('invalid dependency recipe')
                if names[1] == self._CONFIG:
                    p.load_config(os.path.join(self.recipe_d, f))
                    if p.config is None:
                        raise RuntimeError('invalid config recipe')

        print('Product recipes are', self.product.keys(), file=sys.stderr)

    def load_product(self, product):
        if os.path.exists(self.recipe_d) and os.path.isdir(self.recipe_d):
            p = Recipe(product)
            dep_recipe = os.path.join(self.recipe_d,
                                      product + self._DEPENDENCY_RECIPE_EXT)
            p.load_dependencies(dep_recipe)
            if p.dependencies is None:
                raise RuntimeError('invalid dependency recipe')

            conf_recipe = os.path.join(self.recipe_d,
                                       product + self._CONFIG_RECIPE_EXT)
            p.load_config(conf_recipe)
            if p.is_valid():
                self.product[p.product] = p
            else:
                print('Cannot load product recipe.', product)
                raise RuntimeError('invalid config recipe')

    def get_func(self, cmd):
        return {
            'help': self.__do_help,
            'deploy': self.__do_deploy,
            'prepare': self.__do_prepare,
            'config': self.__do_config,
            'status': self.__do_status,
            'clear': self.__do_clear,
            'recipe': self.__do_recipe,
            'check': self.__do_check,
            'list': self.__do_list
        }.get(cmd, self.__do_invalid_cmd)


    def go(self):
        for p in self.product:
            for cmd in self.commands:
                func = self.get_func(cmd)
                func(self.product[p])

    def __do_invalid_cmd(self, product):
        print('WoW, how do you hack it?')

    def help(self):
        self.__do_help(None)

    def __do_help(self, product):
        help = """
setup

options
    -p : specify a product / mlt, m2u, voc, default all
    -e : specify environment / prod, dev, test, staging
    -v : run verbose mode, on config, prepare
    -d : use default value, no input required at config.

commands
    deploy  : prepare / config
    prepare : prepare all dependencies
    config  : generate config files with interactive mode.
    status  : list a product, its recipes(dependencies, config, svc)
    clear   : clear all generated configs
    recipe  : load recipe configuration and show how it works.
    check   : check dependencies and config, find some errors.
    upgrade : upgrade package with new file
        -f  : package tar file
    export  : export supervisor service
    list    : list all recipe files.
    help    : print help messages

examples
    setup   : deploy all products, prepare and config
    setup -p m2u -e prod status
    setup -p mlt -e dev prepare
    setup -p voc -e prod upgrade -f xx.tar.gz (todo)
    setup -p m2u list
"""
        print(help)

    def __do_deploy(self, product):
        self.__do_prepare(product)
        self.__do_config(product)

    def __do_prepare(self, product):
        print('prepare : COMING SOON')

    def __do_config(self, product):
        cg = ConfigGenerator(product.config)
        cg.set_environ(self.target)
        cg.set_verbose(self.verbose)
        cg.input_variables(self.use_default)
        cg.generate()

    def __do_status(self, product):
        cg = ConfigGenerator(product.config)
        cg.set_environ(self.target)
        cg.status()

    def __do_clear(self, product):
        cg = ConfigGenerator(product.config)
        cg.set_environ(self.target)
        cg.clear()

    def __do_recipe(self, product):
        cg = ConfigGenerator(product.config)
        cg.set_environ(self.target)
        cg.recipe()

    def __do_check(self, product):
        cg = ConfigGenerator(product.config)
        cg.set_environ(self.target)
        cg.set_verbose(self.verbose)
        cg.check()

    def __do_list(self, product):
        cg = ConfigGenerator(product.config)
        cg.set_environ(self.target)
        cg.set_verbose(self.verbose)
        cg.list()


def main():
    """

    :return: system exit process
    """

    """
    setup

    options
      --product -p : specify a product / mlt, m2u, biz, default all
      --target  -t : specify target / prod, dev, test, staging
      --verbose -v : run verbose mode, on config, prepare

    commands
      deploy       : prepare / config
      prepare      : prepare all dependencies
      config       : generate config files with interactive mode.
      status       : list a product, its recipes(dependencies, config, svc)
      clear        : clear all generated configs
      recipe       : load recipe configuration and show how it works.
      check        : check dependencies and config, find some errors.
      upgrade      : upgrade package with new file
        -f         : package tar file
      export       : export supervisor service
      list         : list all recipe files.
      help         : print help messages3

    examples
      setup        : deploy all products, prepare and config
      setup -p m2u -t prod status
      setup -p mlt -t dev prepare
      setup -p voc -t prod upgrade -f xx.tar.gz
      setup export
    """
    parser = argparse.ArgumentParser(
        description='maum.ai common setup tool')
    parser.add_argument('-p',
                        nargs='?',
                        metavar="PROD",
                        dest='product',
                        type=str,
                        default='all',
                        help='Product name')
    parser.add_argument('-t',
                        nargs='?',
                        dest='target',
                        default='prod',
                        help='Build target (dev, test, staging, prod)')
    parser.add_argument('-d',
                        required=False,
                        dest='use_default',
                        action='store_true',
                        default=False,
                        help='Use default value on generation.')
    parser.add_argument('-v',
                        required=False,
                        dest='verbose',
                        action='store_true',
                        default=False,
                        help='Verbose output at running.')
    parser.add_argument('command',
                        nargs=1,
                        help='Command to process',
                        default='deploy',
                        choices=['help',
                                 'deploy',
                                 'prepare',
                                 'config',
                                 'status',
                                 'clear',
                                 'recipe',
                                 'check',
                                 'export',
                                 'list',
                                 'help',
                                 'upgrade'])

    # SUB COMMAND
    # upgrade

    args = parser.parse_args()
    # print(args)
    # print(args.product,
    #   args.target,
    #   args.command,
    #   args.verbose,
    #   args.use_default)

    try:
        if len(args.command) == 1 and args.command[0] == 'export':
            c = Command('libmaum', 'prod', ['config'], True, False)
            c.go()
            export_sv_systemd()
        elif args.command[0] == 'help':
            cmd = Command('libmaum', 'prod', ['config'], True, False)
            cmd.help()
        else:
            cmd = Command(args.product,
                        args.target,
                        args.command,
                        args.use_default,
                        args.verbose)
            cmd.go()
        return 0
    except RuntimeError as re:
        print(str(re))
        return 1

if __name__ == "__main__":
    main()
