# -*- coding:utf-8 -*-

from maum.common.setup import recipe_pb2
from google.protobuf import json_format


class Recipe(object):
    product = None
    dependencies = None
    config = None

    def __init__(self, product):
        self.product = product
        pass

    @staticmethod
    def _read_recipe(filename):
        f = open(filename, 'r')
        data = f.read()
        f.close()
        return data

    def load_dependencies(self, file):
        try:
            data = Recipe._read_recipe(file)
            dep_msg = json_format.Parse(data,
                                        recipe_pb2.Dependencies(),
                                        ignore_unknown_fields=False)
            self.dependencies = dep_msg
        except json_format.ParseError as ex:
            print '>>>', str(ex)
        except IOError as ex:
            print '>>>', str(ex)

    def load_config(self, file):
        try:
            data = Recipe._read_recipe(file)
            dep_msg = json_format.Parse(data,
                                        recipe_pb2.ConfigRecipe(),
                                        ignore_unknown_fields=False)

            dep_msg.variables.sort(key=lambda v: v.variable)
            self.config = dep_msg
        except json_format.ParseError as ex:
            print '>>>', str(ex)
        except IOError as ex:
            print '>>>', str(ex)

    def has_config(self):
        return self.config is not None

    def has_dependencies(self):
        return self.dependencies is not None

    def is_valid(self):
        return self.config is not None or self.dependencies is not None
