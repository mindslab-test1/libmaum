# -*- coding:utf-8 -*-
from __future__ import print_function

import hashlib
import os
import sys
import pwd
import shutil
from datetime import datetime
from string import Template
import binascii
import base64
from cStringIO import StringIO

from google.protobuf import json_format

from common.util import get_primary_ip
from maum.common.setup import recipe_pb2
import subprocess
import tempfile

__var_syntax = r"\${([A-Za-z0-9_]+)\}"

_MAUM_ROOT = 'MAUM_ROOT'
_NONE = 'NONE'
_HOME = 'HOME'
_HOSTIP = 'HOSTIP'
_SYSUSER = 'SYSUSER'
_ENVIRON = 'ENVIRON'
_LD_LIBRARY_PATH = 'LD_LIBRARY_PATH'


class BackColors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    OKCYAN = '\033[96m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

    def __init__(self):
        pass

#
# OPENSSL commands
#
# openssl rand -hex 16
# openssl \
#    enc \
#    -nopad \
#    -aes-128-cbc \
#    -e \
#    -K  00112233445566778899AABBCCDDEEFF \
#    -iv 00112233445566778899AABBCCDDEEFF \
#    -in ee.txt \
#    -out rr.txt
#

def _gen_random(len):
    cmd = ['openssl', 'rand', '-hex', str(len)]
    pipe = subprocess.Popen(cmd, close_fds=True, stdout=subprocess.PIPE)
    rand_num = pipe.stdout.readline()
    rand_num = rand_num.rstrip()
    pipe.wait()
    return rand_num


def encrypt_text(clear):
    iv = _gen_random(16)
    # print("IV[", iv, "]")
    key = _gen_random(16)
    # print("KEY[", key, "]")

    tempdir = tempfile.mkdtemp(prefix='maumai-setup', suffix='.d', dir='/tmp')
    # print('TEMPDIR [', tempdir, ']')
    in_file = os.path.join(tempdir, 'in.txt')
    with open(in_file, "wb") as ii:
        ii.write(clear)
        ii.close()
    out_file = os.path.join(tempdir, 'out.bin')

    cmd = ['openssl', 'enc', '-aes-128-cbc',
           '-e',
           '-K', key,
           '-iv', iv,
           '-in', in_file,
           '-out', out_file]
    pipe = subprocess.Popen(cmd, close_fds=True, stdin=subprocess.PIPE, stdout=None)
    pipe.wait()

    with open(out_file, "rb") as oo:
        enc_bin = oo.read()
        oo.close()

    # print("enc", len(enc_bin), "hex", binascii.b2a_hex(enc_bin))

    os.remove(in_file)
    os.remove(out_file)
    os.rmdir(tempdir)
    # make CIPHER BINARY
    # iv  key cipher
    iv_bin = binascii.unhexlify(iv)
    key_bin = binascii.unhexlify(key)

    out_bin = StringIO()

    for i in range(0, 16):
        out_bin.write(enc_bin[i])
        out_bin.write(iv_bin[i])
        out_bin.write(key_bin[i])
    for i in range(16, len(enc_bin)):
        out_bin.write(enc_bin[i])

    enc_str = out_bin.getvalue()
    # print("ENC_STR [", enc_str, "]", len(enc_str))
    # print("ENC_STR [", enc_str, "]")

    b_enc_str = base64.b64encode(enc_str)
    return b_enc_str.rstrip()


def decrypt_text(cipher):
    b_enc_str = base64.decodestring(cipher)
    # print('LEN = ', len(b_enc_str))
    if (b_enc_str  < (16 * 3)):
        return False
    iv_bin = ''
    key_bin = ''
    enc_bin = ''

    for i in range(0, (16 * 3)):
        mod = i % 3
        if (mod == 0):
            enc_bin += b_enc_str[i]
        elif (mod == 1):
            iv_bin += b_enc_str[i]
        else:
            key_bin += b_enc_str[i]
    for i in range((16 * 3), len(b_enc_str)):
        enc_bin += b_enc_str[i]

    iv = binascii.b2a_hex(iv_bin)
    key = binascii.b2a_hex(key_bin)

    # print("IV[", iv, "]")
    # print("KEY[", key, "]")
    # print("enc", len(enc_bin), "hex", binascii.b2a_hex(enc_bin))

    tempdir = tempfile.mkdtemp(prefix='maumai-setup', suffix='.d', dir='/tmp')
    # print('TEMPDIR [', tempdir, ']')
    in_file = os.path.join(tempdir, 'in.txt')
    with open(in_file, "wb") as ii:
        ii.write(enc_bin)
        ii.close()
    out_file = os.path.join(tempdir, 'out.bin')

    # SOMEDAY, PIPE 기반 openssl 호출도 성공했으면 좋겠네요.
    cmd = ['openssl', 'enc', '-aes-128-cbc',
           '-d',
           '-K', key,
           '-iv', iv,
           '-in', in_file,
           '-out', out_file]
    pipe = subprocess.Popen(cmd, close_fds=True, stdin=None, stdout=None)
    # pipe.stdin.write(enc_bin)
    # pipe.stdin.close()
    # b_dec = pipe.stdout.readline()
    # b_dec = b_dec.rstrip()
    pipe.wait()

    # dec_bin = base64.b64encode(b_dec)
    with open(out_file, "rb") as oo:
        dec_bin = oo.read()
        oo.close()

    os.remove(in_file)
    os.remove(out_file)
    os.rmdir(tempdir)

    # print("dec", len(dec_bin), "txt", dec_bin)

    # print("============================================")
    # print("============================================")
    # print("============================================")


class ConfigGenerator(object):
    variables = {}
    default_values = {}
    store = recipe_pb2.VariableStore()
    verbose = False
    environ = []
    store_file = None
    old_store = None
    enctyped = {}

    def load_stored_value(self):
        s_d = os.path.join(self.variables[_MAUM_ROOT], 'etc', 'setup.d')
        if not os.path.exists(s_d):
            os.makedirs(s_d)
        s_f = os.path.join(s_d, self.config_recipe.name + '.store.json')
        self.store_file = s_f

        # LOAD JSON STORE
        if os.path.exists(self.store_file):
            f = open(self.store_file)
            data = f.read()
            f.close()
            lstore = json_format.Parse(data, recipe_pb2.VariableStore(),
                                       ignore_unknown_fields=False)
            # OVERWRITE
            for k in lstore.variables:
                self.variables[str(k)] = str(lstore.variables[k])
            self.old_store = lstore

    def __init__(self, config_recipe):
        self.config_recipe = config_recipe
        if _MAUM_ROOT in os.environ:
            self.variables[_MAUM_ROOT] = os.environ[_MAUM_ROOT]
        else:
            self.variables[_MAUM_ROOT] = _NONE
        self.variables[_SYSUSER] = pwd.getpwuid(os.getuid()).pw_name
        self.variables[_HOME] = pwd.getpwuid(os.getuid()).pw_dir
        self.variables[_HOSTIP] = get_primary_ip()

        for v in self.config_recipe.variables:
            # default value에 ${MAUM_ROOT} 등의 변수가 들어 있을 때에는
            #  위의 변수에서 채워준다.
            str_temp = Template(str(v.default_value))
            val = str_temp.substitute(
                MAUM_ROOT=self.variables[_MAUM_ROOT],
                SYSUSER=self.variables[_SYSUSER],
                HOME=self.variables[_HOME],
                HOSTIP=self.variables[_HOSTIP])

            if v.needs_encryption:
                val = encrypt_text(val)
                self.enctyped[str(v.variable)] = True

            self.variables[str(v.variable)] = val
            self.default_values[str(v.variable)] = val
        # load stored value
        self.load_stored_value()

    def set_verbose(self, verbose):
        self.verbose = verbose

    def set_environ(self, environ):
        self.environ = [environ]

        if environ == 'dev':
            self.environ.append('development')
            self.variables[_ENVIRON] = 'development'
        elif environ == 'prod':
            self.environ.append('production')
            self.variables[_ENVIRON] = 'production'
        else:
            self.variables[_ENVIRON] = environ

    def find_all_variables_from_in(self):
        """
        모든 입력 파일들에서 변수들을 모두 가져와 변수별로 정의된 위치을 모두 나열한다.
        위치는 파일이름과 라인수, 컬럼위치이다. 이때,
        :return: { 'VAR': [ {file: "name.conf", line: 222, column: 3232}, ... ]
        """
        pass

    def print_variables(self):
        print('\nVariables for', self.config_recipe.name)
        for v in self.config_recipe.variables:
            is_default = False
            if str(v.variable) in self.variables:
                set_val = self.variables[str(v.variable)]
            else:
                set_val = None
            if set_val is not None:
                if set_val == self.default_values[str(v.variable)]:
                    is_default = True
            def_val = '[default]' if is_default else '<<modified>>'
            def_val = '<<encrypted>>' if v.needs_encryption else def_val
            if not is_default:
                print('@@@@@@',
                      BackColors.OKGREEN + v.variable,
                      '=', set_val + BackColors.ENDC, def_val)
            else:
                print('    >>', v.variable, '=', set_val, def_val)

    def store_variables(self):
        for k, v in self.variables.iteritems():
            self.store.variables[k] = v

    def input_variables(self, use_default):
        if use_default:
            for v in self.config_recipe.variables:
                vv = str(v.variable)
                self.variables[vv] = self.default_values[vv]
        self.print_variables()
        if use_default:
            return
        done = False
        while not done:
            try:
                kv = raw_input('Input variable (KEY=VALUE),'
                               ' done | Ctrl+D if done,'
                               ' [ENTER] to refresh\n> ')
                if len(kv) == 0:
                    print
                    self.print_variables()
                    continue

                kvs = [x.strip() for x in kv.split('=')]
                if len(kvs) is not 2:
                    if kvs[0] == 'done':
                        print()
                        done = True
                        continue
                    else:
                        print("  : Invalid format")
                        continue
                if kvs[0] in self.variables:
                    if kvs[0] in self.enctyped:
                        kvs[1] = encrypt_text(kvs[1])
                    self.variables[kvs[0]] = kvs[1]
                    print('    ', '{0}{1}={2}{3} captured'.format(
                        BackColors.HEADER, kvs[0], kvs[1], BackColors.ENDC))
                else:
                    print('    : Unknown variable name')
            except EOFError:
                done = True
                print()
                # self.print_variables()

    def find_in_file(self, template, conf_dir):
        f = os.path.join(conf_dir, template)
        if os.path.exists(f):
            return f
        for e in self.environ:
            fname = os.path.join(conf_dir, template + '.' + e)
            if self.verbose:
                print('sannning', fname)
            if os.path.exists(fname):
                return fname
        return None

    def get_target_file(self, in_file, target, conf_dir):
        if len(target) > 0:
            ret = os.path.join(conf_dir, target)
        else:
            names = in_file.split('.in')
            ret = names[0]
        return ret, ret[len(self.variables[_MAUM_ROOT]) + 1:]

    def generate(self):
        self.store_variables()
        error_count = 0
        print('++', 'generating config files for', self.config_recipe.name)
        for cf in self.config_recipe.config_files:
            conf_dir = cf.dir
            if not cf.dir.startswith('/'):
                conf_dir = os.path.join(self.variables[_MAUM_ROOT], cf.dir)
            print('--', 'dir:', cf.dir)

            for map_f in cf.mappings:
                in_file = self.find_in_file(map_f.template, conf_dir)
                if in_file is None:
                    print(':: cannot find template {0} at {1} for {2}'.format(
                        map_f.template, conf_dir, self.config_recipe.name))
                    error_count = error_count + 1
                    continue
                out_file, short_out = self.get_target_file(in_file,
                                                           map_f.target,
                                                           conf_dir)

                inf = open(in_file)
                src = Template(inf.read())
                inf.close()

                var = self.variables.copy()
                # 원래 MAUM_ROOT, HOME 을 그대로 재사용하기 위한 용도
                if map_f.use_reserved is True:
                    var[_MAUM_ROOT] = '${MAUM_ROOT}'
                    # var[_LD_LIBRARY_PATH] = '${LD_LIBRARY_PATH}'
                    var[_HOME] = '${HOME}'

                try:
                    dest = src.substitute(var)

                    outf = open(out_file, 'w')
                    outf.write(dest)
                    outf.close()
                    print('   ', short_out, "generated.")
                    digest = hashlib.sha256(dest).hexdigest()
                    self.store.file_hashes[out_file] = digest
                except KeyError as e:
                    print('    {0}{1} KeyError {2} in {3}{4}'.format(
                        BackColors.FAIL, short_out, str(e), in_file,
                        BackColors.ENDC))
                except ValueError as e:
                    print('    {0}{1} ValueError {2} in {3}{4}'.format(
                        BackColors.FAIL, short_out, str(e), in_file,
                        BackColors.ENDC))
        #
        # STORE json
        #
        self.store.saved_at.GetCurrentTime()
        f = open(self.store_file, 'wb')
        json = json_format.MessageToJson(self.store,
                                         including_default_value_fields=True)
        f.write(json)
        f.close()
        print()
        print('++ Configuration is stored at', self.store_file)

    def check_in_out(self, in_file, out_file):
        ret = []
        out_stat = None
        if not os.path.exists(out_file):
            ret.append('not exist')
        else:
            in_stat = os.lstat(in_file)
            out_stat = os.lstat(out_file)
            if in_stat.st_mtime > out_stat.st_mtime:
                ret.append('old version')
            # check modified
            outf = open(out_file, 'r')
            data = outf.read()
            digest = hashlib.sha256(data).hexdigest()
            outf.close()
            if self.old_store.file_hashes[out_file] != digest:
                ret.append('manually modified')
        return ret, out_stat

    def check(self):
        print('++', 'check config for', self.config_recipe.name)
        for cf in self.config_recipe.config_files:
            conf_dir = cf.dir
            if not cf.dir.startswith('/'):
                conf_dir = os.path.join(self.variables[_MAUM_ROOT], cf.dir)
            print('--', 'checking at', cf.dir)
            error_count = 0

            for map_f in cf.mappings:
                in_file = self.find_in_file(map_f.template, conf_dir)
                if in_file is None:
                    print('    cannot find template {0} at {1} for {2}'.format(
                        map_f.template, conf_dir, self.config_recipe.name))
                    error_count = error_count + 1
                    continue
                out_file, short_out = self.get_target_file(in_file,
                                                           map_f.target,
                                                           conf_dir)
                check_ret, _ = self.check_in_out(in_file, out_file)
                if len(check_ret) > 0:
                    print('   {0}{1}: {2}{3}'.format(
                        BackColors.FAIL, short_out, ' ,'.join(check_ret),
                        BackColors.ENDC))
                error_count += len(check_ret)
            if error_count is 0:
                print('   fine!')

    def status(self):
        print('\n\n++', 'Config status for', self.config_recipe.name)
        for cf in self.config_recipe.config_files:
            conf_dir = cf.dir
            if not cf.dir.startswith('/'):
                conf_dir = os.path.join(self.variables[_MAUM_ROOT], cf.dir)
            print('--', 'dir:', cf.dir)
            error_count = 0

            for map_f in cf.mappings:
                print('  [', map_f.template, ']')
                in_file = self.find_in_file(map_f.template, conf_dir)
                if in_file is None:
                    print('    cannot find template {0} at {1} for {2}'.format(
                        map_f.template, conf_dir, self.config_recipe.name))
                    error_count = error_count + 1
                    continue
                out_file, short_out = self.get_target_file(in_file,
                                                           map_f.target,
                                                           conf_dir)
                print('    target:', short_out)
                check_ret, out_stat = self.check_in_out(in_file, out_file)
                if len(check_ret) > 0:
                    print('    status:', ' ,'.join(check_ret))
                if out_stat is not None:
                    print('    created at: {0}'.format(
                        datetime.fromtimestamp(out_stat.st_mtime).strftime(
                            '%Y-%m-%d %H:%M:%S')))
        self.print_variables()

    def recipe(self):
        print('++', 'Recipe dump for', self.config_recipe.name)
        for cf in self.config_recipe.config_files:
            conf_dir = cf.dir
            if not cf.dir.startswith('/'):
                conf_dir = os.path.join(self.variables[_MAUM_ROOT], cf.dir)
            print('--', 'dir:', cf.dir)

            for map_f in cf.mappings:
                print('  [', map_f.template, ']')
                if len(map_f.target) == 0:
                    in_file = self.find_in_file(map_f.template, conf_dir)
                    out_file, short_out = self.get_target_file(in_file,
                                                               map_f.target,
                                                               conf_dir)
                    print('    target:', short_out)
                else:
                    print('    target:', map_f.target)
                print('    use reserved:', map_f.use_reserved)
        self.print_variables()

    def clear(self):
        print('++', 'Clear generated config for', self.config_recipe.name)
        for cf in self.config_recipe.config_files:
            conf_dir = cf.dir
            if not cf.dir.startswith('/'):
                conf_dir = os.path.join(self.variables[_MAUM_ROOT], cf.dir)
            print('--', 'dir:', cf.dir)
            # error_count = 0

            for map_f in cf.mappings:
                print('  [', map_f.template, ']')
                in_file = self.find_in_file(map_f.template, conf_dir)
                out_file, short_out = self.get_target_file(in_file,
                                                           map_f.target,
                                                           conf_dir)
                if os.path.exists(out_file):
                    os.unlink(out_file)
                    print('  {0} deleted.'.format(short_out))

    def list(self):
        for cf in self.config_recipe.config_files:
            conf_dir = cf.dir
            if not cf.dir.startswith('/'):
                conf_dir = os.path.join(self.variables[_MAUM_ROOT], cf.dir)
            print('--', 'dir:', cf.dir, file=sys.stderr)
            for map_f in cf.mappings:
                in_file = self.find_in_file(map_f.template, conf_dir)
                if in_file is not None:
                    print(in_file[len(self.variables[_MAUM_ROOT]) + 1:])


def export_sv_systemd():
    print('\n\n++', 'Export supervisor unit to systemd')
    filename = 'maumai-sv.service'
    in_file = os.path.join(os.environ[_MAUM_ROOT],
                           'etc', 'supervisor', 'export',
                           filename)
    out_dir = os.path.join(os.environ[_HOME],
                           '.config', 'systemd', 'user')
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    shutil.copy2(in_file, out_dir)
    print('\n>>',
          BackColors.WARNING + out_dir + BackColors.ENDC,
          'has', BackColors.OKGREEN + filename + BackColors.ENDC)

    cmd = os.path.join(os.environ[_MAUM_ROOT],
                       'etc', 'supervisor', 'export',
                       'export-sv.sh')
    print('\n>>Running', cmd)
    os.system(cmd)


if __name__ == "__main__":
    x = encrypt_text("Encrypt and decrypt buffers")
    print('-----------', x)
    decrypt_text(x)
    y = encrypt_text("The first decision is the AES encryption mode")
    print('-----------', y)
    decrypt_text(y)
    z = encrypt_text("All examples are available on Github")
    print('-----------', z)
    decrypt_text(z)
    z2 = encrypt_text("MAUM.ai")
    print('-----------', z2)
    decrypt_text(z)
