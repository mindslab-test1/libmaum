#! /usr/bin/python
from __future__ import print_function
import sys
import os
import supervisor.supervisord
if __name__ == '__main__':
    if os.environ.has_key('MAUM_ROOT') is False:
        print('>>> MAUM_ROOT is not defined')
        sys.exit(1)
    m_root = os.environ.get('MAUM_ROOT')
    conf = os.path.join(m_root, 'etc', 'supervisor', 'supervisord.conf')
    args = ['-c', conf, '-u', str(os.getuid())]
    args.extend(sys.argv[1:])

    sys.exit(supervisor.supervisord.main(args))
