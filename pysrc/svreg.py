#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import socket
import grpc
import sys
from common.config import Config
import time
import traceback

from maum.supervisor import monitor_pb2_grpc as monitor_grpc
from maum.supervisor import monitor_pb2 as monitor


class SupervisorRegister(object):
    """
    `SupervisorMonitor`에 실행 여부를 알려주는 클래스
    """

    ip = ''
    port = 9799
    hostname = 'Unknown'
    stub = None
    conf = Config()
    admin_remotes = []

    @staticmethod
    def get_local_ip():
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("8.8.8.8", 53))
        # print(s.getsockname()[0])
        ret = s.getsockname()[0]
        s.close()
        return ret

    def __init__(self, ip, port, hostname):
        self.ip = ip
        self.port = port
        self.hostname = hostname

        if self.ip is None:
            self.ip = SupervisorRegister.get_local_ip()
        if self.hostname is None:
            self.hostname = socket.gethostname()

        conf_files_var = self.conf.get('svreg.conf.files[]')
        print('config files var', conf_files_var)
        conf_files = conf_files_var.split(' ,')
        print('all config files', conf_files)
        for cf in conf_files:
            self.conf.add_config_file(cf)

        try:
            targets_var = self.conf.get('svreg.targets[]')
            targets = targets_var.split(' ,')
            for t in targets:
                self.admin_remotes.append(t)
        except Exception as e:
            print(sys.stderr, e.__doc__)
            print(sys.stderr, e.message)
            print(sys.stderr, traceback.format_exc())

    def inform(self):
        try:
            for admin_remote in self.admin_remotes:
                print('admin_remote:', admin_remote)
                stub = monitor_grpc.SupervisorMonitorStub(
                    grpc.insecure_channel(admin_remote))
                sv_grp = monitor.SupervisorServerGroup()
                sv_grp.grp_name = ''
                sv_grp.host = self.hostname
                sv_grp.ip = self.ip
                sv_grp.port = self.port
                print('notify object', sv_grp)
                res = stub.InformStartUp(sv_grp)
                print('res of InformStartUp', res)
        except grpc.RpcError as e:
            print(sys.stderr, e.__doc__)
            print(sys.stderr, e.message)
            print(sys.stderr, traceback.format_exc())
        except Exception as e:
            print(sys.stderr, e.__doc__)
            print(sys.stderr, e.message)
            print(sys.stderr, traceback.format_exc())


def run_cmd():
    conf = Config()
    conf.init('libmaum.conf')

    parser = argparse.ArgumentParser(
        description='register supervisor')
    parser.add_argument('-i',
                        nargs='?',
                        dest='ip',
                        required=False,
                        help='ip address')
    parser.add_argument('-p',
                        nargs='?',
                        dest='port',
                        type=int,
                        required=False,
                        default=9799,
                        help='port')
    parser.add_argument('-n', '--hostname',
                        nargs='?',
                        dest='hostname',
                        required=False,
                        help='hostname')

    args = parser.parse_args()

    print(args)

    runner = SupervisorRegister(
        None if args.ip is None else args.ip,
        9799 if args.port is None else args.port,
        None if args.hostname is None else args.hostname)
    try:
        runner.inform()
    except KeyboardInterrupt as e:
        print(str(e))
        exit(1)
    except Exception as ee:
        print(str(ee))

    time.sleep(60)


if __name__ == '__main__':
    run_cmd()
