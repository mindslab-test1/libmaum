#! /usr/bin/python
import sys
import supervisor.confecho

if __name__ == '__main__':
    sys.exit(supervisor.confecho.main())
