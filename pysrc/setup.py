#! /usr/bin/python
import sys
import os
import site

_MAUM_ROOT = 'MAUM_ROOT'


def get_maum_root():
    if _MAUM_ROOT not in os.environ:
        exe_path = os.path.realpath(sys.argv[0])
        bin_path = os.path.dirname(exe_path)
        if bin_path.endswith('/bin'):
            root_path = os.path.realpath(os.path.join(bin_path, '..'))
            os.environ[_MAUM_ROOT] = root_path
            return root_path
        else:
            print(_MAUM_ROOT, 'environment variable is not defined.')
            exit(1)
    else:
        return os.environ[_MAUM_ROOT]


__maum_root = get_maum_root()
__maum_lib = os.path.join(__maum_root, 'lib', 'python')


def create_maum_pth(lib_path):
    site_dir = site.getusersitepackages()
    if not os.path.isdir(site_dir):
        os.makedirs(site_dir)
    maum_pth = os.path.join(site_dir, 'maum.pth')
    f = open(maum_pth, mode='w')
    f.write(lib_path + '\n')
    f.close()
    print('maum.pth', 'added to', site_dir)


if __maum_lib not in sys.path:
    create_maum_pth(__maum_lib)
    sys.path.append(__maum_lib)

import setup.main

if __name__ == '__main__':
    sys.exit(setup.main.main())
