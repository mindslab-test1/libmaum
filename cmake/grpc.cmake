###
### CPP
###
function(PROTOBUF_GENERATE_GRPC_CPP SRCS HDRS)
  if (NOT ARGN)
    message(SEND_ERROR "Error: PROTOBUF_GENERATE_GRPC_CPP() called without any proto files")
    return()
  endif ()

  if (PROTOBUF_GENERATE_CPP_APPEND_PATH)
    # Create an include path for each file specified
    foreach (FIL ${ARGN})
      get_filename_component(ABS_FIL ${FIL} ABSOLUTE)
      get_filename_component(ABS_PATH ${ABS_FIL} PATH)
      list(FIND _protobuf_include_path ${ABS_PATH} _contains_already)
      if (${_contains_already} EQUAL -1)
        list(APPEND _protobuf_include_path -I ${ABS_PATH})
      endif ()
    endforeach ()
  else ()
    set(_protobuf_include_path -I ${CMAKE_CURRENT_SOURCE_DIR})
  endif ()

  if (DEFINED PROTOBUF_IMPORT_DIRS)
    foreach (DIR ${PROTOBUF_IMPORT_DIRS})
      get_filename_component(ABS_PATH ${DIR} ABSOLUTE)
      list(FIND _protobuf_include_path ${ABS_PATH} _contains_already)
      if (${_contains_already} EQUAL -1)
        list(APPEND _protobuf_include_path -I ${ABS_PATH})
      endif ()
    endforeach ()
  endif ()

  if (NOT DEFINED PROTOBUF_GRPC_CPP_PLUGIN)
    find_program(PROTOBUF_GRPC_CPP_PLUGIN
        NAMES grpc_cpp_plugin
        DOCS "The Goole Grpc plugin for protobuf"
        PATHS
        ${PROTOBUF_SRC_ROOT_FOLDER}/vsprojects/${_PROTOBUF_ARCH_DIR}Release
        ${PROTOBUF_SRC_ROOT_FOLDER}/vsprojects/${_PROTOBUF_ARCH_DIR}Debug
        )
  endif ()

  set(${SRCS})
  set(${HDRS})
  foreach (FIL ${ARGN})
    get_filename_component(ABS_FIL ${FIL} ABSOLUTE)
    get_filename_component(FIL_DIR ${FIL} DIRECTORY)
    get_filename_component(FIL_WE ${FIL} NAME_WE)

    if (DEFINED PROTOBUF_IMPORT_DIRS)
      foreach (DIR ${PROTOBUF_IMPORT_DIRS})
        get_filename_component(ABS_DIR ${DIR} ABSOLUTE)
        string(REPLACE ${ABS_DIR}/ "" REL_FIL ${ABS_FIL})
         #message("replace result : " ${REL_FIL} "  " ${ABS_FIL} "  with " ${DIR})
        if (NOT ${REL_FIL} STREQUAL ${ABS_FIL})
          get_filename_component(REL_DIR ${REL_FIL} DIRECTORY)
          get_filename_component(REL_WE ${REL_FIL} NAME_WE)
          #message("set local variable : " ${REL_DIR} "  " ${REL_WE} "  for " ${REL_FIL})
        endif ()
      endforeach ()
    endif ()

    list(APPEND ${SRCS} "${CMAKE_CURRENT_SOURCE_DIR}/${REL_DIR}/${REL_WE}.pb.cc")
    list(APPEND ${HDRS} "${CMAKE_CURRENT_SOURCE_DIR}/${REL_DIR}/${REL_WE}.pb.h")
    list(APPEND ${SRCS} "${CMAKE_CURRENT_SOURCE_DIR}/${REL_DIR}/${REL_WE}.grpc.pb.cc")
    list(APPEND ${HDRS} "${CMAKE_CURRENT_SOURCE_DIR}/${REL_DIR}/${REL_WE}.grpc.pb.h")

    add_custom_command(
        OUTPUT "${CMAKE_CURRENT_SOURCE_DIR}/${REL_DIR}/${REL_WE}.pb.cc"
        "${CMAKE_CURRENT_SOURCE_DIR}/${REL_DIR}/${REL_WE}.pb.h"
        "${CMAKE_CURRENT_SOURCE_DIR}/${REL_DIR}/${REL_WE}.grpc.pb.cc"
        "${CMAKE_CURRENT_SOURCE_DIR}/${REL_DIR}/${REL_WE}.grpc.pb.h"
        COMMAND ${PROTOBUF_PROTOC_EXECUTABLE}
        ARGS
            --grpc_out ${CMAKE_CURRENT_SOURCE_DIR}
            --cpp_out ${CMAKE_CURRENT_SOURCE_DIR}
            --plugin=protoc-gen-grpc=${PROTOBUF_GRPC_CPP_PLUGIN}
            ${_protobuf_include_path} ${ABS_FIL}
        DEPENDS ${ABS_FIL} ${PROTOBUF_PROTOC_EXECUTABLE}
        COMMENT "Running C++ grpc compiler on ${FIL}"
        VERBATIM)
  endforeach ()

  set_source_files_properties(${${SRCS}} ${${HDRS}} PROPERTIES GENERATED TRUE)
  set(${SRCS} ${${SRCS}} PARENT_SCOPE)
  set(${HDRS} ${${HDRS}} PARENT_SCOPE)
endfunction()


function(PROTOBUF_GENERATE_PB_CPP SRCS HDRS)
  if (NOT ARGN)
    message(SEND_ERROR "Error: PROTOBUF_GENERATE_PB_CPP() called without any proto files")
    return()
  endif ()
  if (PROTOBUF_GENERATE_CPP_APPEND_PATH)
    # Create an include path for each file specified
    foreach (FIL ${ARGN})
      get_filename_component(ABS_FIL ${FIL} ABSOLUTE)
      get_filename_component(ABS_PATH ${ABS_FIL} PATH)
      list(FIND _protobuf_include_path ${ABS_PATH} _contains_already)
      if (${_contains_already} EQUAL -1)
        list(APPEND _protobuf_include_path -I ${ABS_PATH})
      endif ()
    endforeach ()
  else ()
    set(_protobuf_include_path -I ${CMAKE_CURRENT_SOURCE_DIR})
  endif ()

  if (DEFINED PROTOBUF_IMPORT_DIRS)
    foreach (DIR ${PROTOBUF_IMPORT_DIRS})
      get_filename_component(ABS_PATH ${DIR} ABSOLUTE)
      list(FIND _protobuf_include_path ${ABS_PATH} _contains_already)
      if (${_contains_already} EQUAL -1)
        list(APPEND _protobuf_include_path -I ${ABS_PATH})
      endif ()
    endforeach ()
  endif ()

  set(${SRCS})
  set(${HDRS})
  foreach (FIL ${ARGN})
    get_filename_component(ABS_FIL ${FIL} ABSOLUTE)
    get_filename_component(FIL_DIR ${FIL} DIRECTORY)
    get_filename_component(FIL_WE ${FIL} NAME_WE)

    if (DEFINED PROTOBUF_IMPORT_DIRS)
      foreach (DIR ${PROTOBUF_IMPORT_DIRS})
        get_filename_component(ABS_DIR ${DIR} ABSOLUTE)
        string(REPLACE ${ABS_DIR}/ "" REL_FIL ${ABS_FIL})
        #message("replace result : " ${REL_FIL} "  " ${ABS_FIL} "  with " ${DIR})
        if (NOT ${REL_FIL} STREQUAL ${ABS_FIL})
          get_filename_component(REL_DIR ${REL_FIL} DIRECTORY)
          get_filename_component(REL_WE ${REL_FIL} NAME_WE)
          #message("set local variable : " ${REL_DIR} "  " ${REL_WE} "  for " ${REL_FIL})
        endif ()
      endforeach ()
    endif ()

    list(APPEND ${SRCS} "${CMAKE_CURRENT_SOURCE_DIR}/${REL_DIR}/${REL_WE}.pb.cc")
    list(APPEND ${HDRS} "${CMAKE_CURRENT_SOURCE_DIR}/${REL_DIR}/${REL_WE}.pb.h")

    add_custom_command(
        OUTPUT "${CMAKE_CURRENT_SOURCE_DIR}/${REL_DIR}/${REL_WE}.pb.cc"
        "${CMAKE_CURRENT_SOURCE_DIR}/${REL_DIR}/${REL_WE}.pb.h"
        COMMAND ${PROTOBUF_PROTOC_EXECUTABLE}
        ARGS --cpp_out ${CMAKE_CURRENT_SOURCE_DIR} ${_protobuf_include_path} ${ABS_FIL}
        DEPENDS ${ABS_FIL} ${PROTOBUF_PROTOC_EXECUTABLE}
        COMMENT "Running C++ protobuf compiler on ${FIL}"
        VERBATIM)
  endforeach ()

  set_source_files_properties(${${SRCS}} ${${HDRS}} PROPERTIES GENERATED TRUE)
  set(${SRCS} ${${SRCS}} PARENT_SCOPE)
  set(${HDRS} ${${HDRS}} PARENT_SCOPE)
endfunction()


###
### PYTHON
###
function(PROTOBUF_GENERATE_GRPC_PYTHON SRCS)
  if (NOT ARGN)
    message(SEND_ERROR "Error: PROTOBUF_GENERATE_GRPC_PYTHON() called without any proto files")
    return()
  endif ()

  if (PROTOBUF_GENERATE_CPP_APPEND_PATH)
    # Create an include path for each file specified
    foreach (FIL ${ARGN})
      get_filename_component(ABS_FIL ${FIL} ABSOLUTE)
      get_filename_component(ABS_PATH ${ABS_FIL} PATH)
      list(FIND _protobuf_include_path ${ABS_PATH} _contains_already)
      if (${_contains_already} EQUAL -1)
        list(APPEND _protobuf_include_path -I ${ABS_PATH})
      endif ()
    endforeach ()
  else ()
    set(_protobuf_include_path -I ${CMAKE_CURRENT_SOURCE_DIR})
  endif ()

  if (DEFINED PROTOBUF_IMPORT_DIRS)
    foreach (DIR ${PROTOBUF_IMPORT_DIRS})
      get_filename_component(ABS_PATH ${DIR} ABSOLUTE)
      list(FIND _protobuf_include_path ${ABS_PATH} _contains_already)
      if (${_contains_already} EQUAL -1)
        list(APPEND _protobuf_include_path -I ${ABS_PATH})
      endif ()
    endforeach ()
  endif ()

  if (NOT DEFINED PROTOBUF_GRPC_PYTHON_PLUGIN)
    find_program(PROTOBUF_GRPC_PYTHON_PLUGIN
        NAMES grpc_python_plugin
        DOCS "The Goole grpc python plugin for protobuf"
        PATHS
            ${PROTOBUF_SRC_ROOT_FOLDER}/vsprojects/${_PROTOBUF_ARCH_DIR}Release
            ${PROTOBUF_SRC_ROOT_FOLDER}/vsprojects/${_PROTOBUF_ARCH_DIR}Debug
        )
  endif ()

  set(${SRCS})
  foreach (FIL ${ARGN})
    get_filename_component(ABS_FIL ${FIL} ABSOLUTE)
    get_filename_component(FIL_DIR ${FIL} DIRECTORY)
    get_filename_component(FIL_WE ${FIL} NAME_WE)

    if (DEFINED PROTOBUF_IMPORT_DIRS)
      foreach (DIR ${PROTOBUF_IMPORT_DIRS})
        get_filename_component(ABS_DIR ${DIR} ABSOLUTE)
        string(REPLACE ${ABS_DIR}/ "" REL_FIL ${ABS_FIL})
        # message("replace result : " ${REL_FIL} "  " ${ABS_FIL} "  with " ${DIR})
        if (NOT ${REL_FIL} STREQUAL ${ABS_FIL})
          get_filename_component(REL_DIR ${REL_FIL} DIRECTORY)
          get_filename_component(REL_WE ${REL_FIL} NAME_WE)
          # message("set local variable : " ${REL_DIR} "  " ${REL_WE} "  for " ${REL_FIL})
        endif ()
      endforeach ()
    endif ()

    list(APPEND ${SRCS} "${CMAKE_CURRENT_SOURCE_DIR}/${REL_DIR}/${REL_WE}_pb2.py")
    list(APPEND ${SRCS} "${CMAKE_CURRENT_SOURCE_DIR}/${REL_DIR}/${REL_WE}_pb2_grpc.py")

    string(RANDOM LENGTH 7 TS)
    string(SHA1 TMPNAM "${TS},${CMAKE_CURRENT_SOURCE_DIR}/${REL_DIR}/${REL_WE}_pb2_grpc.py")

    add_custom_command(
        OUTPUT "${CMAKE_CURRENT_SOURCE_DIR}/${REL_DIR}/${REL_WE}_pb2.py"
        "${CMAKE_CURRENT_SOURCE_DIR}/${REL_DIR}/${REL_WE}_pb2_grpc.py"
        COMMAND ${PROTOBUF_PROTOC_EXECUTABLE}
        ARGS
            --python_out ${CMAKE_CURRENT_SOURCE_DIR}
            --grpc_python_out ${CMAKE_CURRENT_SOURCE_DIR}
            --plugin=protoc-gen-grpc_python=${PROTOBUF_GRPC_PYTHON_PLUGIN}
            ${_protobuf_include_path} ${ABS_FIL}
        COMMAND iconv -c -f utf8 -t ascii ${CMAKE_CURRENT_SOURCE_DIR}/${REL_DIR}/${REL_WE}_pb2.py > /tmp/${TMPNAM}_${REL_WE}_pb2.py
        COMMAND mv /tmp/${TMPNAM}_${REL_WE}_pb2.py ${CMAKE_CURRENT_SOURCE_DIR}/${REL_DIR}/${REL_WE}_pb2.py
        COMMAND iconv -c -f utf8 -t ascii ${CMAKE_CURRENT_SOURCE_DIR}/${REL_DIR}/${REL_WE}_pb2_grpc.py > /tmp/${TMPNAM}_${REL_WE}_pb2_grpc.py
        COMMAND mv /tmp/${TMPNAM}_${REL_WE}_pb2_grpc.py ${CMAKE_CURRENT_SOURCE_DIR}/${REL_DIR}/${REL_WE}_pb2_grpc.py
        DEPENDS ${ABS_FIL} ${PROTOBUF_PROTOC_EXECUTABLE}
        COMMENT "Generating ${CMAKE_CURRENT_SOURCE_DIR}/${REL_DIR}/${REL_WE}_pb2.py with grpc python compiler on ${FIL}"
        VERBATIM)
  endforeach ()

  set(${SRCS} ${${SRCS}} PARENT_SCOPE)
endfunction()

function(PROTOBUF_GENERATE_PB_PYTHON SRCS)
  if (NOT ARGN)
    message(SEND_ERROR "Error: PROTOBUF_GENERATE_GRPC_PYTHON() called without any proto files")
    return()
  endif ()

  if (PROTOBUF_GENERATE_CPP_APPEND_PATH)
    # Create an include path for each file specified
    foreach (FIL ${ARGN})
      get_filename_component(ABS_FIL ${FIL} ABSOLUTE)
      get_filename_component(ABS_PATH ${ABS_FIL} PATH)
      list(FIND _protobuf_include_path ${ABS_PATH} _contains_already)
      if (${_contains_already} EQUAL -1)
        list(APPEND _protobuf_include_path -I ${ABS_PATH})
      endif ()
    endforeach ()
  else ()
    set(_protobuf_include_path -I ${CMAKE_CURRENT_SOURCE_DIR})
  endif ()

  if (DEFINED PROTOBUF_IMPORT_DIRS)
    foreach (DIR ${PROTOBUF_IMPORT_DIRS})
      get_filename_component(ABS_PATH ${DIR} ABSOLUTE)
      list(FIND _protobuf_include_path ${ABS_PATH} _contains_already)
      if (${_contains_already} EQUAL -1)
        list(APPEND _protobuf_include_path -I ${ABS_PATH})
      endif ()
    endforeach ()
  endif ()

  set(${SRCS})
  foreach (FIL ${ARGN})
    get_filename_component(ABS_FIL ${FIL} ABSOLUTE)
    get_filename_component(FIL_DIR ${FIL} DIRECTORY)
    get_filename_component(FIL_WE ${FIL} NAME_WE)

    if (DEFINED PROTOBUF_IMPORT_DIRS)
      foreach (DIR ${PROTOBUF_IMPORT_DIRS})
        get_filename_component(ABS_DIR ${DIR} ABSOLUTE)
        string(REPLACE ${ABS_DIR}/ "" REL_FIL ${ABS_FIL})
        #message("replace result : " ${REL_FIL} "  " ${ABS_FIL} "  with " ${DIR})
        if (NOT ${REL_FIL} STREQUAL ${ABS_FIL})
          get_filename_component(REL_DIR ${REL_FIL} DIRECTORY)
          get_filename_component(REL_WE ${REL_FIL} NAME_WE)
          # message("set local variable : " ${REL_DIR} "  " ${REL_WE} "  for " ${REL_FIL})
        endif ()
      endforeach ()
    endif ()

    string(RANDOM LENGTH 7 TS)
    string(SHA1 TMPNAM "${TS},${CMAKE_CURRENT_SOURCE_DIR}/${REL_DIR}/${REL_WE}_pb2.py")

    list(APPEND ${SRCS} "${CMAKE_CURRENT_SOURCE_DIR}/${REL_DIR}/${REL_WE}_pb2.py")

    add_custom_command(
        OUTPUT "${CMAKE_CURRENT_SOURCE_DIR}/${REL_DIR}/${REL_WE}_pb2.py"
        COMMAND ${PROTOBUF_PROTOC_EXECUTABLE}
        ARGS --python_out ${CMAKE_CURRENT_SOURCE_DIR} ${_protobuf_include_path} ${ABS_FIL}
        COMMAND iconv -c -f utf8 -t ascii ${CMAKE_CURRENT_SOURCE_DIR}/${REL_DIR}/${REL_WE}_pb2.py > /tmp/${TMPNAM}_${REL_WE}_pb2.py
        COMMAND mv /tmp/${TMPNAM}_${REL_WE}_pb2.py ${CMAKE_CURRENT_SOURCE_DIR}/${REL_DIR}/${REL_WE}_pb2.py
        DEPENDS ${ABS_FIL} ${PROTOBUF_PROTOC_EXECUTABLE}
        COMMENT "Running Python protocol buffer compiler on ${FIL}"
        VERBATIM)
  endforeach ()

  set(${SRCS} ${${SRCS}} PARENT_SCOPE)
endfunction()
