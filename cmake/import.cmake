function(MSL_IMPORT_BINARY DEST_DIR DEST_MADE PKGNAME FIL)
  set(IMPORT_MODULE_DIRECTORY "${CMAKE_SOURCE_DIR}/import_modules")
  set(DOWNLOAD_FILE ${IMPORT_MODULE_DIRECTORY}/${FIL})

  if (${FIL} MATCHES "(^.*)(\\.tar\\.gz)$")
    string(REGEX REPLACE "(^.*)(\\.tar\\.gz)$" "\\1" _dirname ${FIL})
  endif ()

  if (${FIL} MATCHES "(^.*)(\\.tar\\.bz2)$")
    string(REGEX REPLACE "(^.*)(\\.tar\\.bz2)$" "\\1" _dirname ${FIL})
  endif ()

  set(_full_dest_path "${IMPORT_MODULE_DIRECTORY}/${_dirname}")
  set(_full_dest_target_file "${IMPORT_MODULE_DIRECTORY}/${_dirname}/.extracted")

  set_source_files_properties(${_full_dest_target_file}} PROPERTIES GENERATED TRUE)
  set(${DEST_DIR} ${_full_dest_path} PARENT_SCOPE)
  set(${DEST_MADE} ${_full_dest_target_file} PARENT_SCOPE)

  if (NOT EXISTS ${IMPORT_MODULE_DIRECTORY})
    file(MAKE_DIRECTORY ${IMPORT_MODULE_DIRECTORY})
  endif ()

  set(URL_PREFIX ssh://git@pms.maum.ai:7999/common/import-binaries.git)

  add_custom_command(
      OUTPUT "${DOWNLOAD_FILE}"
      COMMAND git archive --remote=${URL_PREFIX} HEAD ${FIL} | tar x
      COMMENT "Downloading external binaries ${FIL}"
      WORKING_DIRECTORY ${IMPORT_MODULE_DIRECTORY}
      VERBATIM)

  add_custom_command(
      OUTPUT "${_full_dest_target_file}"
      COMMAND tar -zxf ${IMPORT_MODULE_DIRECTORY}/${FIL}
      COMMAND touch ${_full_dest_target_file}
      DEPENDS "${IMPORT_MODULE_DIRECTORY}/${FIL}"
      COMMENT "Extracting external ${FIL}"
      WORKING_DIRECTORY ${IMPORT_MODULE_DIRECTORY}
      VERBATIM)

  set(${PKGNAME}_INCLUDE ${_full_dest_path}/include PARENT_SCOPE)
  set(${PKGNAME}_LIB ${_full_dest_path}/lib PARENT_SCOPE)

endfunction()
