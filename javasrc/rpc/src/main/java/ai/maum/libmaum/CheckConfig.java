package ai.maum.libmaum;

public class CheckConfig {

  public static native void check(String productName);

  static {
    System.loadLibrary("maum-common");
  }
}
