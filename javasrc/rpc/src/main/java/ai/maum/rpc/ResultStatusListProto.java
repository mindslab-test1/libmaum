package ai.maum.rpc;

import static com.google.common.base.Preconditions.checkNotNull;
import static maum.rpc.Status.ExCode.ABORTED_VALUE;
import static maum.rpc.Status.ExCode.NOT_FOUND_VALUE;

import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.StatusException;
import io.grpc.StatusRuntimeException;
import io.grpc.protobuf.lite.ProtoLiteUtils;

import javax.annotation.Nullable;
import maum.rpc.Status.ExCode;

public final class ResultStatusListProto {
  private ResultStatusListProto() {
  }

  private static final Metadata.Key<maum.rpc.Status.ResultStatusList> STATUS_DETAILS_KEY =
      Metadata.Key.of(
          "grpc-status-details-bin",
          ProtoLiteUtils.metadataMarshaller(maum.rpc.Status.ResultStatusList.getDefaultInstance()));

  /**
   * Convert a ResultStatus instance to a {@link StatusRuntimeException}.
   * <p>
   * <p>The returned {@link StatusRuntimeException} will wrap a {@link Status} whose code and
   * description are set from the code and message in Status.ResultStatus. Status.ResultStatus will
   * be serialized and placed into the metadata of the returned {@link StatusRuntimeException}.
   *
   * @throws IllegalArgumentException if the value of Status.ResultStatus.getException() is not a valid
   *                                  gRPC status code.
   */
  public static StatusRuntimeException toStatusRuntimeException(ai.maum.rpc.ResultStatus resultStatus) {
    return toStatus(resultStatus.getResultStatusList()).asRuntimeException(toMetadata(resultStatus.getResultStatusList()));
  }

  /**
   * Convert a ResultStatus instance to a {@link StatusRuntimeException} with
   * additional metadata.
   * <p>
   * <p>The returned {@link StatusRuntimeException} will wrap a {@link Status} whose code and
   * description are set from the code and message in {@code statusProto}. {@code statusProto} will
   * be serialized and added to {@code metadata}. {@code metadata} will be set as the metadata of
   * the returned {@link StatusRuntimeException}.
   *
   * @throws IllegalArgumentException if the value of {@code statusProto.getCode()} is not a valid
   *                                  gRPC status code.
   */
  public static StatusRuntimeException toStatusRuntimeException(
      ai.maum.rpc.ResultStatus resultStatus, Metadata metadata) {
    return toStatus(resultStatus.getResultStatusList()).asRuntimeException(toMetadata(resultStatus.getResultStatusList(), metadata));
  }

  /**
   * Convert a ResultStatus instance to a {@link StatusException}.
   * <p>
   * <p>The returned {@link StatusException} will wrap a {@link Status} whose code and description
   * are set from the code and message in {@code statusProto}. {@code statusProto} will be
   * serialized and placed into the metadata of the returned {@link StatusException}.
   *
   * @throws IllegalArgumentException if the value of {@code statusProto.getCode()} is not a valid
   *                                  gRPC status code.
   */
  public static StatusException toStatusException(ai.maum.rpc.ResultStatus resultStatus) {
    return toStatus(resultStatus.getResultStatusList()).asException(toMetadata(resultStatus.getResultStatusList()));
  }

  /**
   * Convert a ai.rpc.ResultStatus instance to a {@link StatusException} with additional
   * metadata.
   * <p>
   * <p>The returned {@link StatusException} will wrap a {@link Status} whose code and description
   * are set from the code and message in {@code statusProto}. {@code statusProto} will be
   * serialized and added to {@code metadata}. {@code metadata} will be set as the metadata of the
   * returned {@link StatusException}.
   *
   * @throws IllegalArgumentException if the value of {@code statusProto.getCode()} is not a valid
   *                                  gRPC status code.
   */
  public static StatusException toStatusException(
      ai.maum.rpc.ResultStatus resultStatus, Metadata metadata) {
    return toStatus(resultStatus.getResultStatusList()).asException(toMetadata(resultStatus.getResultStatusList(), metadata));
  }

  private static Status toStatus(maum.rpc.Status.ResultStatusList resultStatusList) {
    maum.rpc.Status.ResultStatus resultStatus = resultStatusList.getList(0);

    // Mapping ResultStatus error code => grpc.Status error code
    Status status = getStatusCode(resultStatus.getCodeValue());
    return status.withDescription(resultStatus.getMessage());
  }

  private static Status getStatusCode(int code) {
    switch (code) {
      case ExCode.SUCCESS_VALUE: return Status.OK;
      case ExCode.AUTHORIZATION_FAILED_VALUE: return Status.UNAUTHENTICATED;
      case ExCode.INVALID_ARGUMENT_VALUE: return Status.INVALID_ARGUMENT;
      case ExCode.PERMISSION_DENIED_VALUE: return Status.PERMISSION_DENIED;
    }

    switch (code / 10) {
      case (ExCode.NOT_FOUND_VALUE / 10): return Status.NOT_FOUND;
      case (ExCode.ALREADY_EXISTS_VALUE / 10): return Status.ALREADY_EXISTS;
      case (ExCode.FAILED_PRECONDITION_VALUE / 10): return Status.FAILED_PRECONDITION;
      case (ExCode.RESOURCE_EXHAUSTED_VALUE / 10): return Status.RESOURCE_EXHAUSTED;
      case (ExCode.OUT_OF_RANGE_VALUE / 10): return Status.OUT_OF_RANGE;
      case (ExCode.DATA_LOSS_VALUE / 10): return Status.DATA_LOSS;
      case (ExCode.ABORTED_VALUE / 10): return Status.ABORTED;
    }

    return Status.UNKNOWN;
  }
  
  private static Metadata toMetadata(maum.rpc.Status.ResultStatusList resultStatusList) {
    Metadata metadata = new Metadata();
    metadata.put(STATUS_DETAILS_KEY, resultStatusList);
    return metadata;
  }

  private static Metadata toMetadata(maum.rpc.Status.ResultStatusList resultStatusList, Metadata metadata) {
    checkNotNull(metadata, "metadata must not be null");
    metadata.discardAll(STATUS_DETAILS_KEY);
    metadata.put(STATUS_DETAILS_KEY, resultStatusList);
    return metadata;
  }

  /**
   * Extract a ResultStatus instance from the causal chain of a {@link Throwable}.
   *
   * @return the extracted  Status.ResultStatusList instance, or {@code null} if none exists.
   * @throws IllegalArgumentException if an embedded  Status.ResultStatusList is found and its
   *                                  code does not match the gRPC {@link Status} code.
   */
  @Nullable
  public static ResultStatus fromThrowable(Throwable t) {
    Throwable cause = checkNotNull(t, "check not null");
    while (cause != null) {
      if (cause instanceof StatusException) {
        StatusException e = (StatusException) cause;
        return toResultStatusListProto(e.getStatus(), e.getTrailers());
      } else if (cause instanceof StatusRuntimeException) {
        StatusRuntimeException e = (StatusRuntimeException) cause;
        return toResultStatusListProto(e.getStatus(), e.getTrailers());
      }
      cause = cause.getCause();
    }
    return null;
  }

  @Nullable
  private static ResultStatus toResultStatusListProto(Status status, Metadata trailers) {
    if (trailers != null) {
      maum.rpc.Status.ResultStatusList resultStatusList = trailers.get(STATUS_DETAILS_KEY);
      if (resultStatusList != null) {
        ResultStatus resultStatus = new ResultStatus(resultStatusList);
        return resultStatus;
      }
    }
    return null;
  }
}
