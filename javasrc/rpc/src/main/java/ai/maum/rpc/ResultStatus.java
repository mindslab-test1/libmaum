package ai.maum.rpc;

import ai.maum.libmaum.CheckConfig;
import com.google.protobuf.Any;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Message;
import com.google.protobuf.util.JsonFormat;
import java.util.ArrayList;
import maum.rpc.ErrorDetails;
import maum.rpc.Status.Module;
import org.json.JSONObject;

public final class ResultStatus {

  private maum.rpc.Status.ResultStatusList resultStatusList;
  public maum.rpc.Status.ResultStatus status;
  static maum.rpc.Status.Module mModule = Module.UNDEFINED;
  static int mProcess = -1;

  public ResultStatus() {
    System.out.println("Called default ResultStatus Constructor");
  }

  public ResultStatus(maum.rpc.Status.ExCode code, int error_index, String message)
      throws Exception {
    if (mModule == null || mProcess == -1) {
      throw new Exception("Module or Process is not initialized.");
    }
    System.out.println("Initialized ResultStatus in ResultStatus Constructor");
    this.status = maum.rpc.Status.ResultStatus.newBuilder()
        .setModule(mModule)
        .setProcess(mProcess)
        .setCode(code)
        .setErrorIndex(error_index)
        .setMessage(message)
        .build();

    this.setResultStatusList(this.status);
  }

  /**
   * binary로 전달되는건 ResultStatusList object 이지만, ResultStatus 내부로 해당 과정을 감추기 위해서
   * ResultStatusListProto 파일 toResultStatusListProto 함수에서 해당 생성자를 사용
   */
  public ResultStatus(maum.rpc.Status.ResultStatusList resultStatusList) {
    System.out.println("Initialized ResultStatusList in ResultStatus Constructor");
    this.resultStatusList = resultStatusList;
  }

  /**
   * Status.ResultList 객체의 repeated ResultStatus에 객체를 추가하는 함수
   *
   * @param newStatus ResultStatus 객체
   */
  public void addStackStatus(ai.maum.rpc.ResultStatus newStatus) {

    if (newStatus.getResultStatusList() == null) {
      this.resultStatusList = maum.rpc.Status.ResultStatusList
          .newBuilder()
          .addAllList(newStatus.getResultStatusList().getListList())
          .build();
    } else {
      this.resultStatusList = maum.rpc.Status.ResultStatusList
          .newBuilder()
          .mergeFrom(this.resultStatusList)
          .addAllList(newStatus.getResultStatusList().getListList())
          .build();
    }
  }

  /**
   * Status.ResultStatus 객체의 repeated Any 타입의 detail 객체를 추가하는 함수
   *
   * @param message Protobuf Message object
   */
  public void addDetail(Message message) {
    Any any = Any.pack(message);
    this.status = maum.rpc.Status.ResultStatus
        .newBuilder()
        .mergeFrom(this.status)
        .addDetails(any)
        .build();

    this.setResultStatusList(this.status);
  }

  /**
   * ai.rpc.ResultStatus의 멤버 변수인 resultStatusList에 list를 추가하는 함수
   */
  private void setResultStatusList(maum.rpc.Status.ResultStatus resultStatus) {
    this.resultStatusList = maum.rpc.Status.ResultStatusList
        .newBuilder()
        .addList(resultStatus)
        .build();
  }

  /**
   * Status.ResultList 객체 반환
   *
   * @return Status.ResultList
   */
  public maum.rpc.Status.ResultStatusList getResultStatusList() {
    return resultStatusList;
  }

  /**
   * 13자리 Error Code를 만드는 함수입니다. 입력받은 ResultStatusList에서 가장 먼저 발생한 에러 인 0번쨰 요소에 접근하여 Error Code를 만들어
   * 냅니다. {module}{proc}{hyphen}{code}{error_index} 2 + 2 + 1 + 3 + 5 : 13자리
   *
   * @return Error Code string
   */
  public String makeErrorCode() throws Exception {
    if (this.resultStatusList.getListCount() == 0) {
      throw new Exception("ResultStatusList is empty");
    } else {
      maum.rpc.Status.ResultStatus resultStatus = this.resultStatusList.getList(0);
      String errorCode = String.format("%02d%02d%s%03d%05d",
          resultStatus.getModule().getNumber(),
          resultStatus.getProcess(),
          '-',
          resultStatus.getCodeValue(),
          resultStatus.getErrorIndex());
      return errorCode;
    }
  }

  /**
   * ResultStatusList에 있는 모든 Stack 정보를 얻는다.
   */
  public String getAllStackStr() {
    ArrayList<ArrayList<String>> rl = new ArrayList<>();

    for (int i = 0; i < this.resultStatusList.getListCount(); i++) {
      System.out.println("Module : " + this.resultStatusList.getList(i).getModule());
      System.out.println("Process : " + this.resultStatusList.getList(i).getProcess());
      System.out.println("Error Code: " + this.resultStatusList.getList(i).getCodeValue());
      System.out.println("Error Index : " + this.resultStatusList.getList(i).getErrorIndex());
      System.out.println("Message : " + this.resultStatusList.getList(i).getMessage());
      System.out.println("Details");

      ArrayList<String> dl = new ArrayList<>();

      for (Any any : this.resultStatusList.getList(i).getDetailsList()) {
        // message DebugInfoFile
        if (any.is(ErrorDetails.DebugInfoFile.class)) {
          try {
            ErrorDetails.DebugInfoFile debug = any.unpack(ErrorDetails.DebugInfoFile.class);
            dl.add(JsonFormat.printer().includingDefaultValueFields().print(debug));
          } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
          }
        }
        // message RetryInfo
        if (any.is(ErrorDetails.RetryInfo.class)) {
          try {
            ErrorDetails.RetryInfo debug = any.unpack(ErrorDetails.RetryInfo.class);
            dl.add(JsonFormat.printer().includingDefaultValueFields().print(debug));
          } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
          }
        }
        // message DebugInfo
        if (any.is(ErrorDetails.DebugInfo.class)) {
          try {
            ErrorDetails.DebugInfo debug = any.unpack(ErrorDetails.DebugInfo.class);
            dl.add(JsonFormat.printer().includingDefaultValueFields().print(debug));
          } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
          }
        }
        // message QuotaFailure
        if (any.is(ErrorDetails.QuotaFailure.class)) {
          try {
            ErrorDetails.QuotaFailure debug = any.unpack(ErrorDetails.QuotaFailure.class);
            dl.add(JsonFormat.printer().includingDefaultValueFields().print(debug));
          } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
          }
        }
        // message PreconditionFailure
        if (any.is(ErrorDetails.PreconditionFailure.class)) {
          try {
            ErrorDetails.PreconditionFailure debug = any
                .unpack(ErrorDetails.PreconditionFailure.class);
            dl.add(JsonFormat.printer().includingDefaultValueFields().print(debug));
          } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
          }
        }
        // message BadRequest
        if (any.is(ErrorDetails.BadRequest.class)) {
          try {
            ErrorDetails.BadRequest debug = any.unpack(ErrorDetails.BadRequest.class);
            dl.add(JsonFormat.printer().includingDefaultValueFields().print(debug));
          } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
          }
        }
        // message RequestInfo
        if (any.is(ErrorDetails.RequestInfo.class)) {
          try {
            ErrorDetails.RequestInfo debug = any.unpack(ErrorDetails.RequestInfo.class);
            dl.add(JsonFormat.printer().includingDefaultValueFields().print(debug));
          } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
          }
        }
        // message ResourceInfo
        if (any.is(ErrorDetails.ResourceInfo.class)) {
          try {
            ErrorDetails.ResourceInfo debug = any.unpack(ErrorDetails.ResourceInfo.class);
            dl.add(JsonFormat.printer().includingDefaultValueFields().print(debug));
          } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
          }
        }
        // message Help
        if (any.is(ErrorDetails.Help.class)) {
          try {
            ErrorDetails.Help debug = any.unpack(ErrorDetails.Help.class);
            dl.add(JsonFormat.printer().includingDefaultValueFields().print(debug));
          } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
          }
        }
        // message LocalizedMessage
        if (any.is(ErrorDetails.LocalizedMessage.class)) {
          try {
            ErrorDetails.LocalizedMessage debug = any.unpack(ErrorDetails.LocalizedMessage.class);
            dl.add(JsonFormat.printer().includingDefaultValueFields().print(debug));
          } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
          }
        }
      }

      if (dl.size() < 1) {
        continue;
      } else {
        rl.add(dl);
      }
    }

    if (rl.size() < 1) {
      return null;
    }

    JSONObject jo = new JSONObject(rl);

    return jo.toString();
  }

  /**
   * Module 정보 설정
   */
  public static void setModule(maum.rpc.Status.Module module) {
    if (module.getNumber() <= Module.UNDEFINED_VALUE) {
      CheckConfig.check(module.name().toLowerCase());
    }
    mModule = module;
  }

  /**
   * Process 정보 설정
   */
  public static void setProcess(int process) {
    mProcess = process;
  }

  /**
   * Module과 Process 정보 설정
   */
  public static void setModuleAndProcess(maum.rpc.Status.Module module, int process) {
    setModule(module);
    setProcess(process);
  }
}
