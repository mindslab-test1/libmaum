#!/usr/bin/env bash

target="[libmaum]"

lictool=$1
if [ -z ${lictool} ]; then
  lictool=OFF
fi

maum_root=${MAUM_ROOT}
if [ "${maum_root}" = "" ]; then
  echo "${target}" "MAUM_ROOT is not defined!"
  exit 1
fi

test -d ${maum_root} || mkdir -p ${maum_root}

repo_root=$(pwd)
NPROC=$(nproc)
echo "${target}" "repo_root: ${repo_root}, NPROC: ${NPROC}, MAUM_ROOT: ${MAUM_ROOT}"

git submodule update --init

OS=
if [ -f /etc/lsb-release ]; then
  OS=ubuntu
elif [ -f /etc/centos-release ]; then
  OS=centos
elif [ -f /etc/redhat-release ]; then
  OS=centos
else
  echo "${target}" "Illegal OS, use ubuntu or centos"
  exit 1
fi

function get_requirements() {
  if [ "$OS" = "ubuntu" ]; then
    sudo apt-get install -y \
      autoconf \
      automake \
      libtool \
      curl \
      make \
      cmake \
      unzip \
      g++ g++-4.8 \
      libgflags-dev \
      libgtest-dev \
      libpcre3-dev \
      libssl-dev

  else
    sudo yum install -y epel-release
    sudo yum install -y \
      autoconf \
      audomake \
      cmake \
      cmake3 \
      make \
      gcc gcc-c++ \
      libcurl-devel.x86_64 \
      libtool \
      autoconf \
      automake \
      unzip.x86_64 \
      gflags-devel.x86_64 \
      openssl-devel.x86_64 \
      pcre-devel.x86_64

  fi
}

GLOB_BUILD_DIR=${HOME}/.maum-build
test -d ${GLOB_BUILD_DIR} || mkdir -p ${GLOB_BUILD_DIR}
sha1=$(git log -n 1 --pretty=format:%H ./build.sh)
echo "${target}" "Last commit for build.sh: ${sha1}"

if [ -z "${DOCKER_MAUM_BUILD}" ] && [ ! -f ${GLOB_BUILD_DIR}/${sha1}.done ]; then
  get_requirements
  if [ "$?" = "0" ]; then
    touch ${GLOB_BUILD_DIR}/${sha1}.done
  fi
else
  echo "${target}" "get_requirements had been done!"
fi

__CC=${CC}
__CXX=${CXX}
if [ "${OS}" = "centos" ]; then
  if [ -z ${__CC} ]; then
    echo "${target}" "CC not defined, use /usr/bin/gcc"
    __CC=/usr/bin/gcc
  fi
  if [ -z ${__CXX} ]; then
    echo "${target}" "CXX not defined, use /usr/bin/g++"
    __CXX=/usr/bin/g++
  fi
  CMAKE=/usr/bin/cmake3
  OS_VER=$(cat /etc/redhat-release)
else
  if [ -z ${__CC} ]; then
    echo "${target}" "CC not defined, use /usr/bin/gcc-4.8"
    __CC=/usr/bin/gcc-4.8
  fi
  if [ -z ${__CXX} ]; then
    echo "${target}" "CXX not defined, use /usr/bin/g++-4.8"
    __CXX=/usr/bin/g++-4.8
  fi
  CMAKE=/usr/bin/cmake
  OS_VER=$(lsb_release -sd)
fi

GCC_VER=$(${__CC} -dumpversion)

LIBMAUM_SHA1=$(cd ${PWD}/third-party >/dev/null && git log -n1 --pretty=%H -- .)
CACHE_PREFIX=${HOME}/.maum-build/cache/libmaum-${LIBMAUM_SHA1}-${GCC_VER}
echo "${target}" "cache prefix is ${CACHE_PREFIX}"

function build_protobuf_and_grpc() {
  if [ -f ${CACHE_PREFIX}//lib/libprotobuf.so.19.0.0 ] &&
    [ -f ${repo_root}/third-party/protobuf/src/.libs/libprotobuf.so.19.0.0 ] &&
    [ -f ${CACHE_PREFIX}/lib/libgrpc++.so.1.22.1 ] &&
    [ -f ${repo_root}/third-party/grpc/libs/opt/libgrpc++.so.1.22.1 ]; then
    echo "${target}" "Protobuf & GRPC are already built."
  else
    echo "${target}" "Protobuf & GRPC are not ready"
    rm -rf "${CACHE_PREFIX}"
    # protobuf build
    echo "${target}" "Build Protobuf..."
    cd ${repo_root}/third-party/protobuf || exit
    git clean -fdx
    ./autogen.sh
    ./configure CC=${__CC} CXX=${__CXX} --prefix=${CACHE_PREFIX}
    make -j${NPROC}
    make install
    if [ -f ./confcache ]; then
      rm ./confcache
    fi
    # grpc build
    echo "${target}" "Build GRPC..."
    cd ${repo_root}/third-party/grpc || exit
    git clean -fdx
    git submodule update --init
    git submodule foreach git clean -fdx
    export CC=${__CC}
    export CXX=${__CXX}
    make -j${NPROC}
    make grpc_cli -j${NPROC}
    ln -sf ${CACHE_PREFIX} LCACHE
    make prefix=LCACHE install
    rm -f LCACHE
    cp bins/opt/grpc_cli ${CACHE_PREFIX}/bin/grpc_cli
  fi
}

function install_cache() {
  echo "${target}" "Install Cache..."
  local cache_ver=($(sha1sum -b $(readlink -f ${CACHE_PREFIX}/lib/libprotobuf.so) 2>/dev/null))
  local prev_ver=""
  if [ -f ${maum_root}/lib/libprotobuf.so ]; then
    prev_ver=($(sha1sum -b $(readlink -f ${maum_root}/lib/libprotobuf.so) 2>/dev/null))
  else
    prev_ver="not_exist"
  fi

  if [ "${cache_ver}" != "${prev_ver}" ]; then
    (
      cd ${CACHE_PREFIX}
      cp -dpr bin include lib share ${maum_root}
    )
    sudo ldconfig ${maum_root}
  fi
}

function build_libmaum() {
  echo "${target}" "Build libmaum..."
  build_base="build-debug" && [[ "${MAUM_BUILD_DEPLOY}" == "true" ]] && build_base="build-deploy-debug"
  build_dir=${repo_root}/${build_base}-${GCC_VER}

  if [ "$MAUM_BUILD_DEPLOY" = true ]; then
    test -d ${build_dir} && rm -rf ${build_dir}
  fi
  test -d ${build_dir} || mkdir -p ${build_dir}
  cd ${build_dir}

  ${CMAKE} \
    -DCMAKE_PREFIX_PATH=${maum_root} \
    -DCMAKE_BUILD_TYPE=Debug \
    -DCMAKE_C_COMPILER=${__CC} \
    -DCMAKE_CXX_COMPILER=${__CXX} \
    -DCMAKE_INSTALL_PREFIX=${maum_root} \
    -DLICTOOL=${lictool} ..

  make install -j${NPROC}
}

function build_libmaum_jar() {
  echo "${target}" "Build libmaum jar..."
  local src_dir=${repo_root}/javasrc/rpc
  local build_dir=${src_dir}/build/install/libmaum-rpc-shadow/lib
  local deploy_dir=${maum_root}/lib
  (
    cd ${src_dir}
    ./gradlew clean
    ./gradlew installShadowDist
    cp ${build_dir}/libmaum-rpc-0.1.0-all.jar ${deploy_dir}/
  )
}

function copy_polyglot_jar() {
  echo "${target}" "Copy polyglot..."
  local deploy_dir=${maum_root}/lib
  if [ ! -f ${deploy_dir}/polyglot_deploy.jar ]; then
    (cp ${repo_root}/third-party/polyglot/polyglot_deploy.jar ${deploy_dir}/)
  fi
}

build_protobuf_and_grpc
install_cache
build_libmaum
build_libmaum_jar
copy_polyglot_jar

### VERSION
BIZ_VERSION=$(git describe --tags)
BUILD_DATE=$(date '+%Y-%m-%d %H:%M:%S')
vfile=${maum_root}/etc/version/libmaum
vdir=${maum_root}/etc/version
test -d ${vdir} || mkdir ${vdir}
echo "BIZ_VERSION: ${BIZ_VERSION}" >${vfile}
echo "BUILD_DATE: ${BUILD_DATE}" >>${vfile}
echo "OS_VERSION: ${OS_VER}" >>${vfile}

echo "${target}" "Build FINISHED."
